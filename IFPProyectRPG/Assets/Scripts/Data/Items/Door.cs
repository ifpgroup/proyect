﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : InteractableItem
{
    [SerializeField] private Animator anim;
    [SerializeField] private Collider2D colliderOfDoor;
    private AudioController sound;
    private int index;
    public override void PlayAction()
    {
        Debug.Log("Herer executed");
        anim.SetTrigger("Open");
        InteractableItems.SetActive(index);
        if(sound) sound.PlayDoor();
        colliderOfDoor.enabled = false;
    }

    public override void SetData(ScriptableObject data, int i)
    {
        index = i;
    }

    private void Awake()
    {
        if (anim == null)
            anim = GetComponent<Animator>();
        if (colliderOfDoor == null)
            colliderOfDoor = GetComponent<Collider2D>();
    }
    private void Start()
    {
        sound = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
    }
}
