﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    private GameObject playerManager;
    private GameObject questManager;
    // Start is called before the first frame update
    void Start()
    {
        playerManager = GameObject.FindGameObjectWithTag("PlayerManager");        
        questManager = GameObject.FindGameObjectWithTag("QuestManager");        
    }

    public void LoadLevel()
    {
        if (playerManager)
            Destroy(playerManager.gameObject);
        if (questManager)
            Destroy(questManager.gameObject);
        SceneManager.LoadScene("PlayerCreation");
    }
    public void LoadMenu()
    {
        if (playerManager)
            Destroy(playerManager.gameObject);
        if (questManager)
            Destroy(questManager.gameObject);
        SceneManager.LoadScene("MainMenu");
    }
    public void LoadCredits()
    {
        SceneManager.LoadScene("Credits");
    }
    public void Exit()
    {
        Application.Quit();
    }
}
