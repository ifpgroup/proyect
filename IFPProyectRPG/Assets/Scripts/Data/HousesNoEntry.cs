﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HousesNoEntry : MonoBehaviour
{
    private Player player;
    public AutoMessage autoMess;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("PlayerManager")?.GetComponent<Player>();
        //autoMess = GameObject.FindGameObjectWithTag("AutoMessage").GetComponent<AutoMessage>();
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Entity"))
        {
            autoMess.SetTextAutoMessage("Aquí vive alquien.");
            autoMess.OpenMessage();
        }
    }
}
