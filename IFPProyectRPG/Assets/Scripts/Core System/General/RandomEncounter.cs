﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class RandomEncounter : MonoBehaviour
{
    // Start is called before the first frame update
    private Player player;
    private float distToEncounter;
    private float distMoved;
    [SerializeField] private MovementController moveController;    
    private Transform playerLocation;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("PlayerManager").GetComponent<Player>();
        distMoved = 0.0f;
        moveController.onMove += OnMove;        
        distToEncounter = Random.Range(5.0f, 150.0f);
    }
    public void SetPlayerTransform(Transform location) => playerLocation = location;
    public void OnMove(float dist)
    {
        distMoved += Mathf.Abs(dist);
        if(distMoved>=distToEncounter)
        {
            distToEncounter = Random.Range(5.0f, 150.0f);
            distMoved = 0.0f;
            player.SetLastPosition(playerLocation.position);
            player.SetStateSceneName(SceneManager.GetActiveScene().name);            
            SceneManager.LoadScene("CombatLevel");
        }
       // Debug.Log("Distance moved: " + distMoved + " Distance to encounter: " + distToEncounter);
    }
    public void startCombat()
    {
        player.SetLastPosition(playerLocation.position);
        player.SetStateSceneName(SceneManager.GetActiveScene().name);
        SceneManager.LoadScene("CombatLevel");
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
