﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestAreaControl : MonoBehaviour
{
    public Quest quest;
    private QuestManager questManager;
    private string questID = "KillTrollsQuest001";
    public AutoMessage autoMess;
    private Player player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("PlayerManager")?.GetComponent<Player>();
        questManager = GameObject.FindGameObjectWithTag("QuestManager").GetComponent<QuestManager>();
        quest = questManager.GiveQuestFromList(questID);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Entity"))
        {
            if (quest.GetIsCompleted() == false)
            {
                autoMess.SetTextAutoMessage("Tengo que hacer lo que me dijo el abuelo.");
                autoMess.OpenMessage();
            }
            else if (quest.GetIsCompleted() == true)
            {
                var pos = collision.gameObject.transform.position;
                pos.y -= 1.5f;
                player.SetLastPosition(pos);
                player.SetStateSceneName("Town");
                player.GoForwardOrBack(false);
                UnityEngine.SceneManagement.SceneManager.LoadScene("QuestArea");
            }
        }
    }
}
