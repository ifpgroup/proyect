﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Effect 
{
    [SerializeField] private TypeOfEffect typeOfEffect;    
    [SerializeField] private bool isAlwaysInEffect;
    [SerializeField] private float chanceToAppear;
    [SerializeField] private bool isUsingDice;
    [SerializeField] private int numDices;
    [SerializeField] private int maxDiceRoll;
    [SerializeField] private float fixedValue;
    [SerializeField] private int duration;

    public TypeOfEffect GetTypeOfEffect() => typeOfEffect;
    public effectResult Apply()
    {
        effectResult result = new effectResult();
        if (isAlwaysInEffect)
        {
            result = CalculateResult();
        }
        else
        {
            var chance = Random.Range(0.0f, 100.0f);
            if(chance <= chanceToAppear)
            {
                result = CalculateResult();
            }
            else
            {
                result.value = -1;
                result.duration = -1;                
            }
        }
        return result;
    }
    private effectResult CalculateResult()
    {
        effectResult result = new effectResult();
        if (isUsingDice)
        {
            int diceNumber = Random.Range(1, maxDiceRoll + 1) * numDices;
            float value = diceNumber + fixedValue;
            result.value = value;
            result.duration = duration;
        }
        else
        {            
            result.value = fixedValue;
            result.duration = duration;
        }
        return result;
    }
}
