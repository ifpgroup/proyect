﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class LightEffectFire : MonoBehaviour
{
    public float timeBetween = 0.25f;
    public float intensityChange = 0.25f;
    private Light2D light;
    private float rand;
    private float originalIntensity;
    private float timer;

    void Awake()
    {
        light = gameObject.GetComponent<Light2D>();
        originalIntensity = light.intensity;
        timer = timeBetween;
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            rand = Random.Range(originalIntensity - intensityChange, originalIntensity + intensityChange);
            light.intensity = rand;
            timer = timeBetween;
        }
    }
}
