﻿#pragma warning disable 0649
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest
{
	private QuestData questData;
	public bool isInProgress;
	private bool isCompleted;
    private bool rewardGiven;
	private QuestTarget qt;
	private List<QuestTarget> qts;
	

	public Quest(QuestData qData, bool isInProgress, bool isCompleted)
	{
		questData = qData;
        this.isInProgress = isInProgress;
        this.isCompleted = isCompleted;
        rewardGiven = false;
        qts = new List<QuestTarget>();
        foreach (var item in questData.GetTargets())
        {
            QuestTarget newTarget = new QuestTarget(item);
            qts.Add(newTarget);
        }
	}

    ////	consider if there will be an UI for quests
    public Reward GetReward() => questData.GetReward();
	public string GetQuestID() => questData.GetQuestID();
	public string GetQuestName() => questData.GetQuestName();
	public string GetQuestDescription() => questData.GetQuestDescription();
	public bool GetIsCompleted() => isCompleted;
	public bool GetIsInProgress() => isInProgress;
	//	public bool SetIsInProgress(bool prog) => isInProgress = prog;
	//	public bool GetIsMain() => questData.GetIsMain();
	public QuestTarget[] GetQuestTargets() => questData.GetTargets();

    /*public Quest ActivateQuest(string questID)
	{
		qManager.GiveQuestFromList(questID);
		return new Quest(questData, true, false);
	}*/
    public void RewardGiven() => rewardGiven = true;
    public bool GetRewardGiven() => rewardGiven;
	public void IsCompletedQuest()
	{
        foreach (var target in qts)
        {
            if (!target.GetCheckTarget())
                return;
        }		
        isCompleted = true;
        isInProgress = false;
        Debug.Log("Todos los objetivos completos. Misión completada.");
		
	}

	//	List which gets complete quests of the main list of quests


	//	recorre targets[] de questData en busca del ID

	public bool CheckTarget(TypeOfTargets typeTar, string tgID, int ammount = 1)
	{
		Debug.Log("Voy a proceder a comprobar tipos de target");
		foreach (QuestTarget t in qts)
		{
			if (typeTar == TypeOfTargets.KILLENEMIES)
			{
				int n = t.GetNumberOfTypes();
				if (tgID.Equals(t.GetTargetID()))
				{
                    t.IncreaseNumberCompleted(ammount);
					if (t.CheckCompleted())
						t.SetCheckTarget(true);
					Debug.Log("He matado un enemigo check");
                    IsCompletedQuest();
                    return true;
				}
			}
			if (typeTar == TypeOfTargets.TALKTOSOMEONE)
			{
				int n = t.GetNumberOfTypes();
				if (tgID == t.GetTargetID())
				{
                    t.IncreaseNumberCompleted(ammount);
                    if (t.CheckCompleted())
                        t.SetCheckTarget(true);
					Debug.Log("He hablado con alguien check");
                    IsCompletedQuest();
                    return true;
                }
			}
			if (typeTar == TypeOfTargets.GETITEM)
			{
				int n = t.GetNumberOfTypes();
				if (tgID == t.GetTargetID())
				{
                    t.IncreaseNumberCompleted(ammount);
                    if (t.CheckCompleted())
                        t.SetCheckTarget(true);
					Debug.Log("He cogido un item");
                    IsCompletedQuest();
                    return true;
                }
			}
			IsCompletedQuest();
		}
		return false;
	}

}