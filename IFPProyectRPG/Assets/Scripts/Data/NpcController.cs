﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NpcController : MonoBehaviour, INPCConversation
{
    [SerializeField] private string npcID;
    [SerializeField] private string questID;
    [SerializeField] private List<Conversation> conversations;
    [SerializeField] private Conversation questAccepted;
    [SerializeField] private Conversation questCompleted;
    [SerializeField] private Conversation questAlreadyCompleted;
    [SerializeField] private DialogueManager dialogueManager;
    [SerializeField] private string trigger;
    [SerializeField] private string refuseTrigger;
    private Player player;
    private Conversation activeConversation;
    private Conversation conv;
    private QuestManager questManager;
    private Quest quest;
    private bool questCompletedAndNpcTalked;
    private bool giveReward;
    private bool refusedQuest = false;

    public void Talk()
    {
        player.NotifyQuestsSpokeWithNPC(npcID);
        if (activeConversation == null) return;
        if (refusedQuest)
        {
            if (conversations[1].GetID() != "AcceptQuest001" && conversations[1].GetID() != "Refuse")
                activeConversation = conversations[1];
        }
        if (quest != null)
        { 
            if (quest.isInProgress)
            activeConversation = questAccepted;
            else if (quest.GetIsCompleted() && questCompletedAndNpcTalked == false)
            {
                RewardPlayer();
                if (giveReward)
                    activeConversation = questCompleted;
                questCompletedAndNpcTalked = true;
            }
            else if (quest.GetIsCompleted() && giveReward)
                activeConversation = questAlreadyCompleted;
        }
        /// will open a dialogue        
        Debug.Log("Talk with old woman");
        MovementController.isTalking = true;
        dialogueManager.StartConversation(activeConversation, 0);
        dialogueManager.optionPress += ChangeConversation;
    }
    public void ChangeConversation(string nextConv)
    {
        if (nextConv.Equals(trigger))
        {
            quest.isInProgress = true;
            player.AcceptQuest(quest);            
        }
        foreach (var conv in conversations)
        {
            if (conv.GetID().Equals(nextConv))
            {
                activeConversation = conv;
                dialogueManager.StartConversation(activeConversation, 0);
                return;
            }
        }
        dialogueManager.optionPress -= ChangeConversation;
        dialogueManager.EndConversation();        
            
    }
    // Start is called before the first frame update
    void Start()
    {
        
        giveReward = false;
        player = GameObject.FindGameObjectWithTag("PlayerManager").GetComponent<Player>();
        questManager = GameObject.FindGameObjectWithTag("QuestManager").GetComponent<QuestManager>();
        quest = questManager.GiveQuestFromList(questID);
        if (quest != null)
        {
            if (quest.GetIsCompleted() && quest.GetRewardGiven())
            {
                questCompletedAndNpcTalked = true;
                giveReward = true;
            }
            else
                questCompletedAndNpcTalked = false;
        }
        if (conversations != null)
            activeConversation = conversations[0];
    }

    private void RewardPlayer()
    {
        var reward = quest.GetReward();
        giveReward = true;
        quest.RewardGiven();
        player.CompleteQuest(quest);
        if(reward.MaxGold > 0)
        {
            player.GetInventory().AddGold(reward.MaxGold);
        }
        if (reward.maxCrystals > 0)
            player.GetInventory().AddCrystal(reward.maxCrystals);
        if (reward.possibleCards.Count > 0)
        {
            foreach (var item in reward.possibleCards)
            {
                // TODO
            }
        }
        if (reward.possibleValuableItemDrop.Count > 0)
        {
            foreach (var item in reward.possibleValuableItemDrop)
            {
                player.GetInventory().AddItem(item);
            }
        }
    }
    void Update()
	{
        if (activeConversation == null)
            return;
        if (refuseTrigger == null)
            return;
        else if (activeConversation.GetID() == refuseTrigger)
        {
            refusedQuest = true;
        }
    }

}
