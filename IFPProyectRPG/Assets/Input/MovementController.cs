﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

enum animationStates { UP, DOWN, RIGHT, LEFT, IDLE}
public class MovementController : MonoBehaviour
{
    public LayerMask IgnoreMe;
    public float movementH;
    public float movementV;
    public float speed;
    bool isMovH;
    bool isMovV;
    bool lastPressedDirIsUpDown;
    public GameObject target;
    public Animator animator;
    private animationStates state;
    private animationStates lookingDir;
    public event UnityAction<float> onMove;
    private const float distUntilHealingConst = 20.0f;
    private float distUntilHealing;
    private float healing = 5.0f;
    private Player player;
    private Rigidbody2D rBodyPlayer;
    public static bool isTalking;
    private UIDungeonManager uiDungeonManager;

    // Start is called before the first frame update
    void Start()
    {
        isTalking = false;
        movementH = 0;
        movementV = 0;
        distUntilHealing = distUntilHealingConst;
        player = GameObject.FindGameObjectWithTag("PlayerManager")?.GetComponent<Player>();
        state = animationStates.IDLE;
        uiDungeonManager = GameObject.FindGameObjectWithTag("UIDungeonManager")?.GetComponent<UIDungeonManager>();
    }
    public void SetTarget(GameObject character) {
        target = character;
        rBodyPlayer = target.GetComponent<Rigidbody2D>();
    }
    public void SetAnimator(Animator animController) => animator = animController;
    public void Move(float dist)
    {
        distUntilHealing -= Mathf.Abs(dist);
       // Debug.Log("Distance until healing: " + distUntilHealing);
        if(distUntilHealing <= 0.0f && player != null)
        {
            
            foreach (var partyMember in player.GetParty())
            {
                Debug.Log("Healing current Hp: " + partyMember.GetAttr.hp);
                partyMember.Heal(healing);
                partyMember.Recovery();
                Debug.Log("Healing Done current Hp: " + partyMember.GetAttr.hp);
                if(uiDungeonManager != null)
                {
                    uiDungeonManager.UpdateMiniStats();
                }
            }
            distUntilHealing = distUntilHealingConst;
        }
        onMove?.Invoke(dist);
    }
    // Update is called once per frame
    public void CastRayCast(InputAction.CallbackContext ctx)
    {
        if (ctx.performed)
        {
            RaycastHit2D hit;
            Vector2 dir;
            switch (lookingDir)
            {
                case animationStates.UP:
                    dir = new Vector2(0.0f, 1.0f);
                    break;
                case animationStates.DOWN:
                    dir = new Vector2(0.0f, -1.0f);
                    break;
                case animationStates.RIGHT:
                    dir = new Vector2(1.0f, 0.0f);
                    break;
                case animationStates.LEFT:
                    dir = new Vector2(-1.0f, 0.0f);
                    break;                
                default:
                    dir = new Vector2(0.0f, 0.0f);
                    break;
            }
            hit = Physics2D.CircleCast(target.transform.position, 0.5f,dir, 3.0f, ~IgnoreMe);
            Debug.Log("keyPressed E");
            if (hit)
            {
                Debug.Log("Hit: " + hit.collider.gameObject.tag);
                if (hit.collider.gameObject.tag.Equals("InteractuableItem"))
                {
                    var script = hit.collider.gameObject.GetComponent<InteractableItem>();
                    if (script.GetTypeOfItem() != TypeOfInteractableItem.TRAP)
                    {
                        script.PlayAction();
                    }
                }
                else if (hit.collider.gameObject.tag.Equals("NPC"))
                {
                    var script = hit.collider.gameObject.GetComponent<INPCConversation>();
                    script.Talk();
                }
            }
        }
    }
    void Update()
    {
        if (isTalking) return;
        float dist = 0.0f;
        float vel = 0.0f;
        if (isMovH && ((isMovV && !lastPressedDirIsUpDown) || !isMovV))
        {
            vel = movementH * speed;
            dist = vel * Time.deltaTime;
            rBodyPlayer.velocity = new Vector2(vel, 0.0f);
            if (movementH.Equals(1))
            {
                if(state != animationStates.RIGHT)
                {
                    state = animationStates.RIGHT;
                    lookingDir = animationStates.RIGHT;
                    animator.SetTrigger("Right");
                }
                    
            }
            else
            {
                if (state != animationStates.LEFT)
                {
                    state = animationStates.LEFT;
                    lookingDir = animationStates.LEFT;
                    animator.SetTrigger("Left");
                }
                    
            }
        }
        else if(isMovV && ((isMovH && lastPressedDirIsUpDown) || !isMovH)) 
        {
            vel = movementV * speed;
            dist = vel * Time.deltaTime;
            rBodyPlayer.velocity = new Vector2(0.0f,vel);
            if (movementV.Equals(1))
            {
                if (state != animationStates.UP)
                {
                    state = animationStates.UP;
                    lookingDir = animationStates.UP;
                    animator.SetTrigger("Up");
                }
            }
            else
            {
                if(state != animationStates.DOWN)
                {
                    state = animationStates.DOWN;
                    lookingDir = animationStates.DOWN;
                    animator.SetTrigger("Down");
                }
            }
        }
        else if((!isMovH && !isMovV) || (isMovV && isMovH))
        {         
            dist = 0.0f;
            rBodyPlayer.velocity = Vector2.zero;
            if(state != animationStates.IDLE)
            {
                state = animationStates.IDLE;
                animator.SetTrigger("Idle");
            }
        }
        //Debug.Log("vertical mov: " + isMovV);
        //Debug.Log("horizontal mov: " + isMovH);
        //Debug.Log("Last pressed key is vertical mov: " + lastPressedDirIsUpDown);
        Move(dist);
    }




    public void MoveLeftRight(InputAction.CallbackContext ctx)
    {
        if (ctx.performed)
        {
            movementH = ctx.ReadValue<float>();            
            lastPressedDirIsUpDown = false;
            isMovH = true;
        }
        else if (ctx.canceled)
        {
            isMovH = false;
            if (isMovV)
                lastPressedDirIsUpDown = true;
            movementH = 0;            
        }
    }
    public void MoveUPDown(InputAction.CallbackContext ctx)
    {
        if (ctx.performed)
        {
            movementV = ctx.ReadValue<float>();
            lastPressedDirIsUpDown = true;
            isMovV = true;

        }
        else if (ctx.canceled)
        {
            movementV = 0;
            isMovV = false;
            if (isMovH)
                lastPressedDirIsUpDown = false;
        }
    }
}
