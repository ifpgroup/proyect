﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Entity
{
    // General data
    protected Stats baseStats;
    protected Stats modStats;
    protected EntityAttributes finalAttributes;
    protected int level;
    protected string name;
    protected int ID;
    protected  Deck entityActions;

    protected bool isDead;
    
    public int GetID() => ID;
    public int GetInit() => baseStats.dexterity;
    public Deck GetDeck() => entityActions;
    public EntityAttributes GetAttr => finalAttributes;
    public int GetLevel() => level;
    //protected abstract CardResult[] PlayCard(int index);
    public abstract void DrawCard();
    protected abstract void CalculateAttributes();
    public abstract void Dead();
    public abstract bool CreateDeck();
    public abstract float CalculateDamage(CardResult cardUsed);
    public void Heal(float value)
    {
        finalAttributes.hp += value;
        if (finalAttributes.hp > finalAttributes.maxHp)
            finalAttributes.hp = finalAttributes.maxHp;
        Debug.Log("HEAL -> " + finalAttributes.hp);
    }
    public bool CheckCost(int cost, bool isMana)
    {
        if (isMana)
        {
            if (finalAttributes.mana < cost) return false;
        }
        else
        {
            if (finalAttributes.energy < cost) return false;
        }
        return true;
    }
    public void Regen(bool isMana, float value)
    {
        if (isMana)
        {
            finalAttributes.mana += value;
            if (finalAttributes.mana > finalAttributes.maxMana)
                finalAttributes.mana = finalAttributes.maxMana;
        }
        else
        {
            finalAttributes.energy += value;
            if (finalAttributes.energy > finalAttributes.maxEnergy)
                finalAttributes.energy = finalAttributes.maxEnergy;
        }
    }
    public void Recovery()
    {
        finalAttributes.mana += finalAttributes.manaRecoverPerTurn;
        if (finalAttributes.mana > finalAttributes.maxMana)
        {
            finalAttributes.mana = finalAttributes.maxMana;
        }
        finalAttributes.energy += finalAttributes.energyRecoverPerTurn;
        if (finalAttributes.energy > finalAttributes.maxEnergy)
        {
            finalAttributes.energy = finalAttributes.maxEnergy;
        }
    }
    public void ApplyCost(float cost, bool isMana)
    {
        if (isMana)
        {
            finalAttributes.mana -= cost;
        }
        else
        {
            finalAttributes.energy -= cost;
        }
    }
    public bool TakeDamage(float damage)
    {
        finalAttributes.hp -= damage;
        if (finalAttributes.hp <= 0)
        {
            isDead = true;
            Dead();
            
        }
        return isDead;
    }

}
