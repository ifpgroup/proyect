﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlayer : MonoBehaviour
{
    // Start is called before the first frame update
    private Player player;
    [SerializeField] private Transform spawnLocation;
    [SerializeField] private Transform spawnLocationGoBack;    
    [SerializeField] private MovementController movController;
    [SerializeField] private CamControl camControl;
    [SerializeField] private GameObject finalencounter;
    private RandomEncounter randomEncounter;
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("PlayerManager")?.GetComponent<Player>();
        randomEncounter = GameObject.FindGameObjectWithTag("EncounterController")?.GetComponent<RandomEncounter>();
        if (player)
        {
            GameObject objectSpawned;
            if (player.GetState().goBack)
                objectSpawned = Instantiate(player.GetPartyMemberPrefab(0), spawnLocationGoBack);
            else if (player.GetState().lastLevelName.Equals(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name))
            {
                objectSpawned = Instantiate(player.GetPartyMemberPrefab(0), player.GetState().positionOnMap, Quaternion.identity);
            }
            else
                objectSpawned = Instantiate(player.GetPartyMemberPrefab(0), spawnLocation);
            objectSpawned.GetComponent<UIDHolder>().SetData(player.GetPartyMember(0));
            movController.SetTarget(objectSpawned);
            movController.SetAnimator(objectSpawned.GetComponent<Animator>());
            
            camControl.SetPlayerTransform(objectSpawned.transform);
            if (randomEncounter)
                randomEncounter.SetPlayerTransform(objectSpawned.transform);
            Debug.Log(player.GetPartyMember(0).GetAttr.hp);
            if (finalencounter && (CombatManager.isFixedEncounter || CombatManager.isFixedEncounterDone))
            { finalencounter.SetActive(false);
                CombatManager.isFixedEncounter = false;
                CombatManager.isFixedEncounterDone = true;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
