﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName ="EnemyType", menuName ="Scriptable Object/EnemyType", order = 1)]
public class EnemyType : ScriptableObject
{
    [SerializeField] private Stats baseStats;
    [SerializeField] private Stats statsIncreasePerLevel;
    [SerializeField] private EntityAttributes modBoss;
    [SerializeField] private bool isBoss;
    [SerializeField] private List<Card> actions;
    [SerializeField] private Reward loot;
    [SerializeField] private string nameOfEnemy;

    public string GetEnemyName() => nameOfEnemy;
    public EntityAttributes GetModBoss() => modBoss;
    public bool IsBoss() => isBoss;
    public Reward GetLoot() => loot;
    public List<Card> GetActions() => actions;
    public Stats GetFinalStats(int level)
    {
        Stats finalStats;
        finalStats = baseStats;
        finalStats.strength += statsIncreasePerLevel.strength * level;
        finalStats.wisdom += statsIncreasePerLevel.wisdom * level;
        finalStats.intelligence += statsIncreasePerLevel.intelligence* level;
        finalStats.constitution += statsIncreasePerLevel.constitution * level;
        finalStats.dexterity += statsIncreasePerLevel.dexterity * level;
        finalStats.vitality += statsIncreasePerLevel.vitality * level;

        return finalStats;
    }
    public Stats GetBaseStats() => baseStats;
    public Stats GetIncreasePerLevel => statsIncreasePerLevel;
    //public List<Card> GetActions() => actions;
}
