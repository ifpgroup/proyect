﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DefaultDeck", menuName = "Scriptable Object/Create Deck")]
public class DefaultDeck : ScriptableObject
{
    public List<Card> actions;
}
