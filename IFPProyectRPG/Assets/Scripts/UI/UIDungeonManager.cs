﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIDungeonManager : MonoBehaviour
{
    public GameObject InventoryItemPrefab;
    private UIDHolder playerUIDHolder;
    private Player player;
    public GameObject inventoryPanel;
    public GameObject statusPanel;
    public GameObject hpBarAmount;
    public GameObject mpBarAmount;
    public GameObject energyBarAmount;
    public Transform inventoryContainer;
    public Image playerSprite;
    public TextMeshProUGUI levelText;
    public TextMeshProUGUI[] resourcesAmount;
    public GameObject prefabErrorText;
    public Transform canvasT;
    public GameObject shopPanel;
    public Transform shopContainer;
    public GameObject prefabShopItem;
    public Transform weaponSlot;
    public Transform armorSlot;
    public Transform trinketSlot;
    public Transform infoZone;
    public GameObject infoPopUpPrefab;
    public GameObject prefabDamageNumber;

    void Awake()
    {
        inventoryPanel.SetActive(false);
        statusPanel.SetActive(false);
        if(shopPanel != null)
            shopPanel.SetActive(false);

        player = GameObject.FindGameObjectWithTag("PlayerManager").GetComponent<Player>();
    }

    private void Start()
    {
        player.GetInventory().itemAdded += OnItemAdded;

        GameObject[] aux = GameObject.FindGameObjectsWithTag("Entity");
        foreach (GameObject entity in aux)
        {
            if (entity.GetComponent<UIDHolder>().GetDataCombat()==null || (!entity.GetComponent<UIDHolder>().GetDataCombat().isEnemy))
            {
                playerUIDHolder = entity.GetComponent<UIDHolder>();
                break;
            }
        }
        InitializeBars();
        InitializeInventory();
        InitializeEquipment();
        if(playerSprite)
            playerSprite.sprite = playerUIDHolder.gameObject.GetComponent<SpriteRenderer>().sprite;
    }

    private void InitializeBars()
    {
        UpdatePlayerStatsNoAni(player.GetPartyMember(0).GetAttr);
    }

    private void InitializeEquipment()
    {
        levelText.SetText("Nivel " + player.GetPartyMember(0).GetLevel());

        GameObject aux = Instantiate(InventoryItemPrefab, weaponSlot);
        aux.GetComponent<InventoryItem>().SetMovable(false);
        aux.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = player.GetPartyMember(0).GetEquipment(TypeOfEquipment.Weapon).GetIcon();
        aux.GetComponent<InventoryItem>().SetEquipment(player.GetPartyMember(0).GetEquipment(TypeOfEquipment.Weapon));
        aux.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "";
        aux.GetComponent<InventoryItem>().SetAmount(1);
        aux.GetComponent<ShowTooltip>().SetText(player.GetPartyMember(0).GetEquipment(TypeOfEquipment.Weapon).GetName());
        aux.GetComponent<InventoryItem>().uiDungeonManager = this;
        aux.GetComponent<InventoryItem>().SetItem(player.GetPartyMember(0).GetEquipment(TypeOfEquipment.Weapon));

        aux = Instantiate(InventoryItemPrefab, armorSlot);
        aux.GetComponent<InventoryItem>().SetMovable(false);
        aux.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = player.GetPartyMember(0).GetEquipment(TypeOfEquipment.Armor).GetIcon();
        aux.GetComponent<InventoryItem>().SetEquipment(player.GetPartyMember(0).GetEquipment(TypeOfEquipment.Armor));
        aux.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "";
        aux.GetComponent<InventoryItem>().SetAmount(1);
        aux.GetComponent<ShowTooltip>().SetText(player.GetPartyMember(0).GetEquipment(TypeOfEquipment.Armor).GetName());
        aux.GetComponent<InventoryItem>().uiDungeonManager = this;
        aux.GetComponent<InventoryItem>().SetItem(player.GetPartyMember(0).GetEquipment(TypeOfEquipment.Armor));

        aux = Instantiate(InventoryItemPrefab, trinketSlot);
        aux.GetComponent<InventoryItem>().SetMovable(false);
        aux.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = player.GetPartyMember(0).GetEquipment(TypeOfEquipment.Trinket).GetIcon();
        aux.GetComponent<InventoryItem>().SetEquipment(player.GetPartyMember(0).GetEquipment(TypeOfEquipment.Trinket));
        aux.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "";
        aux.GetComponent<InventoryItem>().SetAmount(1);
        aux.GetComponent<ShowTooltip>().SetText(player.GetPartyMember(0).GetEquipment(TypeOfEquipment.Trinket).GetName());
        aux.GetComponent<InventoryItem>().uiDungeonManager = this;
        aux.GetComponent<InventoryItem>().SetItem(player.GetPartyMember(0).GetEquipment(TypeOfEquipment.Trinket));
    }

    private void InitializeInventory()
    {
        int itemsAmount = player.GetInventory().GetSize();
        bool vacio = false;
        bool weaponFound = false;
        bool armorFound = false;
        bool trinketFound = false;
        int i = 0;
        while(i < itemsAmount && !vacio)
        {
            if (player.GetInventory().GetItem(i) == null)
            {
                vacio = true;
            }
            else
            {
                if (!weaponFound && player.GetInventory().GetItem(i).Equals(player.GetPartyMember(0).GetEquipment(TypeOfEquipment.Weapon)))
                {
                    //Hay más de un item igual (equipamiento equipadito)
                    //Mostramos el item menos uno en amount
                    if (player.GetInventory().GetAmmountItem(i) > 1)
                    {
                        GameObject aux = Instantiate(InventoryItemPrefab, inventoryContainer);
                        aux.GetComponent<InventoryItem>().SetIndex(i);
                        aux.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = player.GetInventory().GetItem(i)?.GetIcon();
                        aux.GetComponent<InventoryItem>().SetItem(player.GetInventory().GetItem(i));
                        aux.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "x" + (player.GetInventory().GetAmmountItem(i) - 1);
                        aux.GetComponent<InventoryItem>().SetAmount(player.GetInventory().GetAmmountItem(i) - 1);
                        aux.GetComponent<ShowTooltip>().SetText(player.GetInventory().GetItem(i)?.GetName());
                        aux.GetComponent<InventoryItem>().uiDungeonManager = this;
                    }
                    i++;
                    weaponFound = true;
                }
                else if (!armorFound && player.GetInventory().GetItem(i).Equals(player.GetPartyMember(0).GetEquipment(TypeOfEquipment.Armor)))
                {
                    //Hay más de un item igual (equipamiento equipadito)
                    //Mostramos el item menos uno en amount
                    if (player.GetInventory().GetAmmountItem(i) > 1)
                    {
                        GameObject aux = Instantiate(InventoryItemPrefab, inventoryContainer);
                        aux.GetComponent<InventoryItem>().SetIndex(i);
                        aux.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = player.GetInventory().GetItem(i)?.GetIcon();
                        aux.GetComponent<InventoryItem>().SetItem(player.GetInventory().GetItem(i));
                        aux.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "x" + (player.GetInventory().GetAmmountItem(i) - 1);
                        aux.GetComponent<InventoryItem>().SetAmount(player.GetInventory().GetAmmountItem(i) - 1);
                        aux.GetComponent<ShowTooltip>().SetText(player.GetInventory().GetItem(i)?.GetName());
                        aux.GetComponent<InventoryItem>().uiDungeonManager = this;
                    }
                    i++;
                    armorFound = true;
                }
                else if (!trinketFound && player.GetInventory().GetItem(i).Equals(player.GetPartyMember(0).GetEquipment(TypeOfEquipment.Trinket)))
                {
                    //Hay más de un item igual (equipamiento equipadito)
                    //Mostramos el item menos uno en amount
                    if (player.GetInventory().GetAmmountItem(i) > 1)
                    {
                        GameObject aux = Instantiate(InventoryItemPrefab, inventoryContainer);
                        aux.GetComponent<InventoryItem>().SetIndex(i);
                        aux.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = player.GetInventory().GetItem(i)?.GetIcon();
                        aux.GetComponent<InventoryItem>().SetItem(player.GetInventory().GetItem(i));
                        aux.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "x" + (player.GetInventory().GetAmmountItem(i) - 1);
                        aux.GetComponent<InventoryItem>().SetAmount(player.GetInventory().GetAmmountItem(i) - 1);
                        aux.GetComponent<ShowTooltip>().SetText(player.GetInventory().GetItem(i)?.GetName());
                        aux.GetComponent<InventoryItem>().uiDungeonManager = this;
                    }
                    i++;
                    trinketFound = true;
                }
                else
                {
                    GameObject aux = Instantiate(InventoryItemPrefab, inventoryContainer);
                    aux.GetComponent<InventoryItem>().SetIndex(i);
                    aux.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = player.GetInventory().GetItem(i)?.GetIcon();
                    aux.GetComponent<InventoryItem>().SetItem(player.GetInventory().GetItem(i));
                    if (player.GetInventory().GetAmmountItem(i) > 0)
                    {
                        aux.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "x" + player.GetInventory().GetAmmountItem(i);
                        aux.GetComponent<InventoryItem>().SetAmount(player.GetInventory().GetAmmountItem(i));
                    }
                    else
                    {
                        aux.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "";
                        aux.GetComponent<InventoryItem>().SetAmount(1);
                    }
                    aux.GetComponent<ShowTooltip>().SetText(player.GetInventory().GetItem(i)?.GetName());
                    aux.GetComponent<InventoryItem>().uiDungeonManager = this;
                    i++;
                }
            }
        }
        /*
        for(int i = 0; i < itemsAmount; i++)
        {
            GameObject aux = Instantiate(InventoryItemPrefab, inventoryContainer);
            aux.GetComponent<InventoryItem>().SetIndex(i);
            aux.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = player.GetInventory().GetItem(i)?.GetIcon();
            aux.GetComponent<InventoryItem>().SetItem(player.GetInventory().GetItem(i));
            if(player.GetInventory().GetAmmountItem(i) > 0)
            {
                aux.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "x" + player.GetInventory().GetAmmountItem(i);
                aux.GetComponent<InventoryItem>().SetAmount(player.GetInventory().GetAmmountItem(i));
            }
            else
            {
                aux.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "";
                aux.GetComponent<InventoryItem>().SetAmount(1);
            }
            aux.GetComponent<ShowTooltip>().SetText(player.GetInventory().GetItem(i)?.GetName());
            aux.GetComponent<InventoryItem>().uiDungeonManager = this;
        }
        */
        resourcesAmount[0].text = "x" + player.GetInventory().GetResourceAmmount(TypeOfResource.Gold).ToString();
        resourcesAmount[1].text = "x" + player.GetInventory().GetResourceAmmount(TypeOfResource.Crystal).ToString();
        resourcesAmount[2].text = "x" + player.GetInventory().GetResourceAmmount(TypeOfResource.Wood).ToString();
        resourcesAmount[3].text = "x" + player.GetInventory().GetResourceAmmount(TypeOfResource.Stone).ToString();
    }

    private void RemoveEquipment()
    {
        foreach (Transform go in weaponSlot)
        {
            Destroy(go.gameObject);
        }
        foreach (Transform go in armorSlot)
        {
            Destroy(go.gameObject);
        }
        foreach (Transform go in trinketSlot)
        {
            Destroy(go.gameObject);
        }
    }

    private void RemoveInventory()
    {
        if (inventoryContainer != null)
        {
            foreach (Transform go in inventoryContainer)
            {
                if(go != null)
                    Destroy(go.gameObject);
            }
        }
    }

    private void UpdatePlayerStatsNoAni(EntityAttributes attr)
    {
        string currentHp;
        string currentMp;
        string currentEnergy;
        if ((attr.hp % 1) == 0)
        {
            currentHp = ((int)attr.hp).ToString();
        }
        else
        {
            currentHp = attr.hp.ToString("F1");
        }
        hpBarAmount.GetComponent<TextMeshProUGUI>().text = currentHp + "/" + attr.maxHp.ToString();
        hpBarAmount.transform.parent.gameObject.GetComponent<HealthBarShrink>().SetHealthAndDamagedHealth((float)attr.hp / attr.maxHp);

        if ((attr.mana % 1) == 0)
        {
            currentMp = ((int)attr.mana).ToString();
        }
        else
        {
            currentMp = attr.mana.ToString("F1");
        }
        mpBarAmount.GetComponent<TextMeshProUGUI>().text = currentMp + "/" + attr.maxMana.ToString();
        mpBarAmount.transform.parent.gameObject.GetComponent<HealthBarShrink>().SetHealthAndDamagedHealth((float)attr.mana / attr.maxMana);

        if ((attr.energy % 1) == 0)
        {
            currentEnergy = ((int)attr.energy).ToString();
        }
        else
        {
            currentEnergy = attr.energy.ToString("F1");
        }
        energyBarAmount.GetComponent<TextMeshProUGUI>().text = currentEnergy + "/" + attr.maxEnergy.ToString();
        energyBarAmount.transform.parent.gameObject.GetComponent<HealthBarShrink>().SetHealthAndDamagedHealth((float)attr.energy / attr.maxEnergy);
    }

    public void ShowPanels()
    {
        statusPanel.SetActive(!statusPanel.activeSelf);
        inventoryPanel.SetActive(!inventoryPanel.activeSelf);
    }

    public bool IsEquipable(Item item)
    {
        return item.CanBeEquip();
    }

    public Item GetItemAtIndex(int id)
    {
        return player.GetInventory().GetItem(id);
    }

    public void EquipItem(Item item)
    {
        //TO DO
    }

    public bool BuyItem(Item item)
    {
        if(CheckBuyable(item))
        {
            player.GetInventory().UseGold(item.GetValue());
            player.GetInventory().AddItem(item);
            UpdateInventory();
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool SellItem(Item item)
    {
        if (CheckSellable(item))
        {
            player.GetInventory().SoldItem(item);
            UpdateInventory();
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool CheckBuyable(Item item)
    {
        int actualGold = player.GetInventory().GetResourceAmmount(TypeOfResource.Gold);
        int cost = item.GetValue();
        if(actualGold >= cost)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool CheckSellable(Item item)
    {
        if (item.CanBeSold())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ShowErrorText(string str)
    {
        GameObject aux = Instantiate(prefabErrorText, canvasT);
        aux.GetComponent<TextMeshProUGUI>().text = str;
    }

    public void UpdateInventory()
    {
        RemoveInventory();
        InitializeInventory();
    }

    public void UpdateEquipment()
    {
        RemoveEquipment();
        InitializeEquipment();
    }

    public void ShowShopPanel()
    {
        shopPanel.SetActive(true);
    }

    public void CloseShop()
    {
        shopPanel.SetActive(false);
    }

    public void UpdateShop(Item item)
    {
        GameObject go = Instantiate(prefabShopItem, shopContainer);
        go.GetComponent<ShopItem>().SetItem(item);
        go.GetComponent<ShopItem>().SetStock(1);
    }

    public void EquipItem(GameObject go, Item e)
    {
        go.transform.SetParent(inventoryContainer);
        player.GetPartyMember(0).ChangeEquipment((Equipment)e);
        UpdateInventory();
        UpdateEquipment();
    }

    public void UpdateMiniStats()
    {
        UpdatePlayerStatsNoAni(player.GetPartyMember(0).GetAttr);
    }    
    
    public void ShowDamageTrap(float damage)
    {
        GameObject aux = Instantiate(prefabDamageNumber, canvasT);
        if ((damage % 1) == 0)
        {
            aux.GetComponent<UpAndFade>().SetText(((int)damage).ToString());
        }
        else
        {
            aux.GetComponent<UpAndFade>().SetText(damage.ToString("F1"));
        }
    }

    private void OnItemAdded(string str, int i)
    {
        ShowInfoPopUp(str + "<color=green> x" + i + "</color>");
        UpdateInventory();
    }

    public void HideToolTip()
    {
        Tooltip.HideTooltip_Static();
    }

    public void ShowInfoPopUp(string str)
    {
        GameObject go = Instantiate(infoPopUpPrefab, infoZone);
        go.GetComponent<FadePopUp>().SetText(str);
    }
}
