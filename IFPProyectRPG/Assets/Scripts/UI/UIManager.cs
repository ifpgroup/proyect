﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour, IUIManager, IUIEffects
{
    //FALTAN COSAS COMO CONTROLAR LA UI DE VIDAS, ETC Y MÁS
    public GameObject panelIzqInfo;
    public GameObject panelCardInfo;
    public GameObject panelGraveyardInfo;
    public GameObject cardsZone;
    public GameObject prefabCard;
    public GameObject panelTurnOrderInfo;
    public GameObject endTurnButton;
    public GameObject panelGlobalPlayerTurn;
    public GameObject currentEntityHP;
    public GameObject currentEntityMP;
    public GameObject currentEntityENERGY;
    public GameObject currentTurnEntityDecoOver;
    public GameObject targetSelectorOverDeco;

    public GameObject debugText;
    public TextMeshProUGUI lootText;

    private TextMeshProUGUI textGraveyardAmount;
    private TextMeshProUGUI textCardInfoTitle;
    private TextMeshProUGUI textCardInfoDesc;
    private TextMeshProUGUI textCardInfoCosts;
    private bool isPlayerTurn;
    private List<Card> currentHand = new List<Card>();
    private int currentTurnUID;

    private int cardBeingUsed;
    private EntityAttributes currentEntityAttr;

    private ICombatManager combatManager;

    private GameObject entityTarget;
    public GameObject prefabDamageNumber;
    private Transform canvasT;
    public GameObject prefabUnusableCardText;
    private int remainingActions;
    private bool supportAction;
    private int actualMana;
    private int actualEnergy;
    public GameObject endCombatPanel;
    private string sceneToLoad;

    private EffectsManager effectsManager;

    void Start()
    {
        panelGlobalPlayerTurn.SetActive(false);
        effectsManager = gameObject.GetComponent<EffectsManager>();
        canvasT = GameObject.FindGameObjectWithTag("Canvas").transform;
        GameObject aux = GameObject.FindGameObjectWithTag("CombatManager");
        if(aux != null)
        {
            combatManager = aux.GetComponent<ICombatManager>();
        }

        if(panelGraveyardInfo != null)
        {
            textGraveyardAmount = panelGraveyardInfo.transform.GetChild(0).GetChild(0).gameObject.GetComponent<TextMeshProUGUI>();
        }
        else
        {
            Debug.LogWarning("BEWARE: 'panelGraveyardInfo' is null (" + this.GetType().FullName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name + ")");
        }

        if(panelCardInfo != null)
        {
            textCardInfoTitle = panelCardInfo.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>();
            textCardInfoDesc = panelCardInfo.transform.GetChild(2).gameObject.GetComponent<TextMeshProUGUI>();
            textCardInfoCosts = panelCardInfo.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>();
        }
        else
        {
            Debug.LogWarning("BEWARE: 'panelCardInfo' is null (" + this.GetType().FullName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name + ")");
        }
    }

    private void CreateCards(List<Card> cards)
    {
        foreach(Card card in cards)
        {
            AddCardToZone(card);
        }
    }

    private void RemoveCards()
    {
        if (cardsZone != null)
        {
            for(int i = 0; i < cardsZone.transform.childCount; i++)
            {
                Destroy(cardsZone.transform.GetChild(i).gameObject);
            }
        }
        else
        {
            Debug.LogWarning("BEWARE: 'cardsZone' is null (" + this.GetType().FullName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name + ")");
        }
    }

    private void AddCardToZone(Card card)
    {
        if(prefabCard != null && cardsZone != null)
        {
            //ANIMACIÓN DE AÑADIR O ROBAR CARTA
            GameObject auxGo;
            auxGo = Instantiate(prefabCard, cardsZone.transform);
            PrepareCard(auxGo, card);
        }
        else
        {
            Debug.LogWarning("BEWARE: 'prefabCard' and/or 'cardsZone' are null (" + this.GetType().FullName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name + ")");
        }
    }

    private void PrepareCard(GameObject go, Card card)
    {
        go.GetComponent<DraggableCard>().SetCard(card);
    }

    private void RemoveCardFromZone(int index)
    {
        if (cardsZone != null)
        {
            //ANIMACIÓN DE MOVERLA AL CEMENTERIO O USARSE O ALGO
            Destroy(cardsZone.transform.GetChild(index).gameObject);
        }
        else
        {
            Debug.LogWarning("BEWARE: 'cardsZone' is null (" + this.GetType().FullName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name + ")");
        }
    }

    public void UpdateCardInfo(int index)
    {
        if (index == -1)
        {
            if (textCardInfoTitle != null && textCardInfoDesc != null)
            {
                textCardInfoTitle.text = "INFO";
                textCardInfoDesc.text = "";
                textCardInfoCosts.text = "";
            }
            else
            {
                Debug.LogWarning(this.name + "BEWARE: 'textCardInfoTitle' and/or 'textCardInfoDesc' are null (" + this.GetType().FullName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name + ")");
            }
        }
        else
        {
            if (textCardInfoTitle != null && textCardInfoDesc != null)
            {
                textCardInfoTitle.text = currentHand[index].GetName();
                textCardInfoDesc.text = currentHand[index].GetDescription();
                textCardInfoCosts.text = "PM: " + currentHand[index].GetCostMana() + " ENERGY: " + currentHand[index].GetCostEnergy();
            }
            else
            {
                Debug.LogWarning(this.name + "BEWARE: 'textCardInfoTitle' and/or 'textCardInfoDesc' are null (" + this.GetType().FullName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name + ")");
            }
        }
    }

    private void UpdateGraveyardCount(int amount)
    {
        if (textGraveyardAmount != null)
        {
            textGraveyardAmount.text = amount.ToString();
        }
        else
        {
            Debug.LogWarning(this.name + "BEWARE: 'textGraveyardAmount' is null (" + this.GetType().FullName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name + ")");
        }
    }

    public void StartTurn(List<Card> hand, bool isEnemy, int graveyardAmount)
    {
        if (isEnemy)
        {
            panelGlobalPlayerTurn.SetActive(false);
            return;
        }

        panelGlobalPlayerTurn.SetActive(true);
        if (hand != null)
        {
            currentHand = hand;
            CreateCards(currentHand);
            UpdateGraveyardCount(graveyardAmount);
            Debug.Log("Hand created (UIManager-StartTurn)");
            Debug.Log("Graveyard created (UIManager-StartTurn)");
        }
        else
        {
            Debug.LogWarning(this.name + "BEWARE: 'hand' is null (" + this.GetType().FullName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name + ")");
        }
    }

    public void PlayCard(int index, Card card, int UIDtarget)
    {
        //RemoveCardFromZone(index); //No es necesario si es la IA (no lo mostramos)
        //TO DO: cambiar la info de acciones disponibles, mostrar animación/efectos, etc
        GameObject[] entities;
        entities = GameObject.FindGameObjectsWithTag("Entity");
        foreach(GameObject entity in entities)
        {
            if(entity.GetComponent<UIDHolder>().GetUID() == UIDtarget)
            {
                entityTarget = entity;
            }
        }

        CreateEffect(card, Camera.main.WorldToScreenPoint(entityTarget.transform.position));
    }

    public void PlayCard(int index, Card card, List<int> UIDtargets)
    {
        //RemoveCardFromZone(index); //No es necesario si es la IA (no lo mostramos)
        //TO DO: cambiar la info de acciones disponibles, mostrar animación/efectos, etc
    }

    public void EndTurn()
    {
        RemoveCards();
        //TO DO
    }

    public void CardResultsAttack(float damage)
    {
        if (entityTarget != null)
        {
            Vector2 pos = new Vector2(Camera.main.WorldToScreenPoint(entityTarget.transform.position).x, Camera.main.WorldToScreenPoint(entityTarget.transform.position).y + 50);
            GameObject aux = Instantiate(prefabDamageNumber, pos, Quaternion.identity, canvasT);
            if((damage % 1) == 0)
            {
                aux.GetComponent<UpAndFade>().SetText(((int)damage).ToString());
            }
            else
            {
                aux.GetComponent<UpAndFade>().SetText(damage.ToString("F1"));
            }
            

            float normalizedNewHealth = entityTarget.GetComponent<UIDHolder>().GetDataCombat().GetAttr().hp / entityTarget.GetComponent<UIDHolder>().GetDataCombat().GetAttr().maxHp;
            entityTarget.GetComponent<StatsMiniBar>().OnDamageEffect(normalizedNewHealth);
            if(normalizedNewHealth <= 0)
            {
                entityTarget.gameObject.SetActive(false);
            }
        }
    }

    public void CardResult(CardResult result, EntityAttributes attr, bool playerAttr)
    {
        if (result.TypeOfEffect == TypeOfEffect.Healing)
        {
            if (entityTarget != null)
            {
                Vector2 pos = new Vector2(Camera.main.WorldToScreenPoint(entityTarget.transform.position).x, Camera.main.WorldToScreenPoint(entityTarget.transform.position).y + 50);
                GameObject aux = Instantiate(prefabDamageNumber, pos, Quaternion.identity, canvasT);
                aux.GetComponent<TextMeshProUGUI>().color = Color.green;
                if ((result.value % 1) == 0)
                {
                    aux.GetComponent<UpAndFade>().SetText(((int)result.value).ToString());
                }
                else
                {
                    aux.GetComponent<UpAndFade>().SetText(result.value.ToString("F1"));
                }

                float normalizedNewHealth = entityTarget.GetComponent<UIDHolder>().GetDataCombat().GetAttr().hp / entityTarget.GetComponent<UIDHolder>().GetDataCombat().GetAttr().maxHp;
                entityTarget.GetComponent<StatsMiniBar>().OnHealEffect(normalizedNewHealth);
                if (!entityTarget.gameObject.GetComponent<UIDHolder>().GetDataCombat().isEnemy)
                {
                    if (playerAttr)
                    {
                        UpdatePlayerStatsNoAni(attr);
                    }
                }
            }
        }

        if (result.TypeOfEffect == TypeOfEffect.RegenMana)
        {
            if (entityTarget != null)
            {
                Vector2 pos = new Vector2(Camera.main.WorldToScreenPoint(entityTarget.transform.position).x, Camera.main.WorldToScreenPoint(entityTarget.transform.position).y + 50);
                GameObject aux = Instantiate(prefabDamageNumber, pos, Quaternion.identity, canvasT);
                aux.GetComponent<TextMeshProUGUI>().color = Color.blue;
                if ((result.value % 1) == 0)
                {
                    aux.GetComponent<UpAndFade>().SetText(((int)result.value).ToString());
                }
                else
                {
                    aux.GetComponent<UpAndFade>().SetText(result.value.ToString("F1"));
                }

                //float normalizedNewHealth = entityTarget.GetComponent<UIDHolder>().GetDataCombat().GetAttr().hp / entityTarget.GetComponent<UIDHolder>().GetDataCombat().GetAttr().maxHp;
                //entityTarget.GetComponent<StatsMiniBar>().OnHealEffect(normalizedNewHealth);
                
                if (!entityTarget.gameObject.GetComponent<UIDHolder>().GetDataCombat().isEnemy)
                {
                    if (playerAttr)
                    {
                        UpdatePlayerStatsNoAni(attr);
                    }
                }
            }
        }

        if (result.TypeOfEffect == TypeOfEffect.RegenEnergy)
        {
            if (entityTarget != null)
            {
                Vector2 pos = new Vector2(Camera.main.WorldToScreenPoint(entityTarget.transform.position).x, Camera.main.WorldToScreenPoint(entityTarget.transform.position).y + 50);
                GameObject aux = Instantiate(prefabDamageNumber, pos, Quaternion.identity, canvasT);
                aux.GetComponent<TextMeshProUGUI>().color = Color.yellow;
                if ((result.value % 1) == 0)
                {
                    aux.GetComponent<UpAndFade>().SetText(((int)result.value).ToString());
                }
                else
                {
                    aux.GetComponent<UpAndFade>().SetText(result.value.ToString("F1"));
                }

                //float normalizedNewHealth = entityTarget.GetComponent<UIDHolder>().GetDataCombat().GetAttr().hp / entityTarget.GetComponent<UIDHolder>().GetDataCombat().GetAttr().maxHp;
                //entityTarget.GetComponent<StatsMiniBar>().OnHealEffect(normalizedNewHealth);
                if (!entityTarget.gameObject.GetComponent<UIDHolder>().GetDataCombat().isEnemy)
                {
                    if (playerAttr)
                        UpdatePlayerStatsNoAni(attr);
                }
            }
        }
    }

    public void GetEntityData(EntityAttributes attr, int actionsRemaining, bool hasSupportAction, int UID, bool isPlayer=false)
    {
        ///Added this method so you can ge tht entities info in their turn so you can update whatever you use internally to show this info, the struct contains all the hp/energy/mana/armor values of the creatures, as well as the actions
        ///Finally the UID so you can know which entity it is.
        ///also included a bool so you can know if its a player info
        
        debugText.GetComponent<TextMeshProUGUI>().text = "Is player turn -> " + isPlayer;
        debugText.GetComponent<TextMeshProUGUI>().text += "\nActions -> " + actionsRemaining;
        debugText.GetComponent<TextMeshProUGUI>().text += "\nHealth -> " + attr.hp + "/" + attr.maxHp;
        debugText.GetComponent<TextMeshProUGUI>().text += "\nUID -> " + UID;
        isPlayerTurn = isPlayer;

        UpdatePlayerStatsNoAni(attr);
        currentEntityAttr = attr;
        remainingActions = actionsRemaining;
        supportAction = hasSupportAction;
        currentTurnUID = UID;

        GameObject[] entities;
        entities = GameObject.FindGameObjectsWithTag("Entity");
        foreach (GameObject entity in entities)
        {
            if (entity.GetComponent<UIDHolder>().GetUID() == currentTurnUID)
            {
                Vector2 pos;
                if (isPlayerTurn)
                {
                    pos = Camera.main.WorldToScreenPoint(new Vector2(entity.transform.position.x, entity.transform.position.y + 1f));
                }
                else
                {
                    pos = Camera.main.WorldToScreenPoint(entity.transform.position);
                }
                ShowCurrentTurnDecoOver(true, pos);

                if (remainingActions == 0 && !supportAction)
                {
                    StunEffect(entity, true);
                }
                else
                {
                    StunEffect(entity, false);
                }
            }
        }
    }

    public EntityAttributes GetCurrentEntityAttr()
    {
        return currentEntityAttr;
    }

    private void UpdatePlayerStatsManaAndEnergy(EntityAttributes attr)
    {
        if (isPlayerTurn)
        {
            string currentMp;
            string currentEnergy;

            if ((attr.mana % 1) == 0)
            {
                currentMp = ((int)attr.mana).ToString();
            }
            else
            {
                currentMp = attr.mana.ToString("F1");
            }
            currentEntityMP.GetComponent<TextMeshProUGUI>().text = currentMp + "/" + attr.maxMana.ToString();
            currentEntityMP.transform.parent.gameObject.GetComponent<HealthBarShrink>().OnDamage((float)attr.mana / attr.maxMana);

            if ((attr.energy % 1) == 0)
            {
                currentEnergy = ((int)attr.energy).ToString();
            }
            else
            {
                currentEnergy = attr.energy.ToString("F1");
            }
            currentEntityENERGY.GetComponent<TextMeshProUGUI>().text = currentEnergy + "/" + attr.maxEnergy.ToString();
            currentEntityENERGY.transform.parent.gameObject.GetComponent<HealthBarShrink>().OnDamage((float)attr.energy / attr.maxEnergy);
        }
    }

    private void UpdatePlayerStatsNoAni(EntityAttributes attr)
    {
        if (isPlayerTurn)
        {
            string currentHp;
            string currentMp;
            string currentEnergy;
            if ((attr.hp % 1) == 0)
            {
                currentHp = ((int)attr.hp).ToString();
            }
            else
            {
                currentHp = attr.hp.ToString("F1");
            }
            currentEntityHP.GetComponent<TextMeshProUGUI>().text = currentHp + "/" + attr.maxHp.ToString();
            currentEntityHP.transform.parent.gameObject.GetComponent<HealthBarShrink>().SetHealthAndDamagedHealth((float)attr.hp / attr.maxHp);

            if ((attr.mana % 1) == 0)
            {
                currentMp = ((int)attr.mana).ToString();
            }
            else
            {
                currentMp = attr.mana.ToString("F1");
            }
            currentEntityMP.GetComponent<TextMeshProUGUI>().text = currentMp + "/" + attr.maxMana.ToString();
            currentEntityMP.transform.parent.gameObject.GetComponent<HealthBarShrink>().SetHealthAndDamagedHealth((float)attr.mana / attr.maxMana);

            if ((attr.energy % 1) == 0)
            {
                currentEnergy = ((int)attr.energy).ToString();
            }
            else
            {
                currentEnergy = attr.energy.ToString("F1");
            }
            currentEntityENERGY.GetComponent<TextMeshProUGUI>().text = currentEnergy + "/" + attr.maxEnergy.ToString();
            currentEntityENERGY.transform.parent.gameObject.GetComponent<HealthBarShrink>().SetHealthAndDamagedHealth((float)attr.energy / attr.maxEnergy);
        }
    }

    public void OnClickEndTurn()
    {
        EndTurn();
        if (isPlayerTurn)
        {
            combatManager.EndPlayerTurn();
        }
    }

    public Card GetTouchedCard(int index)
    {
        return currentHand[index];
    }

    public void UIPlayCard(int index, Card card, int UIDtarget, bool isBasic)
    {
        GameObject[] entities;
        entities = GameObject.FindGameObjectsWithTag("Entity");
        foreach (GameObject entity in entities)
        {
            if (entity.GetComponent<UIDHolder>().GetUID() == UIDtarget)
            {
                entityTarget = entity;
            }
        }
        combatManager.PlayCardOnTarget(index, UIDtarget);
        currentEntityAttr.mana -= card.GetCostMana();
        currentEntityAttr.energy -= card.GetCostEnergy();
        UpdatePlayerStatsManaAndEnergy(currentEntityAttr);
        RemoveCardFromZone(index);

        CreateEffect(card, Camera.main.WorldToScreenPoint(entityTarget.transform.position));
    }

    public void GetAllUIDS(List<int> playerUIDS, List<int> enemiesUIDS)
    {
        throw new System.NotImplementedException();
    }

    public void ShowUnusableCard()
    {
        Instantiate(prefabUnusableCardText, canvasT);
    }

    public int GetCurrentEntityRemainingActions()
    {
        return remainingActions;
    }

    public bool GetCurrentEntityHasSupportAction()
    {
        return supportAction;
    }

    private void ShowCurrentTurnDecoOver(bool b, Vector2 pos)
    {
        Vector2 v = new Vector2(pos.x, pos.y + 65);
        currentTurnEntityDecoOver.SetActive(b);
        currentTurnEntityDecoOver.transform.position = v;
    }

    private void StunEffect(GameObject go, bool b)
    {
        go.GetComponent<UIDHolder>().ShowStunEffect(b);
    }

    public void ShowTargetDeco(bool b, Vector2 pos)
    {
        Vector2 v = new Vector2(pos.x, pos.y);
        targetSelectorOverDeco.SetActive(b);
        targetSelectorOverDeco.transform.position = v;
    }

    public void EndCombat(string sceneToLoad, bool victory, Reward loot)
    {
        endCombatPanel.SetActive(true);
        if (!victory)
        {
            endCombatPanel.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "¡HAS PERDIDO!";
            lootText.SetText("Has muerto, no hay recompensa :(");
        }
        else
        {
            string str = "";
            if(loot.MaxGold > 0)
            {
                str += "<color=orange>+" + loot.MaxGold + " oro</color>";
            }
            if (loot.maxCrystals > 0)
            {
                str += "\n<color=blue>+" + loot.maxCrystals + " cristales</color>";
            }
            if(loot.possibleCards != null && loot.possibleCards.Count > 0)
            {
                foreach(Card card in loot.possibleCards)
                {
                    str += "\nNueva carta: <color=purple>" + card.GetName() + "</color>";
                }
            }
            if (loot.possibleValuableItemDrop != null && loot.possibleValuableItemDrop.Count > 0)
            {
                foreach (Item i in loot.possibleValuableItemDrop)
                {
                    str += "\n<color=white>" + i.GetName() + "</color>";
                }
            }
            lootText.SetText(str);
        }
        this.sceneToLoad = sceneToLoad;
    }

    public void StartCombat()
    {

    }

    public void SetCurrentEntityRemainingActions(int n)
    {
        remainingActions = n;
    }

    public void SetCurrentEntityHasSupportAction(bool b)
    {
        supportAction = true;
    }

    public void ChangeToScene()
    {
        SceneManager.LoadScene(sceneToLoad);
    }

    public void AnimationEnded()
    {
        combatManager.CardAnimationOver();
    }

    private void CreateEffect(Card card, Vector2 pos)
    {
        //TO DO: añadir al scriptable object de carta un campo para su propia animación, si no tiene, usa las por defecto
        switch (card.GetCardType()==CardType.Offensive)
        {
            case true:
                //Si es ofensiva y mágica
                if (card.GetAttribute() == CardAttribute.Intelligence)
                {
                    effectsManager.CreateEffect(1, pos);
                }
                //Si es ofensiva y física/neutra
                else
                {
                    effectsManager.CreateEffect(0, pos);
                }
                break;
            case false:
                //Si no es ofensiva
                //Si es de soporte
                if (card.GetCardType() == CardType.Support)
                {
                    effectsManager.CreateEffect(2, pos);
                }
                //Si es defensiva
                else
                {
                    effectsManager.CreateEffect(3, pos);
                }
                break;
        }
    }
}
