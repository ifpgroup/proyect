﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableItems : MonoBehaviour
{
    [SerializeField] private List<InteractableItem> listItems;
    [SerializeField] private List<TrapData> trapsDatas;
    [SerializeField] private List<TreasureData> treasureDatas;   
    static private Dictionary<int, bool> trapActive;

    private void Awake()
    {
        if (trapActive == null) trapActive = new Dictionary<int, bool>();
    }
    public static void Reset()
    {
        trapActive = new Dictionary<int, bool>();
    }
    private void Start()
    {
        
        for (int i = 0; i < listItems.Count; i++)
        {
            if (trapActive.ContainsKey(i))
            {
                if (listItems[i].GetTypeOfItem() == TypeOfInteractableItem.DOOR)
                {
                    listItems[i].PlayAction();
                }
                else
                    listItems[i].gameObject.SetActive(false);
                continue;
            }
            int temp=i;
            switch (listItems[i].GetTypeOfItem())
            {
                case TypeOfInteractableItem.DOOR:
                    listItems[i].SetData(null, temp);
                    break;
                case TypeOfInteractableItem.STAIRS:
                    break;
                case TypeOfInteractableItem.RESOURCENODE:
                    break;
                case TypeOfInteractableItem.TREASURE:
                    var randTreasure = Random.Range(0, treasureDatas.Count);
                    listItems[i].SetData(treasureDatas[randTreasure],temp);
                    break;
                case TypeOfInteractableItem.TRAP:
                    var randTrap = Random.Range(0, trapsDatas.Count);
                    
                    listItems[i].SetData(trapsDatas[randTrap],temp);
                    break;
                default:
                    break;
            }
        }
    }
    static public void SetActive(int item)
    {
        if (trapActive == null)
            trapActive = new Dictionary<int, bool>();
        if (trapActive.ContainsKey(item))
            return;
        trapActive.Add(item, true);
    }
}
