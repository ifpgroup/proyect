﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct Choice
{
	[TextArea(2, 5)]
	public string text;
	public Conversation conversation;
}

[CreateAssetMenu(fileName ="Choices", menuName = "Scriptable Object/Dialogue/Choices", order = 5)]
public class Question : ScriptableObject
{
	[TextArea(2, 5)]
	public string text;
	public Choice[] choices;
}
