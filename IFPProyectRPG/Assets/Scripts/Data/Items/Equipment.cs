﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeOfEquipment { Weapon, Armor, Trinket }
[CreateAssetMenu(fileName = "Equipment", menuName = "Scriptable Object/Create Equipment")]
public class Equipment : Item
{
    [SerializeField] private Card equipmentAction;
    [SerializeField] private Stats moddedStats;
    [SerializeField] private EntityAttributes bonusAttr;
    [SerializeField] private TypeOfEquipment typeOfEquipment;
    public EntityAttributes GetAttr() => bonusAttr;
    public Stats GetStats() => moddedStats;
    public Card GetAction() => equipmentAction;
    public TypeOfEquipment GetTypeOfEquipment() => typeOfEquipment;
        
}
