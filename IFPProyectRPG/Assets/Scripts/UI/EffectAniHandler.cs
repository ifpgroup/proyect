﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectAniHandler : MonoBehaviour
{
    private Animator animator;
    private EffectsManager effectsManager;

    private void Awake()
    {
        animator = gameObject.GetComponent<Animator>();
    }

    private void Update()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !animator.IsInTransition(0))
        {
            effectsManager.AnimationEnded();
            Destroy(gameObject);
        }
    }

    public void SetManager(EffectsManager em)
    {
        effectsManager = em;
    }
}
