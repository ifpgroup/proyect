﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : InteractableItem
{
    private TrapData data;
    private Player player;
    private AudioController sound;
    private UIDungeonManager uiDungeonManager;
    private int index;

    public override void PlayAction()
    {
        gameObject.GetComponent<Animator>().SetTrigger("isTriggered");
        if (sound) sound.PlayAttack();
        foreach (var partyMember in player.GetParty())
        {
            if (partyMember.TakeDamage(data.GetDamage()))
            {
                // PartyMember killed do something??? 

            }
        }
        if (uiDungeonManager != null)
        {
            uiDungeonManager.UpdateMiniStats();
            uiDungeonManager.ShowDamageTrap(data.GetDamage());
        }
    }

    public override void SetData(ScriptableObject data, int i)
    {
        this.data = data as TrapData;
        index = i;
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("PlayerManager")?.GetComponent<Player>();
        uiDungeonManager = GameObject.FindGameObjectWithTag("UIDungeonManager")?.GetComponent<UIDungeonManager>();
        sound = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
    }
    public void FinishAnimation()
    {
        gameObject.SetActive(false);
        InteractableItems.SetActive(index);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
