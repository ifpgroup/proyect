﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck
{
    private Entity owner;
    private List<Card> equipmentCards;
    private List<Card> cardsInUse; // all the cards present in the deck
    private List<Card> cardsUsed; // the cementery
    private Queue<Card> activeCards; // The cards not on hand or in the cementary
    private List<Card> hand;// cards on the entity hand
    private int maxSize;
    private int handSize;
    private readonly int weaponPos = 0;
    private readonly int armorPos = 1;
    private readonly int trinketPos = 2;
    public List<Card> GetCementery() => cardsUsed;
    public List<Card> GetHand() => hand;
    public Card[] GetCardsRemaining() => activeCards.ToArray();

    public List<Card> GetCards() => cardsInUse;
    public Deck(Entity ownerOfDeck, List<Card> actions, int maxSize=10,int handSize=3)
    {
        equipmentCards = new List<Card>();
        cardsInUse = new List<Card>();
        cardsUsed = new List<Card>();
        activeCards = new Queue<Card>();
        hand = new List<Card>();
        owner = ownerOfDeck;
        this.maxSize = maxSize;
        this.handSize = handSize;
        int i = 0;
        foreach (var card in actions)
        {            
            AddCard(card, i);
            i++;
        }
    }
    public Deck(Entity ownerOfDeck, List<Card> actions, Equipment weapon, Equipment armor, Equipment trinket,int maxSize = 10, int handSize = 3)
    {
        equipmentCards = new List<Card>();
        cardsInUse = new List<Card>();
        cardsUsed = new List<Card>();
        activeCards = new Queue<Card>();
        hand = new List<Card>();
        owner = ownerOfDeck;
        this.maxSize = maxSize;
        this.handSize = handSize;
        int i = 0;
        equipmentCards.Add(weapon.GetAction());
        equipmentCards.Add(armor.GetAction());
        equipmentCards.Add(trinket.GetAction());
        //AddCard(weapon.GetAction(),weaponPos);
        //AddCard(armor.GetAction(), armorPos);
        //AddCard(trinket.GetAction(), trinketPos);
        foreach (var card in actions)
        {
            if (i >= maxSize-equipmentCards.Count) break;
            AddCard(card, i);
            i++;
        }
    }
    public void SetNewHandSize(int value) => handSize = value;
    public void SetNewDeckSize(int value) => maxSize = value;
    public void StartCombat()
    {
        hand = new List<Card>();
        activeCards = new Queue<Card>();
        cardsUsed = new List<Card>();
        for (int i =0;i<cardsInUse.Count;i++) //cardsInUse.Count-1; i >= 0; i--)
        {
            activeCards.Enqueue(cardsInUse[i]);
        }
        for (int i = 0; i < handSize; i++)
        {
            if (equipmentCards.Count > 0 && i < equipmentCards.Count)
                hand.Add(equipmentCards[i]);
            else
                hand.Add(activeCards.Dequeue());
        }
    }
    public bool AddCard(Card cardToAdd, int positionToBeAdded)
    {        
        if (positionToBeAdded >= maxSize)
            return false;
        else
        {
            if (positionToBeAdded == cardsInUse.Count)
                cardsInUse.Add(cardToAdd);
            else if (positionToBeAdded < cardsInUse.Count)
                cardsInUse[positionToBeAdded] = cardToAdd;
            else
                return false;
        }
        /// missing checks of being able to add the card because of limits;
        return true;
    }
    public void DrawCard()
    {
        if (activeCards.Count > 0)
        {
            //Debug.Log(hand.Count);
            //Debug.Log(handSize);
            if(hand.Count < handSize)
                hand.Add(activeCards.Dequeue());
        }            
        else
        {
            RestoreDeck();
            hand.Add(activeCards.Dequeue());
        }
            
    }
    public CardResult PlayCard(int index)
    {
        var result = hand[index].PlayCard(owner.GetAttr);
        cardsUsed.Add(hand[index]);
        hand.RemoveAt(index);
        return result;

    }
    public List<Card> GetEquipment() {
        List<Card> cards = new List<Card>(); ;
        foreach (var item in equipmentCards)
        {
            cards.Add(item);
        }
        return cards;
    }
    public bool UnequipCard(Card card)
    {
        Debug.Log(card.GetName());
        if (cardsInUse.Contains(card))
        {
            cardsInUse.Remove(card);
            return true;
        }
        else 
            return false;
    }
    public bool EquipCard(Card card)
    {
        Debug.Log("is this working?");
        Debug.Log(cardsInUse.Count + equipmentCards.Count);
        if (cardsInUse.Count + equipmentCards.Count < maxSize)
        {
            cardsInUse.Add(card);
            Debug.Log("is this working?");
            return true;
        }
        else
            return false;
    }
    private void RestoreDeck()
    {
        if (activeCards.Count > 0)
            return;
        for (int i = 0; i < cardsUsed.Count; i++)
        {
            int randPos = UnityEngine.Random.Range(0, cardsUsed.Count);
            var cardToReplace = cardsUsed[randPos];
            cardsUsed[randPos] = cardsUsed[i];
            cardsUsed[i] = cardToReplace;
        }
        for (int i = 0; i < cardsUsed.Count; i++)
        {
            activeCards.Enqueue(cardsUsed[i]);
        }
        Debug.Log("Restore deck active cards enqueque " + activeCards.Count);
        cardsUsed.Clear();
        Debug.Log("Restore deck active cards enqueque after clear the cardsUsed" + activeCards.Count);
        Debug.Log("Here Resotre");
    }
    public int GetMaxSize() => maxSize;
    public bool EquipEquipment(Equipment equipment)
    {
        Card action = equipment.GetAction();
        switch (equipment.GetTypeOfEquipment())
        {
            case TypeOfEquipment.Weapon:
                equipmentCards[weaponPos] = action;
                break;
            case TypeOfEquipment.Armor:
                equipmentCards[armorPos] = action;
                break;
            case TypeOfEquipment.Trinket:
                equipmentCards[trinketPos] = action;
                break;
            default:
                return false;
        }
        return true;
    }
}
