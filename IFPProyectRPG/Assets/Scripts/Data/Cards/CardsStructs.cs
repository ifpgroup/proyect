﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
// Create the structs for the cards here
[System.Serializable]
public struct CardResult
{
    public CardType CardType;
    public CardAttribute CardAttribute;
    public float value;
    public float cost;
    public effectResult newValue;
    public cardCost newCost;
    public TypeOfEffect TypeOfEffect;
    public Dictionary<TypeOfEffect, float> specialEffects;
    public Dictionary<TypeOfEffect, effectResult> newSpecialEffects;
};
[System.Serializable]
public struct effectResult
{
    public float value;
    public int duration;
}
[System.Serializable]
public struct cardCost
{
    public float costMana;
    public float costEnergy;
    public float costHP;
}
[System.Serializable]
public struct CardEffect
{
    public bool isOffensive;
    public bool isUsingDice;
    public bool isSupport;
    public bool isSingleTarget;
    public int numTargets;
    public int numDices;
    public bool isAddingSpecialEffects;
    public int costEnergy;
    public int costMana;
    public List<TypeOfEffect> keyEffects;
    public List<float> valueEffects;
    public Dictionary<TypeOfEffect, float> effects;
    //Offensive
    public float damageRoll;
    public float fixedValue;
    public float mult;
    //Defensive/support
    public float damageBlockRoll;
    public float hpHeal;
    public float armorGiven;
    public float magicArmorGiven;
    public List<CardAttribute> keyDamageReduction;
    public List<float> valueDamageReduction;
    public Dictionary<CardAttribute, float> damageReduction;
    public List<CardAttribute> keyExtraEffect;
    public List<float> valueExtraEffect;    
    public bool increaseNumberActions;
    public Dictionary<CardAttribute, float> extraEffect;
};