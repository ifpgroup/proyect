﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsManager : MonoBehaviour
{
    private GameObject currentAni;
    private IUIEffects uimanager;
    private Transform canvasT;
    public List<GameObject> effects;

    private void Awake()
    {
        canvasT = GameObject.FindGameObjectWithTag("Canvas").transform;
        uimanager = gameObject.GetComponent<UIManager>();
    }

    public void CreateEffect(int id, Vector2 pos)
    {
        currentAni = Instantiate(effects[id], pos, Quaternion.identity, canvasT);
        currentAni.GetComponent<EffectAniHandler>().SetManager(this);
    }

    public void AnimationEnded()
    {
        uimanager.AnimationEnded();
    }
}
