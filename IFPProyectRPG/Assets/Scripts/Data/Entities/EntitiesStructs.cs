﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StatsNames { Wisdom, Strength, Intelligence, Constitution, Dexterity, Vitality };
[System.Serializable]
public struct Stats
{
    public int strength;
    public int wisdom;
    public int intelligence;
    public int constitution;
    public int dexterity;
    public int vitality;

    public static bool operator >=(Stats a, Stats b)
    {
        if (a.constitution < b.constitution)
            return false;
        if (a.strength < b.strength)
            return false;
        if (a.wisdom < b.wisdom)
            return false;
        if (a.intelligence < b.intelligence)
            return false;
        if (a.dexterity < b.dexterity)
            return false;
        if (a.vitality < b.vitality)
            return false;
        return true;
    }
    public static bool operator <=(Stats a, Stats b)
    {
        if (a.constitution > b.constitution)
            return false;
        if (a.strength > b.strength)
            return false;
        if (a.wisdom > b.wisdom)
            return false;
        if (a.intelligence > b.intelligence)
            return false;
        if (a.dexterity > b.dexterity)
            return false;
        if (a.vitality > b.vitality)
            return false;
        return true;
    }
    public static Stats operator + (Stats a, Stats b)
    {
        Stats finalSum = new Stats();
        finalSum.constitution = a.constitution + b.constitution;
        finalSum.strength = a.strength + b.strength;
        finalSum.dexterity = a.dexterity + b.dexterity;
        finalSum.vitality = a.vitality + b.vitality;
        finalSum.wisdom = a.wisdom + b.wisdom;
        finalSum.intelligence = a.intelligence + b.intelligence;
        return finalSum;
    }
}
[System.Serializable]
public struct EntityAttributes
{
    public float hp;
    public float maxHp;
    public int armor;
    public int mArmor;
    public float energy;
    public float maxEnergy;
    public float mana;
    public float maxMana;
    public float strMult;
    public float intMult;
    public int energyRecoverPerTurn;
    public int manaRecoverPerTurn;
    public static EntityAttributes operator + (EntityAttributes a, EntityAttributes b)
    {
        EntityAttributes finalAttr = new EntityAttributes();
        finalAttr.hp = a.hp + b.hp;
        finalAttr.maxHp = a.maxHp + b.maxHp;
        finalAttr.armor = a.armor + b.armor;
        finalAttr.mArmor = a.mArmor + b.mArmor;
        finalAttr.energy = a.energy + b.energy;
        finalAttr.mana = a.mana + b.mana;
        finalAttr.maxEnergy = a.maxEnergy + b.maxEnergy;
        finalAttr.maxMana = a.maxMana + b.maxMana;
        finalAttr.strMult = a.strMult + b.strMult;
        finalAttr.intMult = a.intMult+ b.intMult;
        finalAttr.energyRecoverPerTurn= a.energyRecoverPerTurn + b.energyRecoverPerTurn;
        finalAttr.manaRecoverPerTurn = a.manaRecoverPerTurn + b.manaRecoverPerTurn;
        return finalAttr;
    }
}

[System.Serializable]
public struct Reward
{
    public int MaxGold;
    public int maxCrystals;
    public float cardDropProbability;
    public List<Card> possibleCards;
    public float valuableItemDropProbability;
    public List<Item> possibleValuableItemDrop;

}
