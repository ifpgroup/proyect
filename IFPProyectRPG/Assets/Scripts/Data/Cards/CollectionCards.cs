﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public struct CardCollectionData
{
    public Dictionary<PlayableCharacter, int> inUsePerPlayer;
    public int inUseTotal;
    public int ammountCollected;
    public int maxAmmountInUse;
    public int maxAmmountInUsePerPlayer;
    public bool isUnlocked;
    public void UnlockCard() {
        Debug.Log("Initial unlocked: "+isUnlocked);
        isUnlocked = true;
        Debug.Log("Final unlocked: " + isUnlocked);
    }
    public void SetMaxNumber(int max) => maxAmmountInUse = max;
    public void AddCard() => ammountCollected++;
    public void AddCard(int ammount) => ammountCollected+=ammount;
    public int GetNumInUsePlayer(PlayableCharacter character)
    {
        if(inUsePerPlayer == null)
        {
            inUsePerPlayer = new Dictionary<PlayableCharacter, int>();
            return -1;
        }
        if (inUsePerPlayer.ContainsKey(character))
        {
            return inUsePerPlayer[character];
        }
        else
        {
            return -1;
        }
    }
    public bool UnequipCard(PlayableCharacter character)
    {
        if (!inUsePerPlayer.ContainsKey(character)) return false;
        if (inUsePerPlayer[character] <= 0) return false;
        inUsePerPlayer[character]--;
        inUseTotal--;
        return true;
    }
    public bool UseCard(PlayableCharacter character)
    {
        if (!isUnlocked) return false;
        if(maxAmmountInUse==-1)
        {
            if (inUsePerPlayer.ContainsKey(character))
            {
                inUseTotal++;
                inUsePerPlayer[character]++;
                return true;
            }
            else
            {
                inUseTotal++;
                if (inUsePerPlayer == null) inUsePerPlayer = new Dictionary<PlayableCharacter, int>();
                inUsePerPlayer.Add(character, 1);
                return true;
            }
        }
        if (inUseTotal < maxAmmountInUse && inUseTotal<ammountCollected)
        {
            if (inUsePerPlayer.ContainsKey(character))
            {
                if (inUsePerPlayer[character] < maxAmmountInUsePerPlayer)
                {
                    inUseTotal++;
                    inUsePerPlayer[character]++;
                    return true;
                }
                return false;
            }
            else
            {
                inUseTotal++;
                if (inUsePerPlayer == null) inUsePerPlayer = new Dictionary<PlayableCharacter, int>();
                inUsePerPlayer.Add(character, 1);
                return true;
            }
        }
        return false;
    }
}
public class CollectionCards : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private List<Card> allCards;    
    private Dictionary<Card, CardCollectionData> cardsData;
    void Start()
    {
        cardsData = new Dictionary<Card, CardCollectionData>();
        foreach (var card in allCards)
        {
            CardCollectionData data = new CardCollectionData();
            data.inUsePerPlayer = new Dictionary<PlayableCharacter, int>();
            data.maxAmmountInUse = card.GetRarity().GetMaxUseInTotal();
            data.maxAmmountInUsePerPlayer = card.GetRarity().GetMaxUsePerPlayer();
            if(data.maxAmmountInUse==-1)
                data.ammountCollected = -1;
            else
                data.ammountCollected = 0;
            data.inUseTotal = 0;
            data.isUnlocked = false;
            cardsData.Add(card, data);
        }
    }
    public void UnlockCards(List<PlayableCharacter> party)
    {
        
        foreach (var cardKey in allCards)
        {
            if (cardsData[cardKey].isUnlocked) continue;
            foreach (var character in party)
            {
                Debug.Log("UNlocking");
                if (cardKey.UnlockCard(character.GetStats()))
                {
                    
                    var tmp = cardsData[cardKey];
                    tmp.UnlockCard();
                    cardsData[cardKey] = tmp;
                    Debug.Log(cardsData[cardKey].isUnlocked);
                    break;
                }
            }
        }
        
    }
    public bool UseCard(Card card, PlayableCharacter character)
    {
        return cardsData[card].UseCard(character);
    }
    public bool UnequipCard(Card card, PlayableCharacter character)
    {
        return cardsData[card].UnequipCard(character);
    }
    public List<Card> GetCardsUnlocked()
    {
        List<Card> cards = new List<Card>();
        foreach (var item in cardsData)
        {
            Debug.Log(item.Value.isUnlocked);
            if (item.Value.isUnlocked)
            {
                Debug.Log("AddingCard");
                cards.Add(item.Key);
            }
        }
        return cards;
    }
    public void AddCard(Card card)
    {
        cardsData[card].AddCard();
    }
    public void AddCard(Card card,int ammount)
    {
        cardsData[card].AddCard(ammount);
    }
}
