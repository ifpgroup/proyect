﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IController
{
    void AddCreature(EntityCombat entity);
    void CardAnimationOver();
    void IsMyTurn();
    void PlayCard(int index, int[] targetsUID);
    void PlayCard(int index, int targetUID);
    void DrawCard();
    void EndTurn();
    void IsDead(EntityCombat entity);
    void GotAttacked(int uid, float damage);
}