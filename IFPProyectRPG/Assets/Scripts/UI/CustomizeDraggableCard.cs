﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class CustomizeDraggableCard : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Sprite offensiveCardSprite;
    public Sprite defensiveCardSprite;
    public Sprite supportCardSprite;
    public Sprite physicalSprite;
    public Sprite magicalSprite;

    public Image image;
    public Image backgroundPanel;
    public Image attributeIcon;
    public TextMeshProUGUI title;
    public TextMeshProUGUI manaCostText;
    public TextMeshProUGUI energyCostText;

    public UICustomizeDeckManager uiCustomizeDeckManager;

    private Transform canvasT;
    private Vector2 originalPos;
    private Transform originalParent;
    private bool isDragging;
    private Player player;

    private bool isFromDeck;

    private Card card;

    private void Start()
    {
        canvasT = GameObject.FindGameObjectWithTag("Canvas").transform;
        isDragging = false;
        originalParent = gameObject.transform.parent;
        originalPos = gameObject.transform.position;
    }

    public void SetPlayer(Player p)
    {
        player = p;
    }

    public void SetCard(Card c)
    {
        card = c;
        switch (c.GetAttribute())
        {
            case CardAttribute.Strength: attributeIcon.sprite = physicalSprite; break;
            case CardAttribute.Intelligence: attributeIcon.sprite = magicalSprite; break;
            default:
                attributeIcon.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
                break;
        }
        switch (c.GetCardType())
        {
            case CardType.Offensive:
                image.sprite = offensiveCardSprite;
                backgroundPanel.color = new Color(235f / 255f, 71f / 255f, 57f / 255f, 1.0f);
                break;
            case CardType.Deffensive:
                image.sprite = defensiveCardSprite;
                backgroundPanel.color = new Color(125f / 255f, 158f / 255f, 227f / 255f, 1.0f);
                break;
            case CardType.Support:
                image.sprite = supportCardSprite;
                backgroundPanel.color = new Color(46f / 255f, 255f / 255f, 186f / 255f, 1.0f);
                break;
        }
        title.text = card.GetName();
        manaCostText.text = card.GetCostMana().ToString();
        energyCostText.text = card.GetCostEnergy().ToString();
    }

    public Card GetCard()
    {
        return card;
    }

    public void SetIsFromDeck(bool b)
    {
        isFromDeck = b;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        isDragging = true;
        gameObject.transform.SetParent(canvasT);
    }

    public void OnDrag(PointerEventData eventData)
    {
        gameObject.transform.position = new Vector2(Mouse.current.position.ReadValue().x, Mouse.current.position.ReadValue().y);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        isDragging = false;
        Vector2 v;

        PointerEventData pointerData = new PointerEventData(EventSystem.current)
        {
            pointerId = -1,
        };

        v = new Vector2(Mouse.current.position.ReadValue().x, Mouse.current.position.ReadValue().y);
        pointerData.position = v;

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, results);

        bool toDeckArea = false;
        bool toCollectionArea = false;

        foreach (RaycastResult aux in results)
        {
            //Debug.Log(aux.gameObject.name + "RAYCAST");
            if (aux.gameObject.name == "DeckPanel")
            {
                //Debug.Log("COLLECTION AREA");
                toDeckArea = true;
                toCollectionArea = false;
            }
            else if (aux.gameObject.name == "CollectionPanel")
            {
                //Debug.Log("DECK AREA");
                toCollectionArea = true;
                toDeckArea = false;
            }
        }

        if (toDeckArea && !isFromDeck)
        {
            if (uiCustomizeDeckManager.Fits())
            {
                isFromDeck = true;
                uiCustomizeDeckManager.MoveCardTo(this.gameObject, "Deck");
            }
            else
            {
                uiCustomizeDeckManager.ShowUnmovableCard("¡No puedes añadir más cartas al deck!");
                //gameObject.transform.position = originalPos;
                gameObject.transform.SetParent(originalParent);
            }
        }
        else if (toCollectionArea && isFromDeck)
        {
            isFromDeck = false;
            uiCustomizeDeckManager.MoveCardTo(this.gameObject, "Collection");
        }
        else
        {
            //gameObject.transform.position = originalPos;
            gameObject.transform.SetParent(originalParent);
        }
    }

    public void SetOriginalParent(Transform t)
    {
        originalParent = t;
    }
}
