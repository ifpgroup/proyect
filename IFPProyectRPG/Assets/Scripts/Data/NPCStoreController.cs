﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCStoreController : MonoBehaviour
{
    private UIDungeonManager dungeonManage;

    private void Start()
    {
        dungeonManage = GameObject.FindGameObjectWithTag("UIDungeonManager").GetComponent<UIDungeonManager>();
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.tag);
        if (collision.gameObject.tag.Equals("Entity"))
        {
            dungeonManage.ShowShopPanel();
        }
    }
}
