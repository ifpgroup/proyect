﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StatsMiniBar : MonoBehaviour
{
    private UIDHolder uidHolder;
    private GameObject miniHpBar;
    public GameObject prefabMiniBar;
    private Transform canvasT;
    public float barsYOffset = 40.0f;

    private void Awake()
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name.Equals("CombatLevel"))
        {
            canvasT = GameObject.FindGameObjectWithTag("MiniHolder").transform;
            uidHolder = gameObject.GetComponent<UIDHolder>();
        }
        else
            enabled = false;
    }

    private void Start()
    {
        float aux = (float)uidHolder.GetDataCombat().GetAttr().hp / uidHolder.GetDataCombat().GetAttr().maxHp;
        CreateStatsMiniPanel(Camera.main.WorldToScreenPoint(transform.position), aux);
        miniHpBar.transform.GetChild(0).GetComponent<HealthBarShrink>().SetAlpha(0.0f);
    }

    public void CreateStatsMiniPanel(Vector2 pos, float healthNormalized)
    {
        float y = pos.y + barsYOffset;
        Vector2 aux = new Vector2(pos.x, y);
        miniHpBar = Instantiate(prefabMiniBar, aux, Quaternion.identity, canvasT);
        miniHpBar.transform.GetChild(0).GetComponent<HealthBarShrink>().SetHealth(healthNormalized);
    }

    public void OnDamageEffect(float newHealth)
    {
        miniHpBar.transform.GetChild(0).GetComponent<HealthBarShrink>().OnDamage(newHealth);
    }

    public void OnHealEffect(float newHealth)
    {
        miniHpBar.transform.GetChild(0).GetComponent<HealthBarShrink>().OnHeal(newHealth);
    }
}
