﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUIManager
{
    #region Combat
    //Si queremos mostrar el cementerio necesito cuantas cartas
    //hay en él al menos(?) 
    //Si queremos mostrar el cementerio clickando en él, ya necesitaría
    //entonces la lista de cartas que hay en él --> ICombatManager.GetCementery(); esto te devuelve las cartas en el cementerio actual.
    void StartTurn(List<Card> hand, bool isEnemy, int graveyardAmount);

    //Quizá tener index y carta a la vez me venga bien
    void PlayCard(int index, Card card, int UIDtarget);
    void PlayCard(int index, Card card, List<int> UIDtargets);
    void EndTurn();

    //Quizá necesite saber a quien se le ha hecho el daño/efecto
    //para mostrar los números o animaciones en posición(?)
    //Depende de la complejidad quizá se necesite saber si
    //ha hecho daño, curado, o algo especial
    void CardResultsAttack(float damage);
    void CardResult(CardResult result, EntityAttributes attr, bool playerAttr);

    void GetEntityData(EntityAttributes attr, int actions, bool hasSupportAction, int UID, bool isPlayer = false);
    #endregion

    Card GetTouchedCard(int index);
    void UIPlayCard(int index, Card card, int UIDtarget, bool isBasic);
    void GetAllUIDS(List<int> playerUIDS, List<int> enemiesUIDS);
    void UpdateCardInfo(int index);
    EntityAttributes GetCurrentEntityAttr();
    int GetCurrentEntityRemainingActions();
    bool GetCurrentEntityHasSupportAction();
    void ShowUnusableCard();
    void EndCombat(string sceneToLoad, bool victory, Reward loot);
    void StartCombat();
    void SetCurrentEntityRemainingActions(int n);
    void SetCurrentEntityHasSupportAction(bool b);
    void ShowTargetDeco(bool b, Vector2 pos);
}
