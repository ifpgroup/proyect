﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Creditos : MonoBehaviour
{
    public float yLimit;
    public float speed = 2f;
    private bool fin = false;
    private bool inicio = false;

    private void Start()
    {
        Invoke("Inicio", 2.0f);
    }

    void Update()
    {
        if (inicio)
        {
            if (transform.position.y < yLimit)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + (speed * Time.deltaTime), transform.position.z);
            }
            else
            {
                if (!fin)
                {
                    Invoke("IrAMenu", 4.0f);
                    fin = true;
                }
            }
        }
    }

    private void IrAMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    private void Inicio()
    {
        inicio = true;
    }
}
