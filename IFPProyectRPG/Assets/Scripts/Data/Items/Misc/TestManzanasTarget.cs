﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestManzanasTarget : MonoBehaviour
{
    [SerializeField] public Item item;
    [SerializeField] public int ammount;
    private Player player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("PlayerManager").GetComponent<Player>();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("ME he comido 5 manzanas");
        player.GetInventory().AddItem(item, ammount);
    }
}
