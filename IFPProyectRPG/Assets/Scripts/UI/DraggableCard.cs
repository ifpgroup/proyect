﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class DraggableCard : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
{
    private const float CIRCLE_CAST_RADIUS = 1.2f;
    public Sprite offensiveCardSprite;
    public Sprite defensiveCardSprite;
    public Sprite supportCardSprite;
    public Sprite physicalSprite;
    public Sprite magicalSprite;

    public Image image;
    public Image backgroundPanel;
    public Image attributeIcon;
    public TextMeshProUGUI title;
    public TextMeshProUGUI manaCostText;
    public TextMeshProUGUI energyCostText;

    private GameObject enemyArrow;
    private GameObject allyArrow;
    private GameObject supportArrow;
    private IUIManager uiManager;

    private bool isOffensive;
    private bool isDeffensive;
    private bool isSingleTarget;
    private bool isSupport;
    private Card card;
    private int index;
    private bool isBasic; //Usado para simplemente arrastrar la carta al medio
    private Vector2 originalPos;
    private Animator animator;
    private bool isDragging;
    private bool isUsable;

    private void Start()
    {
        isDragging = false;
        animator = gameObject.GetComponent<Animator>();
        enemyArrow = GameObject.FindGameObjectWithTag("EnemyTargetArrow");
        allyArrow = GameObject.FindGameObjectWithTag("AllyTargetArrow");
        supportArrow = GameObject.FindGameObjectWithTag("SupportTargetArrow");
        uiManager = GameObject.FindGameObjectWithTag("UIManager").GetComponent<IUIManager>();
        originalPos = gameObject.transform.position;
        isUsable = false;
    }

    public void SetCard(Card c)
    {
        card = c;
        switch (c.GetAttribute())
        {
            case CardAttribute.Strength: attributeIcon.sprite = physicalSprite; break;
            case CardAttribute.Intelligence: attributeIcon.sprite = magicalSprite; break;
            default:
                attributeIcon.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
                break;
        }
        switch (c.GetCardType())
        {
            case CardType.Offensive: 
                image.sprite = offensiveCardSprite;
                backgroundPanel.color = new Color(235f/255f,71f/255f,57f/255f,1.0f);
                break;
            case CardType.Deffensive: 
                image.sprite = defensiveCardSprite;
                backgroundPanel.color = new Color(125f/255f, 158f/255f, 227f/255f, 1.0f);
                break;
            case CardType.Support: 
                image.sprite = supportCardSprite;
                backgroundPanel.color = new Color(46f/255f, 255f/255f, 186f/255f, 1.0f);
                break;
        }
        title.text = card.GetName();
        manaCostText.text = card.GetCostMana().ToString();
        energyCostText.text = card.GetCostEnergy().ToString();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        animator.SetTrigger("MouseOver");
        uiManager.UpdateCardInfo(gameObject.transform.GetSiblingIndex());
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!isDragging)
        {
            animator.SetTrigger("MouseOverOut");
            //uiManager.UpdateCardInfo(-1);
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        isOffensive = false;
        isDeffensive = false;
        isSingleTarget = false;
        isSupport = false;
        isBasic = false;
        card = null;
        index = -1;

        index = gameObject.transform.GetSiblingIndex();
        
        card = uiManager.GetTouchedCard(index);

        if (uiManager.GetCurrentEntityAttr().mana >= card.GetCostMana() && uiManager.GetCurrentEntityAttr().energy >= card.GetCostEnergy())
        {
            isUsable = true;
        }
        else
        {
            uiManager.ShowUnusableCard();
            isUsable = false;
            isDragging = false;
            animator.SetTrigger("MouseOverOut");
            return;
        }

        isOffensive = card.GetCardType() == CardType.Offensive;
        isDeffensive = card.GetCardType() == CardType.Deffensive;
        isSingleTarget = card.GetIsSingleTarget();
        isSupport = card.GetCardType() == CardType.Support;

        if (isOffensive)
        {
            if(uiManager.GetCurrentEntityRemainingActions() > 0)
            {
                isUsable = true;
            }
            else
            {
                uiManager.ShowUnusableCard();
                isUsable = false;
                isDragging = false;
                animator.SetTrigger("MouseOverOut");
                return;
            }
        }
        else if (isSupport)
        {
            if (!uiManager.GetCurrentEntityHasSupportAction())
            {
                uiManager.ShowUnusableCard();
                isUsable = false;
                isDragging = false;
                animator.SetTrigger("MouseOverOut");
                return;
            }
            else
            {
                isUsable = true;
            }
        }
        else
        {
            if (uiManager.GetCurrentEntityRemainingActions() > 0)
            {
                isUsable = true;
            }
            else
            {
                uiManager.ShowUnusableCard();
                isUsable = false;
                isDragging = false;
                animator.SetTrigger("MouseOverOut");
                return;
            }
        }

        isDragging = true;

        switch (isOffensive)
        {
            case true:
                if (isSingleTarget)
                {
                    //MOSTRAR ARROW Y SELECCIÓN DE OBJETIVO ENEMIGO
                    enemyArrow.SetActive(true);
                    Vector3 screenPoint = gameObject.transform.position;
                    screenPoint.z = Mathf.Abs(Camera.main.transform.position.z); //ScreenToWorldPoint usa la Z de la cámara, por lo tanto hay que anularla
                    enemyArrow.GetComponent<LineRenderer>().SetPosition(0, Camera.main.ScreenToWorldPoint(screenPoint));
                }
                else
                {
                    //PERMITIR ARRASTRAR LA CARTA AL MEDIO
                    isBasic = true;
                }
                break;
            case false:
                if (isSupport)
                {
                    if (isSingleTarget)
                    {
                        //MOSTRAR ARROW Y SELECCIÓN DE OBJETIVO
                        supportArrow.SetActive(true);
                        Vector3 screenPoint = gameObject.transform.position;
                        screenPoint.z = Mathf.Abs(Camera.main.transform.position.z); //ScreenToWorldPoint usa la Z de la cámara, por lo tanto hay que anularla
                        supportArrow.GetComponent<LineRenderer>().SetPosition(0, Camera.main.ScreenToWorldPoint(screenPoint));
                    }
                    else
                    {
                        //PERMITIR ARRASTRAR LA CARTA AL MEDIO
                        isBasic = true;
                    }
                }
                else
                {
                    if (isSingleTarget)
                    {
                        //MOSTRAR ARROW Y SELECCIÓN DE OBJETIVO ALIADO
                        allyArrow.SetActive(true);
                        Vector3 screenPoint = gameObject.transform.position;
                        screenPoint.z = Mathf.Abs(Camera.main.transform.position.z); //ScreenToWorldPoint usa la Z de la cámara, por lo tanto hay que anularla
                        allyArrow.GetComponent<LineRenderer>().SetPosition(0, Camera.main.ScreenToWorldPoint(screenPoint));
                }
                    else
                    {
                        //PERMITIR ARRASTRAR LA CARTA AL MEDIO
                        isBasic = true;
                    }
                }
                break;
        }
    }
    
    public void OnDrag(PointerEventData eventData)
    {
        if (!isUsable) return;
        //Si es básica (solo hay que arrastrarla al medio)
        if (isBasic)
        {
            gameObject.transform.position = new Vector2(Mouse.current.position.ReadValue().x, Mouse.current.position.ReadValue().y);
        }
        //Si no lo es, hay que ver quien es el target posible
        else
        {
            Vector2 v;
            RaycastHit2D hit;
            int uid = -1;
            EntityCombat entityCombat = null;
            v = new Vector2(Mouse.current.position.ReadValue().x, Mouse.current.position.ReadValue().y);
            hit = Physics2D.CircleCast(Camera.main.ScreenToWorldPoint(v), CIRCLE_CAST_RADIUS, Vector2.zero);

            if (hit.collider != null)
            {
                if (hit.transform.tag == "Entity")
                {
                    entityCombat = hit.transform.gameObject.GetComponent<UIDHolder>().GetDataCombat();
                }
            }

            if(entityCombat == null)
            {
                uiManager.ShowTargetDeco(false, Vector2.zero);
            }

            switch (isOffensive)
            {
                case true:
                    if (isSingleTarget)
                    {
                        enemyArrow.SetActive(true);
                        //MOSTRAR ARROW Y SELECCIÓN DE OBJETIVO ENEMIGO
                        Vector3 screenPoint = eventData.position;
                        screenPoint.z = Mathf.Abs(Camera.main.transform.position.z); //ScreenToWorldPoint usa la Z de la cámara, por lo tanto hay que anularla
                        enemyArrow.GetComponent<LineRenderer>().SetPosition(1, Camera.main.ScreenToWorldPoint(screenPoint));
                        if (entityCombat != null && entityCombat.isEnemy)
                        {
                            uiManager.ShowTargetDeco(true, Camera.main.WorldToScreenPoint(hit.transform.position));
                        }
                        else
                        {
                            uiManager.ShowTargetDeco(false, Vector2.zero);
                        }
                    }
                    break;
                case false:
                    if (isSupport)
                    {
                        if (isSingleTarget)
                        {
                            supportArrow.SetActive(true);
                            //MOSTRAR ARROW Y SELECCIÓN DE OBJETIVO
                            Vector3 screenPoint = eventData.position;
                            screenPoint.z = Mathf.Abs(Camera.main.transform.position.z); //ScreenToWorldPoint usa la Z de la cámara, por lo tanto hay que anularla
                            supportArrow.GetComponent<LineRenderer>().SetPosition(1, Camera.main.ScreenToWorldPoint(screenPoint));
                            //TO DO: SUPPORT CARDS WITH SPECIAL EFFECTS MAY TARGET ENEMIES
                            if (entityCombat != null && !entityCombat.isEnemy)
                            {
                                uiManager.ShowTargetDeco(true, Camera.main.WorldToScreenPoint(hit.transform.position));
                            }
                            else
                            {
                                uiManager.ShowTargetDeco(false, Vector2.zero);
                            }
                        }
                    }
                    else
                    {
                        if (isSingleTarget)
                        {
                            allyArrow.SetActive(true);
                            //MOSTRAR ARROW Y SELECCIÓN DE OBJETIVO ALIADO
                            Vector3 screenPoint = eventData.position;
                            screenPoint.z = Mathf.Abs(Camera.main.transform.position.z); //ScreenToWorldPoint usa la Z de la cámara, por lo tanto hay que anularla
                            allyArrow.GetComponent<LineRenderer>().SetPosition(1, Camera.main.ScreenToWorldPoint(screenPoint));
                            if (entityCombat != null && !entityCombat.isEnemy)
                            {
                                uiManager.ShowTargetDeco(true, Camera.main.WorldToScreenPoint(hit.transform.position));
                            }
                            else
                            {
                                uiManager.ShowTargetDeco(false, Vector2.zero);
                            }
                        }
                    }
                    break;
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!isUsable) return;
        Vector2 v;
        RaycastHit2D hit;
        isDragging = false;
        uiManager.UpdateCardInfo(-1);
        //Si es básica (solo hay que arrastrarla al medio)
        if (isBasic)
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current)
            {
                pointerId = -1,
            };

            v = new Vector2(Mouse.current.position.ReadValue().x, Mouse.current.position.ReadValue().y);
            pointerData.position = v;

            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerData, results);
            bool dropArea = false;

            foreach (RaycastResult aux in results)
            {
                if(aux.gameObject.tag == "DropArea")
                {
                    dropArea = true;
                }
            }

            if (dropArea)
            {
                //SE HA SOLTADO EN EL ÁREA CORRECTA PARA USAR LA CARTA
                //DESTRUIR CARTA Y USARSE
                uiManager.UIPlayCard(index, card, -1, true);
            }
            else
            {
                //ZONA INCORRECTA
                //VOLVER CARTA A SU POSICIÓN
                gameObject.transform.position = originalPos;
            }
        }
        else
        {
            //Si las pongo en SetActive(false) surgen problemas, mejor "quitar" la línea
            enemyArrow.GetComponent<LineRenderer>().SetPosition(1, enemyArrow.GetComponent<LineRenderer>().GetPosition(0));
            allyArrow.GetComponent<LineRenderer>().SetPosition(1, allyArrow.GetComponent<LineRenderer>().GetPosition(0));
            supportArrow.GetComponent<LineRenderer>().SetPosition(1, supportArrow.GetComponent<LineRenderer>().GetPosition(0));

            int uid = -1;
            EntityCombat entityCombat = null;
            v = new Vector2(Mouse.current.position.ReadValue().x, Mouse.current.position.ReadValue().y);
            hit = Physics2D.CircleCast(Camera.main.ScreenToWorldPoint(v), CIRCLE_CAST_RADIUS, Vector2.zero);

            if (hit.collider != null)
            {
                if (hit.transform.tag == "Entity")
                {
                    uid = hit.transform.gameObject.GetComponent<UIDHolder>().GetUID();
                    entityCombat = hit.transform.gameObject.GetComponent<UIDHolder>().GetDataCombat();
                }
            }

            if(entityCombat == null)
            {
                uiManager.ShowTargetDeco(false, Vector2.zero);
                isDragging = false;
                animator.SetTrigger("MouseOverOut");
                return;
            }

            switch (isOffensive)
            {
                case true:
                    if (isSingleTarget)
                    {
                        if (!entityCombat.isEnemy)
                        {
                            uiManager.ShowTargetDeco(false, Vector2.zero);
                            isDragging = false;
                            animator.SetTrigger("MouseOverOut");
                            return;
                        }
                        else
                        {
                            uiManager.SetCurrentEntityRemainingActions(uiManager.GetCurrentEntityRemainingActions() - 1);
                        }
                    }
                    break;
                case false:
                    if (isSupport)
                    {
                        if (isSingleTarget)
                        {
                            //TO DO: SUPPORT CARDS
                            uiManager.SetCurrentEntityHasSupportAction(false);
                        }
                    }
                    else
                    {
                        if (isSingleTarget)
                        {
                            if (entityCombat.isEnemy)
                            {
                                uiManager.ShowTargetDeco(false, Vector2.zero);
                                isDragging = false;
                                animator.SetTrigger("MouseOverOut");
                                return;
                            }
                            else
                            {
                                uiManager.SetCurrentEntityRemainingActions(uiManager.GetCurrentEntityRemainingActions() - 1);
                            }
                        }
                    }
                    break;
            }
            uiManager.UpdateCardInfo(-1);
            uiManager.ShowTargetDeco(false, Vector2.zero);
            uiManager.UIPlayCard(index, card, uid, false);
        }
    }
}
