﻿#pragma warning disable 0649
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "QuestData", menuName = "Scriptable Object/QuestData", order = 2)]
public class QuestData : ScriptableObject
{
    [SerializeField] private string questID;
    [SerializeField] private string questName;
    [SerializeField] [TextArea(3, 8)] 
    private string questDescription;
    [SerializeField] private bool isMain;
    [SerializeField] private bool increaseAdventureRank;
    [SerializeField] private List<Item> reward;
    [SerializeField] private List<int> rewardItemsAmmount;
    [SerializeField] private Reward rewardWhenComplete;
    [SerializeField] private int numberOfTargets;
    [SerializeField] private QuestTarget[] targets;
    [SerializeField] private QuestRequirements requirements;

    private int i = 0;


    public Reward GetReward() => rewardWhenComplete;
    public string GetQuestID() => questID;
    public string GetQuestName() => questName;
    public string GetQuestDescription() => questDescription;
    public bool GetIsMain() => isMain;
    public bool GetIncreaseAdventureRank() => increaseAdventureRank;
    public List<int> GetRewardItemsAmmount() => rewardItemsAmmount;
    public QuestTarget[] GetTargets() => targets;
    public QuestRequirements GetRequirements() => requirements;


    public int CheckTargetsToCheckCompletedQuest()
    {
        while (numberOfTargets > 0)
        {
            if (targets[i].GetCheckTarget())  // 1 target complete
            {
                numberOfTargets--; i++;
            }
            else return 0;
        }
        if (numberOfTargets == 0) // all targets completed
            return 1;
        return 2;
    }

}