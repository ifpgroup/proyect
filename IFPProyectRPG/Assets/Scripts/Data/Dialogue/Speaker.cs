﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Speaker", menuName = "Scriptable Object/Dialogue/Speaker", order = 3)]
public class Speaker : ScriptableObject
{
    [SerializeField] private string speakerName;
    [SerializeField] private Sprite speakerSprite;

    public string GetName() => speakerName;
    public Sprite GetSprite() => speakerSprite;
}
