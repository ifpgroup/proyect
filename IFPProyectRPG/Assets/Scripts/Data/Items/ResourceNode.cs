﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceNode : InteractableItem
{
    [SerializeField] private ResourceNodeData data;
    
    private int finalAmmountOfResource;
    Player player;
    private int index;
    public override void PlayAction()
    {
        player.GetInventory().AddResource(data.GetResource(), finalAmmountOfResource);
    }

    public override void SetData(ScriptableObject data, int index)
    {
        this.data = data as ResourceNodeData;
        this.index = index;
        finalAmmountOfResource = Random.Range(1, this.data.GetMaxAmmount());
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("PlayerManager")?.GetComponent<Player>();
        
    }

}
