﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public struct RequirementsStairs
{
    public int adventurerRank; // maybe other requirements later on?
}
public class Stairs : InteractableItem
{
    [SerializeField] private string nextLevel;
    [SerializeField] private bool isUnlocked;
    [SerializeField] private bool hasRequirement;
    [SerializeField] private RequirementsStairs requirement;
    private Player player;
    private int index;

    public override void PlayAction()
    {
        if (!hasRequirement)
        {
            SceneManager.LoadScene(nextLevel);
        }
        else
        {
            if(isUnlocked)
                SceneManager.LoadScene(nextLevel);
            else
            {
                if (player.GetAdventurerRank() >= requirement.adventurerRank)
                    SceneManager.LoadScene(nextLevel);
                else
                {
                    //Something about not having the rank??
                }
            }
        }
    }

    public override void SetData(ScriptableObject data, int index)
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("PlayerManager")?.GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
