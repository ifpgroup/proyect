﻿using UnityEngine;

[System.Serializable]
public class DialogueLine
{
    public Speaker speaker;
    [TextArea(7, 10)]
    public string dialogue;

    public string GetDialogue() => dialogue;
}
