﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private List<PlayableCharacter> party;
    private List<GameObject> partyPrefabs;
    private int maxSize;
    private int AdventurerRank;
    private List<Quest> questsAccepted;
    private List<Quest> questsCompleted;
    [SerializeField] private Inventory inventory;
    [SerializeField] private CollectionCards collection;
    private bool isPartyDead;
    private PlayerState state;
    // Start is called before the first frame update
    void Start()
    {        
        maxSize = 4;
        DontDestroyOnLoad(this);
        state = new PlayerState();
        state.lastLevelName = SceneManager.GetActiveScene().name;
        questsAccepted = new List<Quest>();
        questsCompleted = new List<Quest>();
        AdventurerRank = 1;
        Enemy.OnDead += NotifyQuestsEnemyDead;
        inventory.itemAdded += NotifyQuestsObjectPickUp;
        
    }
    public void NotifyQuestsEnemyDead(string name)
    {
        foreach (var quest in questsAccepted)
        {
            Debug.Log("Enemy killed, name: " + name);
            if(quest.CheckTarget(TypeOfTargets.KILLENEMIES, name))
            {
                Debug.Log("Enough trolls killed");
            }

        }
    }
    public void NotifyQuestsObjectPickUp(string name,int ammount)
    {
        foreach (var quest in questsAccepted)
        {
            if (quest.CheckTarget(TypeOfTargets.GETITEM, name, ammount)) { }
                Debug.Log("Target Completed");
        }
    }
    public void NotifyQuestsSpokeWithNPC(string name)
    {
        foreach (var quest in questsAccepted)
        {
            quest.CheckTarget(TypeOfTargets.TALKTOSOMEONE, name);
        }
    }
    public List<Quest> GetQuestsAccepted() => questsAccepted;
    public List<Quest> GetQuestsCompleted() => questsCompleted;
    public bool AcceptQuest(Quest quest)
    {
        if (questsAccepted.Contains(quest)) return false;
        Debug.Log("Quest accepted: " + quest.GetQuestName());
        questsAccepted.Add(quest);
        return true;
    }
    public bool CompleteQuest(Quest quest)
    {
        if (!questsAccepted.Contains(quest)) return false;        
        if (questsCompleted.Contains(quest)) return false;
        questsCompleted.Add(quest);
        questsAccepted.Remove(quest);
        return true;
    }
    public bool CompleteQuest(string quid)
    {
        int i = 0;
        i = FindQuest(quid);
        if (i!=-1)
        {
            questsCompleted.Add(questsAccepted[i]);
            questsAccepted.RemoveAt(i);
        }
        return true;
    }
    private int FindQuest(string quid)
    {
        int i = 0;
        foreach (var quest in questsAccepted)
        {
            if (quest.GetQuestID().Equals(quid))
            {
                return i;
            }
            i++;
        }
        return -1;
    }
    public PlayerState GetState() => state;
    public void SetStateSceneName(string value) => state.lastLevelName = value;    
    public void GoForwardOrBack(bool value) => state.goBack = value;    
    public void AddPlayerPrefab(GameObject prefab)
    {
        if (partyPrefabs == null) partyPrefabs = new List<GameObject>();
        if(partyPrefabs.Count < maxSize)
            partyPrefabs.Add(prefab);

    }
    public int GetAdventurerRank()
    {        
        return AdventurerRank;
    }
    public Inventory GetInventory() => inventory;
    public void SetLastPosition(Vector2 pos)
    {
        state.positionOnMap = pos;
    }
    public void AddPlayerToParty(PlayableCharacter character)
    {
        if (party == null) party = new List<PlayableCharacter>();
        if (party.Count < 4)
            party.Add(character);
    }
    public List<PlayableCharacter> GetParty() => party;
    public PlayableCharacter GetPartyMember(int index) => party[index];
    public GameObject GetPartyMemberPrefab(int index) => partyPrefabs[index];
    public PlayableCharacter GetPartyMemberByID(int ID)
    {
        foreach (var character in party)
        {
            if (character.GetID() == ID)
                return character;            
        }
        return null;
    }
    public List<Card> GetCardsUnlocked()
    {
        List<Card> cards = collection.GetCardsUnlocked();

        return cards;
    }
    public List<Card> GetDeckCards(int index)
    {
        List<Card> cards = new List<Card>();
        cards = party[index].GetDeck().GetEquipment();
        Debug.Log("Initial card count: " + cards.Count);
        foreach (var card in party[index].GetDeck().GetCards())
        {
            cards.Add(card);
        }
        Debug.Log("Card from player: " + party[index].GetDeck().GetCards().Count);
        return cards;
    }
    public bool EquipCard(Card card, int index=0)
    {
        bool equipment = false;
        bool canUse =collection.UseCard(card, party[index]);
        if (canUse)
        {
            if (equipment) return false;            
            return party[index].GetDeck().EquipCard(card);
        }
        return false;
    }
    public int GetDeckSize(int index=0) => party[index].GetDeck().GetMaxSize();
    public bool UnEquipCard(Card card, int index = 0)
    {
        bool canUnequip = collection.UnequipCard(card, party[index]);
        Debug.Log(canUnequip);
        bool equipment = false;
        if (canUnequip)
        {
            if (equipment) return false;
            return party[index].GetDeck().UnequipCard(card);
        }
        return false;
    }
    public bool EquipEquipment(Equipment equipment, int index=0)
    {
        bool result = party[index].GetDeck().EquipEquipment(equipment);
        if (result)
            party[index].ChangeEquipment(equipment);
        return result;
    }
    public CollectionCards GetCollection() => collection;
    public void AddCard(Card card)
    {
        collection.AddCard(card);
        collection.UnlockCards(party);
    }

    public List<Equipment> GetEquipmentInUse(int index = 0)
    {
        List<Equipment> inUse = new List<Equipment>();
        inUse.Add(party[index].GetEquipment(TypeOfEquipment.Weapon));
        inUse.Add(party[index].GetEquipment(TypeOfEquipment.Armor));
        inUse.Add(party[index].GetEquipment(TypeOfEquipment.Trinket));
        return inUse;
    }
    public Equipment GetEquipment(TypeOfEquipment type, int index = 0)
    {
        return party[index].GetEquipment(type);
    }
}

public struct PlayerState
{
    public string lastLevelName;
    public Vector2 positionOnMap;
    //to check in the dungeon if you are going forward or backwards?
    public bool goBack;    
}