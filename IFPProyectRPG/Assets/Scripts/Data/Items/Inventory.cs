﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum TypeOfResource {  Gold, Wood, Crystal, Stone}
public class Inventory : MonoBehaviour
{
    [SerializeField] private Item Gold;
    [SerializeField] private Item Crystal;
    [SerializeField] private Item Wood;
    [SerializeField] private Item Stone;
    [SerializeField] private Player player;
    private Item[] items; // their location on the grid?
    private Dictionary<Item, int> resources; //gold cristals, wood...
    private Dictionary<Item, int> ammountOfItem; // normal items/equipment
    [SerializeField] private int size;
    public event UnityAction<string, int> itemAdded;
    public event UnityAction<string, int> resourceAdded;
    // Start is called before the first frame update
    void Start()
    {
        items = new Item[size];
        resources = new Dictionary<Item, int>();
        ammountOfItem = new Dictionary<Item, int>();
        resources.Add(Gold, 100);
        resources.Add(Stone, 100);
        resources.Add(Wood, 100);
        resources.Add(Crystal, 100);
    }

    private void OrdenarArrayItems()
    {
        int start = 0;
        int end = items.Length - 1;
        Item[] tmp = new Item[size];

        for (int n = 0; n < items.Length; n++)
        {
            if (items[n] != null)
            {
                tmp[start] = items[n];
                start++;
            }
            else
            {
                tmp[end] = items[n];
                end--;
            }
        }

        for (int i = 0; i < items.Length; i++)
        {
            items[i] = tmp[i];
        }
    }

    public int GetAmmountItem(Item item)
    {
        if (item == null) return 0;
        if (!ammountOfItem.ContainsKey(item)) return 0;
        return ammountOfItem[item];
    }
    public int GetAmmountItem(int index)
    {
        Item item = GetItem(index);
        return GetAmmountItem(item);
    }
    public bool SwapItem(int indexOrigin, int indexEnd)
    {
        try
        {
            var endItem = items[indexEnd];
            items[indexEnd] = items[indexOrigin];
            items[indexOrigin] = endItem;
            return true;
        }catch(System.Exception e)
        {
            Debug.LogError(e);
            return false;
        }
        
    }
    public int GetResourceAmmount(TypeOfResource resource)
    {
        switch (resource)
        {
            case TypeOfResource.Gold:
                return resources[Gold];                
            case TypeOfResource.Wood:
                return resources[Wood];
            case TypeOfResource.Crystal:
                return resources[Crystal];
            case TypeOfResource.Stone:
                return resources[Stone];
            default:
                return -1;                
        }
    }
    public void AddResource(TypeOfResource resource, int ammount)
    {
        switch (resource)
        {
            case TypeOfResource.Gold:
                AddGold(ammount);
                resourceAdded?.Invoke("Gold", ammount);
                break;
            case TypeOfResource.Wood:
                AddWood(ammount);
                resourceAdded?.Invoke("Wood", ammount);
                break;
            case TypeOfResource.Crystal:
                AddCrystal(ammount);
                resourceAdded?.Invoke("Crystal", ammount);
                break;
            case TypeOfResource.Stone:
                AddStone(ammount);
                resourceAdded?.Invoke("Stone", ammount) ;
                break;
            default:
                break;
        }
    }
    public bool UseResource(TypeOfResource resource, int ammount)
    {
        switch (resource)
        {
            case TypeOfResource.Gold:
                return UseGold(ammount);
            case TypeOfResource.Wood:
                return UseWood(ammount);
            case TypeOfResource.Crystal:
                return UseCrystal(ammount);
            case TypeOfResource.Stone:
                return UseStone(ammount);
            default:
                return false;
        }
    }
    public Item GetItem(int index) => items[index];
    public Item[] GetItems() => items;
    public Item GetItemByUid(int uid)
    {
        foreach (var item in items)
        {
            if (item.GetGUID() == uid)
                return item;
        }
        return null;
    }    
    public Item UseItem(int index)
    {
        if (ammountOfItem[items[index]] > 0)
        {
            ammountOfItem[items[index]]--;
            Item itemDesired = items[index];
            if(ammountOfItem[itemDesired]==0)
                items[index] = null; // missing telling something to the ui?
            OrdenarArrayItems();
            return itemDesired;
        }
        else
            return null;
    }
    public Item UseItemByUid(int uid)
    {
        int i = 0;
        foreach (var item in items)
        {
            if (item.GetGUID() == uid)
            {
                if (ammountOfItem[item] > 0)
                {
                    ammountOfItem[item]--;
                    Item itemDesired = item;
                    if (ammountOfItem[itemDesired] == 0)
                        items[i] = null;
                    return itemDesired;
                }
                else
                    return null;
            }

            i++;
        }
        return null;
    }
    public bool SoldItem(Item item)
    {
        int index = GetItemPos(item);
        if (index!=-1 && items[index].CanBeSold())
        {
            UseItem(index);
            AddGold(item.GetValue());
            return true;
        }
        else
            return false;
    }
    public bool EquipItem(int index)
    {
        if (index >= items.Length) return false;
        if (items[index].CanBeEquip())
        {
            return player.EquipEquipment(items[index] as Equipment);
            
        }
        return false;
    }
    public bool EquipItem(Item item)
    {
        if (!HasItem(item)) return false;
        if (item.CanBeEquip())
        {
            return player.EquipEquipment(item as Equipment);
            
        }
        return false;
    }
    public bool AddItem(Item item, int ammount = 1) // posiblemente necesite cambios,. faltaria uno para cambiar posicion de dos items en el inventario?
    {
        if (HasItem(item))
        {
            if (ammountOfItem.ContainsKey(item))
                ammountOfItem[item] += ammount;
            else
                ammountOfItem.Add(item, ammount);

            itemAdded?.Invoke(item.name, ammount);

            return true;
        }
        else
        {
            int i = 0;
            bool thereIsSpace = false;
            foreach (var itemInInventory in items)
            {
                if (itemInInventory == null)
                {                    
                    items[i] = item;
                    thereIsSpace = true;
                    if (ammountOfItem.ContainsKey(item))
                        ammountOfItem[item] += ammount;
                    else
                        ammountOfItem.Add(item, ammount);
                    break;
                }
                i++;
                    
            }
            if (thereIsSpace)
            {
                itemAdded?.Invoke(item.name, ammount);
                return true;
            }
                
            else
                return false;
        }
    }
    public bool RemoveItem(Item item)
    {
        if (HasItem(item))
        {
            ammountOfItem.Remove(item);
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i].GetGUID() == item.GetGUID())
                {
                    items[i] = null;
                    return true;
                }
                    
            }
        }
        return false;
    }
    public void AddGold(int ammount)
    {
        if (resources.ContainsKey(Gold))
            resources[Gold] += ammount;
        else
            resources.Add(Gold, ammount);
    }
    public void AddCrystal(int ammount)
    {
        if (resources.ContainsKey(Crystal))
            resources[Crystal] += ammount;
        else
            resources.Add(Crystal, ammount);
    }
    public void AddStone(int ammount)
    {
        if (resources.ContainsKey(Stone))
            resources[Stone] += ammount;
        else
            resources.Add(Stone, ammount);
    }
    public void AddWood(int ammount)
    {
        if (resources.ContainsKey(Wood))
            resources[Wood] += ammount;
        else
            resources.Add(Wood, ammount);
    }
    public bool UseGold(int ammount)
    {
        if (resources.ContainsKey(Gold))
        {
            resources[Gold] -= ammount;
            return true;
        }            
        else
            return false;
    }
    public bool UseCrystal(int ammount)
    {
        if (resources.ContainsKey(Crystal))
        {
            resources[Crystal] -= ammount;
            return true;
        }            
        else
            return false;
    }
    public bool UseStone(int ammount)
    {
        if (resources.ContainsKey(Stone))
        {
            resources[Stone] -= ammount;
            return true;
        }            
        else
            return false;
    }
    public bool UseWood(int ammount)
    {
        if (resources.ContainsKey(Wood))
        {
            resources[Wood] -= ammount;
            return true;
        }
            

        return false;
    }
    public bool HasItem(Item itemDesired)
    {
        foreach (var item in items)
        {
            if (item == null)
                continue;
            if (itemDesired.GetGUID() == item.GetGUID())
                return true;
        }
        return false;
    }
    public int GetItemPos(Item itemDesired)
    {
        int i = 0;
        foreach (var item in items)
        {
            if (itemDesired.GetGUID() == item.GetGUID())
                return i;
            i++;
        }
        return -1;
    }

    public int GetSize()
    {
        return items.Length;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
