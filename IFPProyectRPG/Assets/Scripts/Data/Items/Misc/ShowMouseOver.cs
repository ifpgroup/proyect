﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShowMouseOver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private bool mouseOver;
    private float currentAlpha;
    private GameObject miniHpBar;
    private bool forcedShow;

    private void Start()
    {
        mouseOver = false;
        forcedShow = false;
        miniHpBar = transform.GetChild(0).gameObject;
    }

    private void Update()
    {
        if (mouseOver || forcedShow)
        {
            if (currentAlpha != 1.0f)
            {
                miniHpBar.GetComponent<HealthBarShrink>().AddAlpha(0.1f);
                currentAlpha = miniHpBar.GetComponent<HealthBarShrink>().GetAlpha();
            }
        }
        else if (!mouseOver && currentAlpha > 0.0f)
        {
            miniHpBar.GetComponent<HealthBarShrink>().SubAlpha(0.1f);
            currentAlpha = miniHpBar.GetComponent<HealthBarShrink>().GetAlpha();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        mouseOver = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        mouseOver = false;
    }

    public void ShowPanel(bool show)
    {
        forcedShow = show;
    }
}
