﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICombatManager 
{
    /// Will change the int to the cards once the class is done
     List<Card> GetCardsOnDeck();
     List<Card> GetCardsOnCementary();
     void PlayCardOnTarget(int index, int UIDTarget);
     void PlayCardOnTarget(int index, int[] UIDTargets);
     void CardAnimationOver();
     void EndPlayerTurn();
}
