﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Rarity",menuName = "Scriptable Object/Create Card Rarity")]
public class CardRarity : ScriptableObject
{
    [SerializeField] private int maxUseInTotal;
    [SerializeField] private int maxUsePerPlayer;

    public int GetMaxUseInTotal() => maxUseInTotal;
    public int GetMaxUsePerPlayer() => maxUsePerPlayer;
}
