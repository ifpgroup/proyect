﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class MOVELEFTRIGHT : MonoBehaviour
{
    public static float movement;
    bool[] keysPressed;
    public GameObject target;
    public Animator animator;
    public event UnityAction<float> onMove;
    // Start is called before the first frame update
    void Start()
    {
        movement = 0;
        keysPressed = new bool[4];
        for (int i = 0; i < keysPressed.Length; i++)
        {
            keysPressed[i] = false;
        }
    }
    public void SetTarget(GameObject character) => target = character;
    public void SetAnimator(Animator animController) => animator = animController;
    public void Move(float dist) {
        onMove?.Invoke(dist);
    }
    // Update is called once per frame
    void Update()
    {
        if (target == null) return;
        float dist = 0.0f;
        if (MOVEUPDOWN.movement.Equals(0))
        {
            if (MOVELEFTRIGHT.movement.Equals(1))
            {
                animator.SetFloat("RSpeed", Mathf.Abs(movement));
                dist = movement * 3 * Time.deltaTime;
                target.transform.Translate(dist, 0.0f, 0.0f);
                animator.SetFloat("RSpeed", 0);
         
            }
            else
            {
                animator.SetFloat("LSpeed", Mathf.Abs(movement));
                dist = movement * 3 * Time.deltaTime;
                target.transform.Translate(dist, 0.0f, 0.0f);
                animator.SetFloat("LSpeed", 0);
            }
        }
        Move(dist);
    }
     
    


    public void MoveLeftRight(InputAction.CallbackContext ctx)
    {
        if (ctx.performed)
        {
            movement = ctx.ReadValue<float>();
            animator.SetFloat("RSpeed", 0);
            animator.SetFloat("LSpeed", 0);
        }
        else if (ctx.canceled)
        {
            movement = 0;
        }
    }
}