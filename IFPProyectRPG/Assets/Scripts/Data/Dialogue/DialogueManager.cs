﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class DialogueManager : MonoBehaviour
{
	//public AudioSource dialogueOn;
	//public AudioSource dialogueOff;
	private AudioController audioController;
	private bool complete = false;

	public TextMeshProUGUI speakerName, dialogue, navButtonText;
	public Image speakerSprite;
	public Button choiceButton;
	public GameObject convOptionsUI;

	public QuestionEvent questionEvent;
	public Question convQuestion;

	private int currentIndex;
	private Conversation currentConv;
	private static DialogueManager instance;
    public event UnityAction<string> optionPress;
	private Animator anim;
	private Coroutine typing;

    private void Start()
    {
		audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
	}

    private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			anim = GetComponent<Animator>();
		}
		else
			Destroy(gameObject);
	}
    public void EndConversation()
    {
        instance.anim.SetBool("isOpen", false);
		//dialogueOff.Play();
		audioController.PlayDialogueOff();
		MovementController.isTalking = false;
        return;
    }
	public void StartConversation(Conversation conv, int startLine)
	{
		instance.anim.SetBool("isOpen", true);
		//dialogueOn.Play();
		audioController.PlayDialogueOn();
		instance.currentIndex = 0;
		startLine = 0;
		instance.currentConv = conv;
		instance.speakerName.text = "";
		instance.dialogue.text = "";
        instance.navButtonText.transform.parent.gameObject.SetActive(true);
        instance.navButtonText.text = "";
        foreach (Transform option in instance.convOptionsUI.transform)
        {
            Destroy(option.gameObject);
        }
		instance.ReadNext();
	}
	public void Next()
    {
		if (complete) ReadNext();
		else return;
    }

	public void ReadNext()
	{
		Debug.Log("Nav: " + navButtonText);
		if (instance.currentIndex > instance.currentConv.GetLength() && !instance.currentConv.GetHaveOptions())
		{
			instance.anim.SetBool("isOpen", false);
			//dialogueOff.Play();
			audioController.PlayDialogueOff();
			MovementController.isTalking = false;
			return;
		}
		instance.speakerName.text = instance.currentConv.GetLineByIndex(instance.currentIndex).speaker.GetName();
		if (instance.typing == null)
		{
			instance.typing = instance.StartCoroutine(TypeText(instance.currentConv.GetLineByIndex(instance.currentIndex).
				dialogue));
		}
		else
		{
			instance.StopCoroutine(typing);
			instance.typing = null;
			instance.typing = instance.StartCoroutine(TypeText(instance.currentConv.GetLineByIndex(instance.currentIndex).
				dialogue));
		}
		instance.speakerSprite.sprite = instance.currentConv.GetLineByIndex(instance.currentIndex).speaker.GetSprite();
		instance.currentIndex++;
		Debug.Log("Current index: " + currentIndex);
		if (instance.currentIndex > instance.currentConv.GetLength() && !instance.currentConv.GetHaveOptions())
		{
			instance.navButtonText.text = "";
		}
		else if (instance.currentIndex > currentConv.GetLength() && instance.currentConv.GetHaveOptions())
		{
			// Show options
			instance.navButtonText.transform.parent.gameObject.SetActive(false);
			for (int i = 0; i < instance.currentConv.GetOptions().Count; i++)
			{
				var buttonInstance = Instantiate(instance.choiceButton, instance.convOptionsUI.transform);
				buttonInstance.GetComponentInChildren<TextMeshProUGUI>().text = instance.currentConv.GetOptions()[i];
				int temp = i;
				buttonInstance.onClick.AddListener(delegate { RaiseOptionPressEvent(instance.currentConv.GetOptionsID()[temp]); });
			}

		}
		
	}
    public void RaiseOptionPressEvent(string quid)
    {
        optionPress?.Invoke(quid);
    }
	private IEnumerator TypeText(string text)
	{
        instance.dialogue.text = "";
		int i = 0;
		complete = false;
		while (!complete)
		{
            instance.dialogue.text += text[i];
			i++;
			yield return new WaitForSeconds(0.001f);
			if (i == text.Length)
				complete = true;
		}
        instance.typing = null;
	}
}
