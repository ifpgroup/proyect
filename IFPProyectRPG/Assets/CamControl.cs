﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamControl : MonoBehaviour
{
    private Transform playerTransform;
    [SerializeField] private CamLimits camLimit;
    private bool isReady = false;
    public void SetPlayerTransform(Transform player) {
        playerTransform = player;
        isReady = true;
    }
    void Start()
    {
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (isReady)
        {
            Vector3 temp = transform.position;
            temp.x = playerTransform.position.x;
            temp.y = playerTransform.position.y;
            if(camLimit)
            {
                temp.x = Mathf.Clamp(temp.x, camLimit.minX, camLimit.maxX);
                temp.y = Mathf.Clamp(temp.y, camLimit.minY, camLimit.maxY);
            }
            transform.position = temp;
        }
        else
        {
            Debug.Log(isReady);
        }
    }
}
