﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestQuest : MonoBehaviour
{
	public QuestManager qm;
	public Quest q;
	private QuestTarget qt;


	/// Hacer test con ActivateQuest()
	///
	public void TakeAQuest()
	{
		Debug.Log("Aqui hay quest??");
		q = qm.GiveQuestFromList("buttonQuest");
		Debug.Log("Nombre de la quest: " + q.GetQuestName());


	}


	public void DoingQuest()
	{
		q.CheckTarget(TypeOfTargets.GETITEM, "button", 1);
		
	}

	public void FinishingTestQuest()
	{
		q.CheckTarget(TypeOfTargets.TALKTOSOMEONE, "button", 1);
		
	}
}
