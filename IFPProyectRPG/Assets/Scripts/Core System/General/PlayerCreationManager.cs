﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerCreationManager : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private List<Text> statsNumbers;
    [SerializeField] private List<Button> addButtons;
    [SerializeField] private List<Button> removeButtons;
    [SerializeField] private int maxPointsToUse;
    [SerializeField] private Text pointsLeft;
    [SerializeField] private int initialStatsPoints;
    [SerializeField] private int maxStatValue;
    [SerializeField] private int minStatValue;
    [SerializeField] private EntityFactory entityFactory;    
    [SerializeField] private List<GameObject> characterPrefabs; // 0-1 female Prefabs, 2-3 male prefabs
    [SerializeField] private List<Sprite> characterImages;
    [SerializeField] private Player player;
    [SerializeField] private Image image;
    [SerializeField] private List<Image> equipmentSprites;
    [SerializeField] private List<TextMeshProUGUI> equipmentText;
    private int currentUsedPoints;
    private Dictionary<int,int> statsPoints;
    private PlayableCharacter character;
    private GameObject selectedPrefab;
    private bool isMale;
    private bool isMage;
    private int selectedIndex;
    void Start()
    {
        statsPoints = new Dictionary<int, int>();
        currentUsedPoints = 0;
        if (initialStatsPoints < minStatValue)
            initialStatsPoints = minStatValue;
        selectedIndex = 0;
        selectedPrefab = characterPrefabs[selectedIndex];
        int i = 0;
        isMage = true;
        foreach (var item in statsNumbers)
        {
            item.text = ""+initialStatsPoints;
            int temp = i;
            statsPoints.Add(temp, initialStatsPoints);
            addButtons[temp].onClick.AddListener(delegate { AddStatPoint(temp); });
            removeButtons[temp].onClick.AddListener(delegate { SubtractStatPoint(temp); });
            currentUsedPoints += initialStatsPoints;
            i++;
        }
        isMale = true;
        int pointsAvaible = maxPointsToUse-currentUsedPoints;
        pointsLeft.text = "" + pointsAvaible;
        ShowEquipment();
    }
    public void SelectPrefab(int i)
    {
        selectedIndex = i;
        if (i == 1)
            isMage = false;
        else
            isMage = true;
        if (!isMale)
            i+=2;        
        selectedPrefab = characterPrefabs[i];
        ShowEquipment();
        ChangeSprite();
    }
    public void ShowEquipment()
    {
        var equipments = entityFactory.GetEquipmentSelected(isMage);
        int i = 0;
        foreach (var item in equipmentSprites)
        {
            item.sprite = equipments[i].GetIcon();
            equipmentText[i].SetText(equipments[i].GetName());
            i++;
        }
    }
    public void ChangeSex(bool sex)
    {
        isMale = sex;
        SelectPrefab(selectedIndex);
    }
    public void ChangeSprite()
    {
        if(isMale)
            image.sprite = characterImages[selectedIndex];
        else
            image.sprite = characterImages[selectedIndex+2];
    }
    public void AddStatPoint(int index)
    {
        if (currentUsedPoints >= maxPointsToUse)
            return;
        if (statsPoints[index] == maxStatValue)
            return;
        currentUsedPoints++;
        statsPoints[index]++;
        statsNumbers[index].text = ""+statsPoints[index];
        int temp = maxPointsToUse - currentUsedPoints;
        pointsLeft.text = "" + temp;
    }
    public void SubtractStatPoint(int index)
    {
        if (statsPoints[index] <= minStatValue)
            return;
        currentUsedPoints--;
        statsPoints[index]--;
        statsNumbers[index].text = "" + statsPoints[index];
        int temp = maxPointsToUse - currentUsedPoints;
        pointsLeft.text = "" + temp;
    }

    public void Continue()
    {
        if (currentUsedPoints != maxPointsToUse) // prob send a message later on!
            return;
        Stats stats = new Stats();
        stats.strength = statsPoints[0];
        stats.dexterity = statsPoints[1];
        stats.constitution = statsPoints[2];
        stats.vitality = statsPoints[3];
        stats.intelligence = statsPoints[4];
        stats.wisdom = statsPoints[5];
        character = entityFactory.CreatePlayer(stats, isMage);
        player.AddPlayerToParty(character);
        player.GetInventory().AddItem(character.GetEquipment(TypeOfEquipment.Weapon));
        player.GetInventory().AddItem(character.GetEquipment(TypeOfEquipment.Armor));
        player.GetInventory().AddItem(character.GetEquipment(TypeOfEquipment.Trinket));
        player.GetCollection().UnlockCards(player.GetParty()) ;
        for (int i = 0; i < player.GetPartyMember(0).GetDeck().GetCards().Count; i++)
        {
            player.GetCollection().UseCard(player.GetPartyMember(0).GetDeck().GetCards()[i], player.GetPartyMember(0));
        }
        InteractableItems.Reset();
        CombatManager.isFixedEncounter = false;
        CombatManager.isFixedEncounterDone = false;
        player.AddPlayerPrefab( selectedPrefab);
        SceneManager.LoadScene("Town");
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
