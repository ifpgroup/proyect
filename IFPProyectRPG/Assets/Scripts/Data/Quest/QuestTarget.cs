﻿#pragma warning disable 0649
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TypeOfTargets { KILLENEMIES, TALKTOSOMEONE, GETITEM};
[System.Serializable]
public class QuestTarget
{
    [SerializeField] private string targetID;
    [SerializeField] private TypeOfTargets typeOfTargets;
    [SerializeField] private int numberOfTypes;
    [SerializeField] private int numberCompleted;
    [SerializeField]
    [TextArea(2, 8)] 
    private string targetDescription;
	[SerializeField] private bool checkTarget;

    public QuestTarget(QuestTarget data)
    {
        targetID = data.GetTargetID() ;
        typeOfTargets = data.GetTypeOfTargets();
        numberOfTypes = data.GetNumberOfTypes();
        targetDescription = data.GetTargetDescription();
        checkTarget = false;
        numberCompleted = 0;

    }
    public void IncreaseNumberCompleted(int ammount) => numberCompleted += ammount;
    public bool CheckCompleted() => numberOfTypes <= numberCompleted;
    public QuestTarget(QuestTarget data, bool completed)
    {
        targetID = data.GetTargetID();
        typeOfTargets = data.GetTypeOfTargets();
        numberOfTypes = data.GetNumberOfTypes();
        targetDescription = data.GetTargetDescription();
        checkTarget = completed;

    }
    public string GetTargetID() => targetID;
    public TypeOfTargets GetTypeOfTargets() => typeOfTargets;
    public int GetNumberOfTypes() => numberOfTypes;
    public int SetNumberOfTypes(int num) => numberOfTypes = num;
    public string GetTargetDescription() => targetDescription;
    public bool GetCheckTarget() => checkTarget;
	public bool SetCheckTarget(bool checkT) => checkTarget = checkT;
}
