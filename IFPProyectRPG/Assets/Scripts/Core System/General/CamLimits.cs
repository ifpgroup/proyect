﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CamLimit", menuName = "Scriptable Object/Create Camera Limit")]
public class CamLimits : ScriptableObject
{
    public int maxY;
    public int minY;
    public int maxX;
    public int minX;
}
