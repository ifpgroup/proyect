﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarShrink : MonoBehaviour
{
    private const float SHRINK_TIMER_MAX = 1.5f;
    private Image barImage;
    private Image damagedBarImage;
    private float damagedHealthTimer;
    private Image background;
    private GameObject parent;
    public bool isMini = true;

    private void Awake()
    {
        if(isMini)
            parent = transform.parent.gameObject;
        background = transform.Find("Background").GetComponent<Image>();
        barImage = transform.Find("Bar").GetComponent<Image>();
        damagedBarImage = transform.Find("DamagedBar").GetComponent<Image>();
    }

    private void Update()
    {
        damagedHealthTimer -= Time.deltaTime;
        if(damagedHealthTimer < 0)
        {
            if(barImage.fillAmount < damagedBarImage.fillAmount)
            {
                float shrinkSpeed = 1f;
                damagedBarImage.fillAmount -= shrinkSpeed * Time.deltaTime;
            }
            else if(isMini)
            {
                parent.GetComponent<ShowMouseOver>().ShowPanel(false);
            }
        }
    }

    public void OnHeal(float newHealth)
    {
        if (isMini)
            parent.GetComponent<ShowMouseOver>().ShowPanel(true);
        damagedHealthTimer = SHRINK_TIMER_MAX;
        SetHealthAndDamagedHealth(newHealth);
    }

    public void OnDamage(float newHealth)
    {
        if(isMini)
            parent.GetComponent<ShowMouseOver>().ShowPanel(true);
        damagedHealthTimer = SHRINK_TIMER_MAX;
        SetHealth(newHealth);
    }

    public void SetHealth(float healthNormalized)
    {
        barImage.fillAmount = healthNormalized;
    }

    public void SetHealthAndDamagedHealth(float healthNormalized)
    {
        damagedHealthTimer = 0;
        barImage.fillAmount = healthNormalized;
        damagedBarImage.fillAmount = healthNormalized;
    }

    public void SetAlpha(float a)
    {
        Color aux;

        aux = barImage.color;
        aux.a = a;
        barImage.color = aux;

        aux = background.color;
        aux.a = a;
        background.color = aux;

        aux = damagedBarImage.color;
        aux.a = a;
        damagedBarImage.color = aux;
    }   
    
    public void AddAlpha(float a)
    {
        Color aux;

        aux = barImage.color;
        aux.a += a;
        barImage.color = aux;

        aux = background.color;
        aux.a += a;
        background.color = aux;

        aux = damagedBarImage.color;
        aux.a += a;
        damagedBarImage.color = aux;
    }    

    public void SubAlpha(float a)
    {
        Color aux;

        aux = barImage.color;
        aux.a -= a;
        barImage.color = aux;

        aux = background.color;
        aux.a -= a;
        background.color = aux;

        aux = damagedBarImage.color;
        aux.a -= a;
        damagedBarImage.color = aux;
    }

    public float GetAlpha()
    {
        return barImage.color.a;
    }
}
