﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UICustomizeDeckManager : MonoBehaviour
{
    public GameObject customizePanels;
    public TextMeshProUGUI totalText;
    public Transform collectionZone;
    public Transform deckZone;
    public GameObject cardPrefab;
    public GameObject prefabUnusableCardText;
    public Transform canvasT;
    private Player player;
    private Color originalColor;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("PlayerManager").GetComponent<Player>();
    }

    private void Start()
    {
        originalColor = totalText.color;
        InitializeZones(player.GetDeckCards(0), player.GetCardsUnlocked());
        if (deckZone.childCount == player.GetDeckSize())
        {
            totalText.color = new Color(235f / 255f, 20f / 255f, 12f / 255f, 1.0f);
        }
        else
        {
            totalText.color = originalColor;
        }
        totalText.text = deckZone.childCount + "/" + (player.GetDeckSize() - 3);
        customizePanels.SetActive(false);
    }

    public void ShowPanels()
    {
        customizePanels.SetActive(!customizePanels.activeSelf);
        if (customizePanels.activeSelf)
        {
            InitializeZones(player.GetDeckCards(0), player.GetCardsUnlocked());
        }
    }

    public void InitializeZones(List<Card> deck, List<Card> collection)
    {
        RemoveCards();
        Debug.Log(collection.Count);
        for (int i = 3; i < deck.Count; i++)
        {
            
            GameObject aux = Instantiate(cardPrefab, deckZone);
            aux.GetComponent<CustomizeDraggableCard>().SetCard(deck[i]);
            aux.GetComponent<CustomizeDraggableCard>().uiCustomizeDeckManager = this;
            aux.GetComponent<CustomizeDraggableCard>().SetIsFromDeck(true);
        }
        
        foreach (Card card in collection)
        {
            GameObject aux = Instantiate(cardPrefab, collectionZone);
            aux.GetComponent<CustomizeDraggableCard>().SetCard(card);
            aux.GetComponent<CustomizeDraggableCard>().uiCustomizeDeckManager = this;
            aux.GetComponent<CustomizeDraggableCard>().SetIsFromDeck(false);
        }
       
    }

    private void RemoveCards()
    {
        for(int i = deckZone.childCount - 1; i >= 0; i--)
        {
            Destroy(deckZone.GetChild(i).gameObject);
        }
        for (int i = collectionZone.childCount - 1; i >= 0; i--)
        {
            Destroy(collectionZone.GetChild(i).gameObject);
        }
        deckZone.DetachChildren();
        collectionZone.DetachChildren();
    }

    public void MoveCardTo(GameObject go, string zone)
    {
        switch (zone)
        {
            case "Deck":
                go.transform.SetParent(deckZone);
                go.GetComponent<CustomizeDraggableCard>().SetOriginalParent(deckZone);
                player.EquipCard(go.GetComponent<CustomizeDraggableCard>().GetCard());
                break;
            case "Collection":
                go.transform.SetParent(collectionZone);
                go.GetComponent<CustomizeDraggableCard>().SetOriginalParent(collectionZone);
                player.UnEquipCard(go.GetComponent<CustomizeDraggableCard>().GetCard());
                break;
            default: break;
        }
        if(deckZone.childCount == player.GetDeckSize())
        {
            totalText.color = new Color(235f/255f, 20f/255f, 12f/255f, 1.0f);
        }
        else
        {
            totalText.color = originalColor;
        }
        totalText.text = deckZone.childCount + "/" + (player.GetDeckSize()-3); // we take away the 3 from the equipment
    }

    public bool Fits()
    {
        return deckZone.childCount < (player.GetDeckSize() - 3);
    }

    public void ShowUnmovableCard(string str)
    {
        GameObject aux = Instantiate(prefabUnusableCardText, canvasT);
        aux.GetComponent<TextMeshProUGUI>().text = str;
    }
}
