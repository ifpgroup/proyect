﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    // public AudioSource dungeonAudio;
    //public AudioSource townAudio;

    public AudioSource dialogueOn;
    public AudioSource dialogueOff;
    public AudioSource selectInMenu;

    public AudioSource treasure;
    public AudioSource door;

    public AudioSource onTheCard;
    public AudioSource clickTheCard;
    public AudioSource attack;
    public AudioSource magicAttack;
    public AudioSource lifeUp;
    public AudioSource manaUp;
    public AudioSource defense;


    void Start()
    {
        
    }

    void Awake()
    {
        //DontDestroyOnLoad(townAudio);
        //DontDestroyOnLoad(dungeonAudio);
    }
    public void PlayDialogueOn()
    {
        dialogueOn.Play();
    }

    public void PlayDialogueOff()
    {
        dialogueOff.Play();
    }

    public void PlaySelectInMenu()
    {
        selectInMenu.Play();
    }

    public void PlayTreasure()
    {
        treasure.Play();
    }

    public void PlayDoor()
    {
        door.Play();
    }

    public void PlayOnTheCard()
    {
        onTheCard.Play();
    }

    public void PlayClickTheCard()
    {
        clickTheCard.Play();
    }

    public void PlayAttack()
    {
        attack.Play();
    }

    public void PlayMagicAttack()
    {
        magicAttack.Play();
    }

    public void PlayLifeUp()
    {
        lifeUp.Play();
    }

    public void PlayManaUp()
    {
        manaUp.Play();
    }

    public void PlayDefense()
    {
        defense.Play();
    }
}
