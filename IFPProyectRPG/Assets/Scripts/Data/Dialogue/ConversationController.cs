﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class QuestionEvent : UnityEvent<Question> { }

public class ConversationController : MonoBehaviour
{
	public DialogueManager dialogueManager;
	
    public Conversation conversation;
    public QuestionEvent questionEvent;

    /*public void ChooseOption()
	{
		if (conversation.GetHaveOptions() == true)
			questionEvent.Invoke(conversation.question);
		else
			dialogueManager.ReadNext();
	}*/
}
