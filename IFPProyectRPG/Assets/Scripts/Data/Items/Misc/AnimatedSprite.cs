﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedSprite : MonoBehaviour
{
    public Sprite[] sprites;
    public float time = 0.5f;

    private SpriteRenderer spriteRenderer;
    private int count;
    private float timer;

    void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        count = 0;
        timer = time;
    }

    void Update()
    {
        if(timer <= 0)
        {
            count++;
            if(count > sprites.Length - 1)
            {
                count = 0;
            }
            spriteRenderer.sprite = sprites[count];
            timer = time;
        }
        else
        {
            timer -= Time.deltaTime;
        }
    }
}
