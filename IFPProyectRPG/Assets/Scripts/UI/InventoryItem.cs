﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private int indexInGrid;
    private Image icon;
    public UIDungeonManager uiDungeonManager;
    private bool equipable;
    private Vector2 originalPos;
    private Transform originalT;
    private EquipSlot equipSlotBeginDrag;
    private bool draggedToSell;
    private Item item;
    private Equipment equipment;
    private int amount;
    private bool movable;

    void Awake()
    {
        movable = true;
        draggedToSell = false;
        originalPos = transform.position;
        icon = transform.GetChild(0).gameObject.GetComponent<Image>();
        equipable = false;
        equipSlotBeginDrag = null;
        originalT = transform.parent;
    }

    public void SetItem(Item i)
    {
        item = i;
    }

    public void SetEquipment(Equipment e)
    {
        equipment = e;
    }

    public void SetIndex(int id)
    {
        indexInGrid = id;
    }

    public int GetIndex()
    {
        return indexInGrid;
    }

    public void SetMovable(bool b)
    {
        movable = b;
    }

    public bool GetMovable()
    {
        return movable;
    }

    public void SetAmount(int i)
    {
        amount = i;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!movable) return;
        if (!uiDungeonManager.IsEquipable(item))
        {
            equipable = false;
        }
        else
        {
            equipable = true;
        }


        PointerEventData pointerData = new PointerEventData(EventSystem.current)
        {
            pointerId = -1,
        };

        EquipSlot equipSlot = null;

        pointerData.position = Mouse.current.position.ReadValue();

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, results);

        foreach (RaycastResult result in results)
        {
            if (result.gameObject.GetComponent<EquipSlot>() != null)
            {
                equipSlot = result.gameObject.GetComponent<EquipSlot>();
                break;
            }
        }

        if (equipSlot != null)
        {
            equipSlotBeginDrag = equipSlot;
        }
        else
        {
            equipSlotBeginDrag = null;
            transform.SetParent(uiDungeonManager.canvasT);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!movable) return;
        if (equipSlotBeginDrag != null) return;

        transform.position = Mouse.current.position.ReadValue();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!movable) return;
        if (equipSlotBeginDrag != null) return;

        PointerEventData pointerData = new PointerEventData(EventSystem.current)
        {
            pointerId = -1,
        };

        EquipSlot equipSlot = null;

        pointerData.position = Mouse.current.position.ReadValue();

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, results);

        foreach(RaycastResult result in results)
        {
            if(result.gameObject.GetComponent<EquipSlot>() != null)
            {
                equipSlot = result.gameObject.GetComponent<EquipSlot>();
                break;
            }
            else if(result.gameObject.name == "SellPanel")
            {
                draggedToSell = true;
                break;
            }
        }


        if (draggedToSell)
        {
            amount--;
            uiDungeonManager.SellItem(item);
            uiDungeonManager.UpdateInventory();
            uiDungeonManager.HideToolTip();
            uiDungeonManager.ShowInfoPopUp(item.GetName() + "<color=red> -1</color>");
            //uiDungeonManager.UpdateShop(item);
            Destroy(gameObject);
        }
        else 
        {
            if(equipSlot != null)
            {
                if (equipable)
                {
                    uiDungeonManager.EquipItem(gameObject, item);
                    uiDungeonManager.UpdateInventory();
                    uiDungeonManager.UpdateEquipment();
                }
                else
                {
                    equipSlotBeginDrag = null;
                    transform.SetParent(originalT);
                }
            }
            else
            {
                equipSlotBeginDrag = null;
                transform.SetParent(originalT);
            }
        }
    }
}
