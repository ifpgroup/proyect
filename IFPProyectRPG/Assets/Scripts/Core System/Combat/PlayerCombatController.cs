﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombatController : MonoBehaviour, IController
{
    [SerializeField] private CombatManager combatManager;
    private List<EntityCombat> party;
    private bool cardBeingUsed = false;
    private IUIManager UICombatManager;
    private int indexCurrentEntityTurn = 0;
    private bool isMyTurn = false;
    private List<Card> hand;
    private EntityCombat currentPartyMemberTurn => party[indexCurrentEntityTurn];
    public void AddCreature(EntityCombat entity)
    {
        Debug.Log("Added Creature : " + entity.GetUID());
        if (party == null) party = new List<EntityCombat>();
        party.Add(entity);
        //UICombatManager.GetEntityData(entity.GetAttr(), entity.GetActionsRemaining(), !entity.GetSupportActionUsed(), entity.GetUID(), true);
    }

    public void DrawCard()
    {
        currentPartyMemberTurn.DrawCard();
        hand = currentPartyMemberTurn.GetHand();
    }

    public void EndTurn()
    {
        // call from UI
        indexCurrentEntityTurn++;
        if (indexCurrentEntityTurn >= party.Count)
            indexCurrentEntityTurn = 0;
        isMyTurn = false;
        combatManager.EndCurrentEntityTurn();
    }

    public void GotAttacked(int uid, float damage)
    {
        Debug.Log(currentPartyMemberTurn.GetAttr().hp);
    }

    public void IsDead(EntityCombat entity)
    {
        Debug.Log("called party members: " + party.Count);
        combatManager.EntityKilled(entity);
        party.Remove(entity);
        Debug.Log("called party members: " + party.Count);
        if (party.Count <= 0)
            combatManager.EndCombat(false);
    }

    public void IsMyTurn()
    {
        isMyTurn = true;
        DrawCard();
        UICombatManager.StartTurn(hand, false, currentPartyMemberTurn.GetCementery().Count);
        UICombatManager.GetEntityData(currentPartyMemberTurn.GetAttr(), currentPartyMemberTurn.GetActionsRemaining(), !currentPartyMemberTurn.GetSupportActionUsed(), currentPartyMemberTurn.GetUID(),true);
    }

    public void PlayCard(int index, int[] targetsUID)
    {
        // call from UI
        if (!isMyTurn) Debug.LogError("Is not the players turn!!! this shouldnt have been call");
        if (cardBeingUsed) Debug.LogError("There is already one card being played!!! this shouldnt have been call");
        //if (currentPartyMemberTurn.GetActionsRemaining() <= 0) return;
        if (hand[index].GetEffects.isSupport && currentPartyMemberTurn.GetSupportActionUsed()) return;
        cardBeingUsed = true;
        CardResult result = currentPartyMemberTurn.PlayCard(index);
        switch (result.CardType)
        {
            case CardType.Offensive:
                currentPartyMemberTurn.UseCardNormalAction();
                foreach (var targetUID in targetsUID)
                {
                    UICombatManager.CardResultsAttack(combatManager.AttackEntity(result, targetUID, currentPartyMemberTurn));
                }                
                break;
            case CardType.Deffensive:
                currentPartyMemberTurn.UseCardNormalAction();
                foreach (var targetUID in targetsUID)
                {
                    ProcessDefenseCard(result, targetUID);
                }
                //Habrá que tocar cuando se metan aoes
                UICombatManager.CardResult(result, currentPartyMemberTurn.GetAttr(), true);
                break;
            case CardType.Support:
                currentPartyMemberTurn.UseSupportCard();
                Debug.Log(result.TypeOfEffect);                
                foreach (var targetUID in targetsUID)
                {
                    foreach (var member in party)
                    {
                        if(member.GetUID() == targetUID)
                        {

                            if (result.TypeOfEffect == TypeOfEffect.ExtraAction)
                                member.IncreaseNormalActions();
                            else if (result.TypeOfEffect == TypeOfEffect.Healing)
                                member.Heal(result.newValue.value);
                            else if (result.TypeOfEffect == TypeOfEffect.RegenEnergy)
                                member.RegenEnergy(result.newValue.value);
                            else if (result.TypeOfEffect == TypeOfEffect.RegenMana)
                                member.RegenMana(result.newValue.value);
                            break;
                        }
                    }
                }
                //Habrá que tocar cuando se metan aoes
                UICombatManager.CardResult(result, currentPartyMemberTurn.GetAttr(), true);
                break;
            default:
                break;
        }
    }

    public void PlayCard(int index, int targetUID)
    {

        // Call from UI
        if(!isMyTurn) Debug.LogError("Is not the players turn!!! this shouldnt have been call");
        //if (cardBeingUsed) Debug.LogError("There is already one card being played!!! this shouldnt have been call");
        //if (currentPartyMemberTurn.GetActionsRemaining() <= 0) return;
        if (hand[index].GetEffects.isSupport && currentPartyMemberTurn.GetSupportActionUsed()) return;
        cardBeingUsed = true;
        CardResult result = currentPartyMemberTurn.PlayCard(index);
        Debug.Log(index);
        Debug.Log(result.value);
        
        switch (result.CardType)
        {
            case CardType.Offensive:
                Debug.Log("Entity attacked is: " + targetUID + " TypeOfCard: " + result.CardType);
                currentPartyMemberTurn.UseCardNormalAction();
                UICombatManager.CardResultsAttack(combatManager.AttackEntity(result, targetUID, currentPartyMemberTurn));
                break;
            case CardType.Deffensive:
                currentPartyMemberTurn.UseCardNormalAction();
                ProcessDefenseCard(result);
                UICombatManager.CardResult(result, currentPartyMemberTurn.GetAttr(), true);
                break;
            case CardType.Support:
                currentPartyMemberTurn.UseSupportCard();
                Debug.Log(result.TypeOfEffect);
                if (result.TypeOfEffect == TypeOfEffect.ExtraAction)
                    currentPartyMemberTurn.IncreaseNormalActions();
                else if (result.TypeOfEffect == TypeOfEffect.Healing)
                    currentPartyMemberTurn.Heal(result.newValue.value);
                else if (result.TypeOfEffect == TypeOfEffect.RegenEnergy)
                    currentPartyMemberTurn.RegenEnergy(result.newValue.value);
                else if (result.TypeOfEffect == TypeOfEffect.RegenMana)
                    currentPartyMemberTurn.RegenMana(result.newValue.value);
                UICombatManager.CardResult(result, currentPartyMemberTurn.GetAttr(), true);
                break;
            default:
                break;
        }
    }
    private void ProcessDefenseCard(CardResult result)
    {
        TypeOfEffect effect = result.TypeOfEffect;
        currentPartyMemberTurn.ProcessDefenseEffect(effect, result.newValue);
        if(result.newSpecialEffects != null)
            foreach (var item in result.newSpecialEffects)
            {
                currentPartyMemberTurn.ProcessDefenseEffect(item.Key, item.Value);
            }
    }

    

    private void ProcessDefenseCard(CardResult result, int UIDTarget)
    {
        // find entity and use the process of that entity.
    }
    public void CardAnimationOver()
    {
        // Call from UI
        cardBeingUsed = false;
    }
    // Start is called before the first frame update
    void Start()
    {
        UICombatManager = GameObject.FindGameObjectWithTag("UIManager").GetComponent<IUIManager>();
    }
}
