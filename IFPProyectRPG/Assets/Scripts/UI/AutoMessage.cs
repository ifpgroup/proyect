﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AutoMessage : MonoBehaviour
{
    private Animator anim;
    public TextMeshProUGUI txt;
    private AudioController audioController;

    private void Start()
    {
        anim = GetComponent<Animator>();
        audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
    }

    public string SetTextAutoMessage(string t) => txt.text = t;

    public void OpenMessage()
    {
        anim.SetBool("openAutoMessage", true);
    }

    public void CloseMessage()
    {
        anim.SetBool("openAutoMessage", false);
    }
}
