﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RaycastTesting : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Vector2 Dir;
    [SerializeField] private float dist;
    private RaycastHit2D hit;
    void Start()
    {
        
    }

    public void InteractClick()
    {
        hit = Physics2D.Raycast(this.transform.position, Dir, dist);
        Debug.DrawRay(this.transform.position, Dir);
        if (hit)
        {
            if (hit.collider.gameObject.tag.Equals("InteractuableItem"))
                Debug.Log(hit.collider.gameObject.tag);
    
        }
    }
    // Update is called once per frame
    void Update()
    {        
    }
}
