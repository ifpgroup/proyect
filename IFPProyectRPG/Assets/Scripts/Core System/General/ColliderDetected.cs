﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderDetected : MonoBehaviour
{
    // Start is called before the first frame update
    private Player player;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("PlayerManager")?.GetComponent<Player>();
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.tag);
        if (collision.gameObject.tag.Equals("mazEntry"))
        {
            player.SetLastPosition(transform.position);
            player.SetStateSceneName(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
            player.GoForwardOrBack(false);
            UnityEngine.SceneManagement.SceneManager.LoadScene("Maz_01");
            return;
        }
        if (collision.gameObject.tag.Equals("mazExit"))
        {
            player.SetLastPosition(transform.position);
            player.SetStateSceneName(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
            player.GoForwardOrBack(true);
            UnityEngine.SceneManagement.SceneManager.LoadScene("Town");
        }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.tag);
        if (collision.gameObject.tag.Equals("InteractuableItem"))
        {
            var script = collision.gameObject.GetComponent<InteractableItem>();
            if(script.GetTypeOfItem() == TypeOfInteractableItem.TRAP)
            {
                script.PlayAction();
            }
        }
        if (collision.gameObject.tag.Equals("mazExit"))
        {
            player.SetLastPosition(transform.position);
            player.SetStateSceneName(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
            player.GoForwardOrBack(true);
            UnityEngine.SceneManagement.SceneManager.LoadScene("Town");
        }
        if (collision.gameObject.tag.Equals("StoreExit"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Town");
        }
        if (collision.gameObject.tag.Equals("FinalEncounter"))
        {
            CombatManager.isFixedEncounter = true;
            GameObject.FindGameObjectWithTag("EncounterController").GetComponent<RandomEncounter>().startCombat();
        }
    }


}
