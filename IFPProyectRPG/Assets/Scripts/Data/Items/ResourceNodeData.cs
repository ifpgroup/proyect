﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ResourceNode", menuName = "Scriptable Object/Create Resource Node")]
public class ResourceNodeData : ScriptableObject
{
    [SerializeField] private TypeOfResource resource;
    [SerializeField] private int maxAmmount;
    public TypeOfResource GetResource() => resource;
    public int GetMaxAmmount() => maxAmmount;
}
