﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Enemy : Entity
{
    private EnemyType enemyType;
    public static event UnityAction<string> OnDead;
    public Enemy(EnemyType type, int level)
    {
        enemyType = type;
        name = enemyType.GetEnemyName();
        baseStats = type.GetFinalStats(level);
        CalculateAttributes();
        CreateDeck();
    }
    protected override void CalculateAttributes()
    {
        finalAttributes.hp = EntitiesPrefs.baseHP/2 + baseStats.vitality * EntitiesPrefs.baseConHPMultEnemy[GetModifer(baseStats.vitality)]/2;
        finalAttributes.mana = EntitiesPrefs.baseMana + baseStats.wisdom * EntitiesPrefs.baseManEnergyMultEnemy[GetModifer(baseStats.wisdom)];
        finalAttributes.energy = EntitiesPrefs.baseEnergy + baseStats.constitution * EntitiesPrefs.baseManEnergyMultEnemy[GetModifer(baseStats.constitution)];

        finalAttributes.energyRecoverPerTurn = EntitiesPrefs.baseManaEnergyRegen + baseStats.constitution * EntitiesPrefs.baseManaEnergyRegenMultEnemy[GetModifer(baseStats.constitution)];
        finalAttributes.manaRecoverPerTurn = EntitiesPrefs.baseManaEnergyRegen + baseStats.wisdom * EntitiesPrefs.baseManaEnergyRegenMultEnemy[GetModifer(baseStats.wisdom)];

        finalAttributes.armor = EntitiesPrefs.baseArmor + (int)(baseStats.constitution * EntitiesPrefs.baseArmorMultEnemy);
        finalAttributes.mArmor = EntitiesPrefs.baseMArmor + (int)(baseStats.wisdom * EntitiesPrefs.baseMArmorMultEnemy);

        finalAttributes.intMult = baseStats.intelligence / 10.0f;
        finalAttributes.strMult = baseStats.strength / 10.0f;
        if (enemyType.IsBoss())
        {
            AddModifiers(enemyType.GetModBoss());

        }
        finalAttributes.maxHp = finalAttributes.hp;
        finalAttributes.maxMana = finalAttributes.mana;
        finalAttributes.maxEnergy = finalAttributes.energy;
        
    }
    private void AddModifiers(EntityAttributes modifiers)
    {
        finalAttributes.hp += modifiers.hp;
        finalAttributes.mana += modifiers.mana;
        finalAttributes.energy += modifiers.energy;
        finalAttributes.energyRecoverPerTurn += modifiers.energyRecoverPerTurn;
        finalAttributes.manaRecoverPerTurn += modifiers.manaRecoverPerTurn;
        finalAttributes.armor += modifiers.armor;
        finalAttributes.mArmor += modifiers.mArmor;
        finalAttributes.intMult += modifiers.intMult;
        finalAttributes.strMult += modifiers.strMult;
    }
    private int GetModifer(int attr)
    {
        int tempValue = attr - 10;
        if(tempValue > 0)
        {
            tempValue = tempValue / 2;
            if (tempValue > 5)
                tempValue = 5;
        }
        else
        {
            tempValue = 0;
        }

        return tempValue;
    }
    public override bool CreateDeck()
    {
        entityActions = new Deck(this, enemyType.GetActions());
        return true;
    }

    public override void Dead()
    {
        isDead = true;
        if (isDead)
            OnDead?.Invoke(name);
    }

    public override void DrawCard()
    {
        throw new System.NotImplementedException();
    }

    public override float CalculateDamage(CardResult cardUsed)
    {
        float damage;
        if(cardUsed.CardAttribute == CardAttribute.Intelligence)
        {
            damage =cardUsed.newValue.value * 10 / (10 + finalAttributes.mArmor);
        }
        else
        {
            damage =cardUsed.newValue.value * 10 / (10 + finalAttributes.armor);
        }
        return damage;
    }
}
