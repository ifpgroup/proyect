﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Conversation", menuName = "Scriptable Object/Dialogue/Conversation", order = 4)]
public class Conversation : ScriptableObject
{    
    [SerializeField] private string conversationID;
    [SerializeField] private bool questRefusedStartLine;
    [SerializeField] public DialogueLine[] allLines;
    [SerializeField] private bool haveOptions;
    [SerializeField] private List<string> options;
    [SerializeField] private List<string> optionsID;
    public Question question;
    public string GetID() => conversationID;
    public List<string> GetOptions() => options;
    public List<string> GetOptionsID() => optionsID;
    public bool GetQuestRefusedStartLine() => questRefusedStartLine;
    public bool GetHaveOptions() => haveOptions;
    public DialogueLine[] GetAllLines() => allLines;

    public DialogueLine GetLineByIndex(int index)
	{
        return allLines[index];
	}

    public int GetLength()
    {
        return allLines.Length - 1;
    }
}
