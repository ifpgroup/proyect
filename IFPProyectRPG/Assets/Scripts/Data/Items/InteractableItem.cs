﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeOfInteractableItem { DOOR, STAIRS, RESOURCENODE,TREASURE,TRAP}
public abstract class InteractableItem : MonoBehaviour
{
    [SerializeField] protected TypeOfInteractableItem typeOfItem;
    [SerializeField] protected string nameOfInteractableItem;

    public abstract void PlayAction();
    public abstract void SetData(ScriptableObject data, int i);
    public TypeOfInteractableItem GetTypeOfItem() => typeOfItem;
    public string GetName() => nameOfInteractableItem;
    public void Start()
    {
        gameObject.tag = "InteractuableItem";
    }
}
