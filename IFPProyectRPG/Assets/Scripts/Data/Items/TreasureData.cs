﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Treasure", menuName = "Scriptable Object/Create Treasure")]
public class TreasureData : ScriptableObject
{
    [SerializeField] private List<Item> listPossibleItems;
    [SerializeField] private List<int> listDropChance;
    [SerializeField] private List<int> listMaxAmmount;

    public List<Item> GetItems() => listPossibleItems;
    public List<int> GetListDropChance() => listDropChance;
    public List<int> GetListMaxAmmount() => listMaxAmmount;
}
