﻿#pragma warning disable 0649
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

[System.Serializable]
public class QuestManager : MonoBehaviour
{
    [SerializeField] private List<QuestData> questsInfo;   
    public List<Quest> quests = new List<Quest>();
    
    private QuestRequirements qr;
    private QuestTarget qt;
    private QuestData qd;


    void Start()
    {
        foreach (QuestData qD in questsInfo)
        {
            Quest newQuest = new Quest(qD, false, false);
            quests.Add(newQuest);
        }
        DontDestroyOnLoad(this);
    }

    public List<Quest> GetQuests() => quests;

    public Quest GiveQuestFromList(string QuestID)
	{
        //if (requirements.CheckRequirements())
        //{
            foreach (Quest q in quests)
            {
                if (q.GetQuestID() == QuestID)
                {
                    Debug.Log("Giving Quest: " + q.GetQuestName());
                    return q;
                }
          //  }
        }
        Debug.Log("Give Quest no funciona, return null");
        return null;   
    }

}

