﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct statusEffect
{
    public bool isActive;
    public int duration;
    public float value;
}
public class EntityCombat
{
    private Entity entityData;
    private Dictionary<TypeOfEffect, statusEffect> hasStatusEffect;   
    private IController entityController;
    private AudioController sound;
    public readonly bool isEnemy;
    private int UID;    
    private bool isSupportActionUsed;
    private int remainingActions;
    private List<TypeOfEffect> listOfEffects;
    public int GetUID() => UID;
    public bool CheckUID(int UIDTarget) => UID == UIDTarget;
    public bool CheckCost(int cost, bool isMana) => entityData.CheckCost(cost, isMana);
    
    public EntityCombat(Entity data, IController controller, int UID, bool isAIControlled, AudioController sound)
    {
        entityData = data;
        entityController = controller;
        this.UID = UID;
        isEnemy = isAIControlled;
        remainingActions = EntitiesPrefs.baseNumActions;
        isSupportActionUsed = false;
        entityData.GetDeck().StartCombat();
        listOfEffects = new List<TypeOfEffect>();
        hasStatusEffect = new Dictionary<TypeOfEffect, statusEffect>();
        this.sound = sound;
    }
    public int GetInit() => entityData.GetInit();
    public List<Card> GetCementery() => entityData.GetDeck().GetCementery();
    public List<Card> GetRemainingCards() => new List<Card>(entityData.GetDeck().GetCardsRemaining());
    public List<Card> GetHand() => entityData.GetDeck().GetHand();
    public void DrawCard() => entityData.GetDeck().DrawCard();    
    public IController GetController() => entityController;
    public EntityAttributes GetAttr() => entityData.GetAttr;
    public int GetActionsRemaining() => remainingActions;
    public bool GetSupportActionUsed() => isSupportActionUsed;
    public void UseCardNormalAction() => remainingActions--;
    public void UseSupportCard() => isSupportActionUsed = true;
    public CardResult PlayCard(int index)
    {
        CardResult result = entityData.GetDeck().PlayCard(index);
        entityData.ApplyCost(result.newCost.costMana, true);
        entityData.ApplyCost(result.newCost.costEnergy, false);
        return result;
            
    }
    public void Heal(float value) 
    { 
        entityData.Heal(value);
        sound.PlayLifeUp();
    }
    public void RegenMana(float value) 
    { 
        entityData.Regen(true, value);
        sound.PlayManaUp();
    }
    public void RegenEnergy(float value) 
    { 
        entityData.Regen(false, value);
        sound.PlayManaUp();
    }
    public void GetMyTurn()
    {
        
        Debug.Log(isEnemy + " My UID IS: " + UID + " and my hp left is: " + entityData.GetAttr.hp);
        remainingActions = 1;
        isSupportActionUsed = false;
        entityData.Recovery();
        sound.PlayManaUp();
        if (hasStatusEffect.ContainsKey(TypeOfEffect.Stun))
        {

            if (hasStatusEffect[TypeOfEffect.Stun].duration >= 1)
            {
                remainingActions = 0;
                isSupportActionUsed = true;
                Debug.Log("Iam stun");
            }
        }
        for(int i=0;i<listOfEffects.Count;i++)
        {
            var key = listOfEffects[i];
            if (!hasStatusEffect.ContainsKey(key)) continue;
            var item = hasStatusEffect[key];
            statusEffect effect = item;
            effect.duration -= 1;
            if (effect.duration <= 0)
                effect.isActive = false;
            hasStatusEffect[key] = effect;            
        }
        entityController.IsMyTurn();
    }
    public float CheckDamage(float damage, CardAttribute attribute)
    {
        /// this will check if there is any effects that reduce damage
        if (hasStatusEffect.ContainsKey(TypeOfEffect.ExtraArmor) && hasStatusEffect[TypeOfEffect.ExtraArmor].isActive && attribute == CardAttribute.Strength)
        {
            damage -= hasStatusEffect[TypeOfEffect.ExtraArmor].value;
        }
        else if (hasStatusEffect.ContainsKey(TypeOfEffect.ExtraMagicArmor) && hasStatusEffect[TypeOfEffect.ExtraMagicArmor].isActive && attribute == CardAttribute.Intelligence)
        {
            damage -= hasStatusEffect[TypeOfEffect.ExtraMagicArmor].value;
        }
        if (hasStatusEffect.ContainsKey(TypeOfEffect.PhysicalDamageReduction) && hasStatusEffect[TypeOfEffect.PhysicalDamageReduction].isActive && attribute == CardAttribute.Strength)
        {
            damage = damage - damage * hasStatusEffect[TypeOfEffect.PhysicalDamageReduction].value / 100;
        } else if (hasStatusEffect.ContainsKey(TypeOfEffect.MagicDamageReduction) && hasStatusEffect[TypeOfEffect.MagicDamageReduction].isActive && attribute == CardAttribute.Intelligence)
        {
            damage = damage - damage * hasStatusEffect[TypeOfEffect.MagicDamageReduction].value / 100;
        } else if (hasStatusEffect.ContainsKey(TypeOfEffect.DamageReduction) && hasStatusEffect[TypeOfEffect.DamageReduction].isActive && attribute == CardAttribute.Neutro)
        {
            damage = damage - damage * hasStatusEffect[TypeOfEffect.DamageReduction].value / 100;
        }
        if (damage < 1.0f)
            damage = 1.0f;
        return damage;
    }
    public float GetDamage(CardResult cardUsed, int uid)
    {
        float damage = entityData.CalculateDamage(cardUsed);
        damage = CheckDamage(damage,cardUsed.CardAttribute);
        Debug.Log("New Damage: " + damage);
        if (entityData.TakeDamage(damage))
        {            
            entityController.IsDead(this);
        }
        else
        {
            entityController.GotAttacked(uid, damage); // can be use to return damage if there is such an effect.
        }
        if (cardUsed.newSpecialEffects.Count >= 1)
        {
            foreach (var item in cardUsed.newSpecialEffects)
            {
                if (hasStatusEffect.ContainsKey(item.Key))
                {
                    var effect = hasStatusEffect[item.Key];
                    effect.duration = item.Value.duration;
                    effect.value = item.Value.value;
                    effect.isActive = true;
                    hasStatusEffect[item.Key] = effect;
                }
                else
                {
                    statusEffect effect = new statusEffect();
                    effect.duration = item.Value.duration;
                    effect.value = item.Value.value;
                    effect.isActive = true;
                    listOfEffects.Add(item.Key);
                    hasStatusEffect.Add(item.Key, effect);
                    
                }
            }
        }
        return damage;
    }
    public void ProcessDefenseEffect(TypeOfEffect typeOfEffect, effectResult effect)
    {
        statusEffect specialStatus;
        statusEffect originalValue;
        specialStatus.isActive = true;
        specialStatus.value = effect.value;
        specialStatus.duration = effect.duration;
        switch (typeOfEffect)
        {
            case TypeOfEffect.IncreaseMultiplier:
                break;
            case TypeOfEffect.DirectDamage:
                Debug.LogError("This is a defense card!!");
                break;
            case TypeOfEffect.ExtraArmor:
                if(hasStatusEffect.TryGetValue(typeOfEffect,out originalValue))
                {
                    hasStatusEffect[typeOfEffect] = specialStatus;                    
                }
                else
                {
                    listOfEffects.Add(typeOfEffect);
                    hasStatusEffect.Add(typeOfEffect, specialStatus);
                }
                sound.PlayDefense();
                break;
            case TypeOfEffect.ExtraMagicArmor:
                if (hasStatusEffect.TryGetValue(typeOfEffect, out originalValue))
                {
                    hasStatusEffect[typeOfEffect] = specialStatus;
                }
                else
                {
                    listOfEffects.Add(typeOfEffect);
                    hasStatusEffect.Add(typeOfEffect, specialStatus);
                }
                sound.PlayDefense();
                break;
            case TypeOfEffect.DamageReduction:
                if (hasStatusEffect.TryGetValue(typeOfEffect, out originalValue))
                {
                    hasStatusEffect[typeOfEffect] = specialStatus;
                }
                else
                {
                    listOfEffects.Add(typeOfEffect);
                    hasStatusEffect.Add(typeOfEffect, specialStatus);
                }
                sound.PlayDefense();
                break;
            case TypeOfEffect.ExtraAction:
                break;
            case TypeOfEffect.Healing:
                entityData.Heal(effect.value);
                 sound.PlayLifeUp();
                break;
            case TypeOfEffect.MagicDamageReduction:
                if (hasStatusEffect.TryGetValue(typeOfEffect, out originalValue))
                {
                    hasStatusEffect[typeOfEffect] = specialStatus;
                }
                else
                {
                    listOfEffects.Add(typeOfEffect);
                    hasStatusEffect.Add(typeOfEffect, specialStatus);
                }
                sound.PlayDefense();
                break;
            case TypeOfEffect.PhysicalDamageReduction:
                if (hasStatusEffect.TryGetValue(typeOfEffect, out originalValue))
                {
                    hasStatusEffect[typeOfEffect] = specialStatus;
                }
                else
                {
                    listOfEffects.Add(typeOfEffect);
                    hasStatusEffect.Add(typeOfEffect, specialStatus);
                }
                sound.PlayDefense();
                break;
            case TypeOfEffect.Stun:
                if (hasStatusEffect.TryGetValue(typeOfEffect, out originalValue))
                {
                    hasStatusEffect[typeOfEffect] = specialStatus;
                }
                else
                {
                    listOfEffects.Add(typeOfEffect);
                    hasStatusEffect.Add(typeOfEffect, specialStatus);
                }
                break;
            default:
                break;
        }
    }
    public void IncreaseNormalActions()
    {
        remainingActions++;
    }
}