﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UpAndFade : MonoBehaviour
{
    private Color color;
    private TextMeshProUGUI text;

    private void Awake()
    {
        text = gameObject.GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        if (text.color.a <= 0.0f)
        {
            Destroy(gameObject);
        }
        transform.position = new Vector2(transform.position.x, transform.position.y + 40f * Time.deltaTime);
        color = text.color;
        color.a -= 0.35f * Time.deltaTime;
        text.color = color;
    }

    public void SetText(string str)
    {
        text.text = str;
    }
}
