﻿#pragma warning disable 0649
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public enum TypeOfRequirements { PlayerLevel, CompletedQuest };
[System.Serializable]
public class QuestRequirements 
{
    private int playerLevel;
    [SerializeField] private int playerLevelRequired;
    private PlayableCharacter playableChar;
    //private int testingLevelPlayer = 1;
    [SerializeField] private bool levelCheck;
    [SerializeField] private string completedQuestID;
    [SerializeField] private bool questIDCompleteCheck;
    //[SerializeField] private Stats playerStats;
    public bool GetLevelCheck() => levelCheck;
    public bool GetQuestIDCompleteCheck() => questIDCompleteCheck;

    private QuestManager questManager;

    void Start()
    {
        playerLevel = playableChar.GetLevel();
        //playerLevel = testingLevelPlayer;
        if (playerLevelRequired == 0)
            levelCheck = true;
        if (completedQuestID == null)
            questIDCompleteCheck = true;
	}

    public bool CheckRequirements()     
	{
        if (CheckLevelRequirement() && CheckCompletedQuestID())
            return true;
        return false;
    }

    private bool CheckLevelRequirement()
	{
        if (playerLevelRequired >= playerLevel)
        {
            levelCheck = true;
            return true;
        }
        return false;
	}

    private bool CheckCompletedQuestID ()
	{
        foreach (Quest q in questManager.GetQuests())
		{
            if (q.GetQuestID() == completedQuestID)
			{
                if (q.GetIsCompleted() == true)
				{
                    questIDCompleteCheck = true;
                    return true;
				}
			}
		}
        return false;
	}
}