﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour
{
    public int stock = 1;
    public Item scriptableItem;
    private Item item;
    private Image icon;
    public UIDungeonManager uiDungeonManager;
    private TextMeshProUGUI stockText;

    void Awake()
    {
        uiDungeonManager = GameObject.FindGameObjectWithTag("UIDungeonManager")?.GetComponent<UIDungeonManager>();
        icon = transform.GetChild(0).gameObject.GetComponent<Image>();
        stockText = transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>();
        SetColorAndText();
    }

    private void Start()
    {
        if (scriptableItem != null)
        {
            item = scriptableItem;
            SetItem(item);
        }
        gameObject.GetComponent<ShowTooltip>().SetText("<color=orange>Comprar</color> " + item.GetName() + "\n" + "<color=orange>Precio: </color> " + item.GetValue() + " <color=orange>oro</color>");
    }

    private void SetColorAndText()
    {
        stockText.SetText(stock.ToString());
        if (stock == 0)
        {
            Color nc = new Color(icon.color.r, icon.color.g, icon.color.b, 0.5f);
            icon.color = nc;
        }
        else
        {
            icon.color = new Color(1f, 1f, 1f, 1f);
        }
    }

    public void SetItem(Item i)
    {
        item = i;
        icon.sprite = item.GetIcon();
    }

    public void SetStock(int i)
    {
        stock = i;
        SetColorAndText();
    }

    public int GetStock()
    {
        return stock;
    }

    public void BuyItem()
    {
        if(stock > 0)
        {
            if (uiDungeonManager.BuyItem(item))
            {
                stock--;
                SetStock(stock);
            }
            else
            {
                uiDungeonManager.ShowErrorText("No puedes comprar ese objeto");
            }
        }
        else
        {
            uiDungeonManager.ShowErrorText("No hay stock");
        }
    }
}
