﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Trap", menuName = "Scriptable Object/Create Trap")]
public class TrapData : ScriptableObject
{
    [SerializeField] private float damage;
    [SerializeField] private float chanceToDisable;
    public float GetDamage() => damage;
    public float GetChance() => chanceToDisable;
}
