﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treasure : InteractableItem
{
    [SerializeField] private TreasureData data;
    private List<Item> finalItems;
    private List<int> finalAmmountOfItems;
    private Player player;
    private bool isUsed;
    private AudioController sound;
    private int index;
    
    
    public override void PlayAction()
    {
        if (isUsed) return;
        gameObject.GetComponentInChildren<Animator>().SetTrigger("open");
        if (sound) sound.PlayTreasure();
        if(index!=-99)
            InteractableItems.SetActive(index);
        int i = 0;
        Debug.Log("Me estoy ejecutando");
        
        foreach (var item in finalItems)
        {
            if(!player.GetInventory().AddItem(item, finalAmmountOfItems[i]))
            {
                // do something about not enough space?
            }            
            i++;
        }
        isUsed = true;
    }

    public override void SetData(ScriptableObject data, int index)
    {
        this.data = data as TreasureData;
        this.index = index;
        int i = 0;
        var listChance = this.data.GetListDropChance();
        var listMaxAmmount = this.data.GetListMaxAmmount();
        foreach (var item in this.data.GetItems())
        {
            Debug.Log(item == null);
            var maxChance = listChance[i];
            var maxAmmount = listMaxAmmount[i];
            if(Random.Range(0,100) < maxChance)
            {
                finalItems.Add(item);
                finalAmmountOfItems.Add(Random.Range(1, maxAmmount));
            }
            i++;
        }
    }

    // Start is called before the first frame update
    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("PlayerManager")?.GetComponent<Player>();
        finalItems = new List<Item>();
        finalAmmountOfItems = new List<int>();
        if (data != null)
            SetData(data,-99);
        
        isUsed = false;
    }
    private void Start()
    {
        sound = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
