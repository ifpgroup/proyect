﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayableCharacter : Entity
{
    protected int pointsAvaible;
    protected static int pointsPerLevel = 5;
    protected int handSize;
    protected int deckSize;
    
    protected Equipment weapon;
    protected Equipment armor;
    protected Equipment trinket;
    
    public Equipment GetEquipment(TypeOfEquipment equipment)
    {
        switch (equipment)
        {
            case TypeOfEquipment.Weapon:
                return weapon;
                
            case TypeOfEquipment.Armor:
                return armor;
            case TypeOfEquipment.Trinket:
                return trinket;
            default:
                return null;
        }
    }
    public void ChangeEquipment(Equipment equipment)
    {
        switch (equipment.GetTypeOfEquipment())
        {
            case TypeOfEquipment.Weapon:
                weapon = equipment;
                break;
            case TypeOfEquipment.Armor:
                armor = equipment;
                break;
            case TypeOfEquipment.Trinket:
                trinket = equipment;
                break;
            default:
                break;
        }
    }
    public PlayableCharacter(Stats stats, List<Card> actions, Equipment weapon, Equipment armor, Equipment trinket)
    {
        pointsAvaible = 0;
        baseStats = stats;
        this.weapon = weapon;
        this.trinket = trinket;
        this.armor = armor;
        CalculateAttributes();
        entityActions = new Deck(this, actions, weapon, armor, trinket,deckSize,handSize);
    }
    public PlayableCharacter(List<Card> actions, Equipment weapon, Equipment armor, Equipment trinket)
    {
        pointsAvaible = 0;
        baseStats.constitution = 10;
        baseStats.strength = 10;
        baseStats.dexterity = 10;
        baseStats.intelligence = 10;
        baseStats.vitality = 10;
        baseStats.wisdom = 10;
        this.weapon = weapon;
        this.trinket = trinket;
        this.armor = armor;
        CalculateAttributes();
        entityActions = new Deck(this, actions, weapon, armor, trinket,deckSize,handSize);
    }

    public bool LevelUp()
    {
        level += 1;
        pointsAvaible += pointsPerLevel;
        return true;
    }
    public bool IncreaseStat(StatsNames statToIncrease, int pointsUsed)
    {
        if (pointsUsed > pointsAvaible)
            return false;
        switch (statToIncrease)
        {
            case StatsNames.Wisdom:
                baseStats.wisdom += pointsUsed;                                    
                break;
            case StatsNames.Strength:
                baseStats.strength += pointsUsed;
                break;
            case StatsNames.Intelligence:
                baseStats.intelligence += pointsUsed;
                break;
            case StatsNames.Constitution:
                baseStats.constitution += pointsUsed;
                break;
            case StatsNames.Dexterity:
                baseStats.dexterity += pointsUsed;
                break;
            case StatsNames.Vitality:
                baseStats.vitality += pointsUsed;
                break;
            default:
                return false;
        }
        pointsAvaible -= pointsUsed;
        CalculateAttributes();
        return true;
    }
    protected override void CalculateAttributes()
    {
        Stats moddedStats = weapon.GetStats() + armor.GetStats() + trinket.GetStats();
        EntityAttributes moddedAttr = weapon.GetAttr() + armor.GetAttr() + trinket.GetAttr();
        finalAttributes.hp = EntitiesPrefs.baseHP + 20+(baseStats.vitality +moddedStats.vitality)* EntitiesPrefs.baseConHPMultEnemy[GetModifer(baseStats.vitality)] + moddedAttr.hp;
        finalAttributes.mana = EntitiesPrefs.baseMana + (baseStats.wisdom + moddedStats.wisdom) * EntitiesPrefs.baseManEnergyMultEnemy[GetModifer(baseStats.wisdom)] + moddedAttr.mana;
        finalAttributes.energy = EntitiesPrefs.baseEnergy + 10 * EntitiesPrefs.baseManEnergyMultEnemy[GetModifer(baseStats.constitution)] + moddedAttr.energy;

        finalAttributes.energyRecoverPerTurn = EntitiesPrefs.baseManaEnergyRegen + (baseStats.constitution + moddedStats.constitution) * EntitiesPrefs.baseManaEnergyRegenMultEnemy[GetModifer(baseStats.constitution)] + moddedAttr.energyRecoverPerTurn;
        finalAttributes.manaRecoverPerTurn = EntitiesPrefs.baseManaEnergyRegen + (baseStats.wisdom + moddedStats.wisdom) * EntitiesPrefs.baseManaEnergyRegenMultEnemy[GetModifer(baseStats.wisdom)] + moddedAttr.manaRecoverPerTurn;

        finalAttributes.armor = moddedAttr.armor;//2 for now, later acording to armor
        finalAttributes.mArmor = moddedAttr.mArmor;//2 for now, later according to armor

        finalAttributes.intMult = baseStats.intelligence / 10.0f + moddedAttr.intMult + 1.0f;
        finalAttributes.strMult = baseStats.strength / 10.0f + moddedAttr.strMult + 1.0f;
        handSize = 3 +GetModifer(baseStats.dexterity);
        deckSize = 10 + 2 * GetModifer(baseStats.constitution);
        finalAttributes.maxHp = finalAttributes.hp;
        finalAttributes.maxMana = finalAttributes.mana;
        finalAttributes.maxEnergy = finalAttributes.energy;
    }
    private int GetModifer(int attr)
    {
        int tempValue = attr - 10;
        if (tempValue > 0)
        {
            tempValue = tempValue / 2;
            if (tempValue > 5)
                tempValue = 5;
        }
        else
        {
            tempValue = 0;
        }

        return tempValue;
    }

    public override void Dead()
    {
        isDead = true;
        
    }

    public override void DrawCard()
    {
        throw new System.NotImplementedException();
    }

    public override bool CreateDeck()
    {
        ///a default deck should be made.
        throw new System.NotImplementedException();
    }

    public override float CalculateDamage(CardResult cardUsed)
    {
        float damage;
        if (cardUsed.CardAttribute == CardAttribute.Intelligence)
        {
            damage = cardUsed.newValue.value * 10 / (10 + finalAttributes.mArmor);
        }
        else
        {
            damage = cardUsed.newValue.value* 10 / (10 + finalAttributes.armor);
        }
        return damage;
    }
    public Stats GetStats() => baseStats;

    /// Missing methods, to equip weapons,armor,trinkets.
}
