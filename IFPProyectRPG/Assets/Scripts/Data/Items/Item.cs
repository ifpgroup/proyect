﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Scriptable Object/Create Items")]
public class Item : ScriptableObject
{
    [SerializeField] protected int GUID;
    [SerializeField] protected string Name;
    [SerializeField] 
    [TextArea(3,6)]
    protected string description;
    [SerializeField] protected bool canBeSold;
    [SerializeField] protected bool canBeEquip;
    [SerializeField] protected int Value;
    [SerializeField] protected Sprite icon;

    public int GetGUID() => GUID;
    public int GetValue() => Value;
    public Sprite GetIcon() => icon;
    public bool CanBeSold() => canBeSold;
    public bool CanBeEquip() => canBeEquip;
    public string GetName() => Name;
    public string GetDescription() => description;
}