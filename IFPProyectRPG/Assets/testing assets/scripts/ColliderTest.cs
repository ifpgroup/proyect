﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderTest : MonoBehaviour
{
        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("InteractuableItem"))
            {
                Debug.Log("Interactuando con un objeto");
            }
        }
    
}