﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipSlot : MonoBehaviour
{
    public bool isArmorSlot = false;
    public bool isWeaponSlot = false;
    public bool isTrinketSlot = false;
    private bool hasItem = false;
    private Item equippedItem;

    public Item GetEquippedItem()
    {
        return equippedItem;
    }

    public void SetEquippedItem(Item item)
    {
        equippedItem = item;
    }

    public bool HasItemEquipped()
    {
        return hasItem;
    }

    public void SetHasItem(bool b)
    {
        hasItem = b;
    }
}
