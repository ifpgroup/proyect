﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDHolder : MonoBehaviour
{
    public Component targetColliderHelper;
    public GameObject stunEffect;
    private Entity data;
    private EntityCombat dataCombat;
    [SerializeField] private int UID;
    public int GetUID() => UID;
    public void SetUID(int id) => UID = id;
    public Entity GetData() => data;
    public EntityCombat GetDataCombat() => dataCombat;
    public void SetData(Entity dat) => data = dat;
    public void SetDataCombat(EntityCombat dat) => dataCombat = dat;

    public void ShowStunEffect(bool b)
    {
        stunEffect.SetActive(b);
    }
}
