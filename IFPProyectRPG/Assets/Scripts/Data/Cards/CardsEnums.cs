﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Create the enums for the cards here
public enum CardType
{
        Offensive = 1,
        Deffensive = 2,
        Support = 3,
    }

public enum CardAttribute
{
        Strength = 1,
        Intelligence = 2,
        Neutro = 3,
    }

 public enum TypeOfEffect
    {
        IncreaseMultiplier = 1,
        DirectDamage = 2,
        ExtraArmor = 3,
        ExtraMagicArmor = 4,
        DamageReduction = 5,
        ExtraAction = 6,
        Healing = 7,
        MagicDamageReduction = 8,
        PhysicalDamageReduction = 9,
        RegenMana = 10,
        RegenEnergy =11,
        Stun = 12,
        TempHP = 13,
}
