﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIEnemy : MonoBehaviour, IController
{
    [SerializeField] private CombatManager combatManager;
    private IUIManager UICombatManager;
    private List<EntityCombat> entitiesOwned;
    private Dictionary<int , float> aggroList;
    private int indexOfEntityTurn;
    private EntityCombat currentEntityTurn => entitiesOwned[indexOfEntityTurn];
    private bool isMyTurn;
    private IEnumerator aiUpdate;   
    private List<Card> hand;
    private bool triedToAttack;
    private bool triedToDefend;
    private bool cardBeingUsed = false;
    private bool goToStartState;
    private bool goToAttackState;
    private bool goToDefendState;
    private bool goToSupportState;
    private bool goToCheckState;

    
    public void Start()
    {
        UICombatManager = GameObject.FindGameObjectWithTag("UIManager").GetComponent<IUIManager>();
        entitiesOwned = new List<EntityCombat>();
        aggroList = new Dictionary<int, float>();
        indexOfEntityTurn = 0;
        goToAttackState = false;
        goToCheckState = false;
        goToDefendState = false;
        goToStartState = false;
        goToSupportState = false;
    }
    public void AddCreature(EntityCombat entity)
    {
        if (entitiesOwned == null) entitiesOwned = new List<EntityCombat>();
        entitiesOwned.Add(entity);
        //UICombatManager.GetEntityData(entity.GetAttr(), entity.GetActionsRemaining(), !entity.GetSupportActionUsed(), entity.GetUID());
        Debug.Log(entitiesOwned.Count);
    }
    public void AddPlayerToAggroList(EntityCombat entity)
    {
        if (aggroList == null) aggroList = new Dictionary<int, float>();
        aggroList.Add(entity.GetUID(), 0.0f);
    }
    public void StartCombat()
    {
        indexOfEntityTurn = 0;
    }
    public IEnumerator DecideDefensiveOrAttack()
    {
        if(isMyTurn== false)
        {
            Debug.LogError("This should not have been executed");
            StopAi();
        }
        triedToDefend = false;
        triedToAttack = false;
        Debug.Log("Courutine executed");
        float chanceDefensiveCard = -1.0f;
        bool hasOffenseCards = false;
        bool hasDefenseCards = false;
        var attr = currentEntityTurn.GetAttr();
        foreach (var item in hand)
        {
            if (item.GetCardType()==CardType.Deffensive)
                hasDefenseCards = true;
            if (item.GetCardType()==CardType.Offensive)
                hasOffenseCards = true;

        }
        if (!hasOffenseCards)
            chanceDefensiveCard += 100.0f;
        if (hasDefenseCards)
        {
            chanceDefensiveCard = 10.0f;
            if (attr.hp <= attr.maxHp && attr.hp > attr.maxHp / 4)
            {
                chanceDefensiveCard += 10.0f;
            }                
            else if (attr.hp <= attr.maxHp / 4)
            {
                chanceDefensiveCard += 30.0f;             
            }
        }            
        
        yield return new WaitForFixedUpdate();
        int rand = UnityEngine.Random.Range(0, 100);

        if (rand <= chanceDefensiveCard)
            goToDefendState = true;
        else
            goToAttackState = true;        
    }
    public IEnumerator PlayOffensiveCard()
    {
        
        if (triedToAttack && triedToDefend)
            StopAi();
        List<Card> cardsAvaible = new List<Card>();
        List<int> indexes = new List<int>();
        bool hasAoeAttack;
        List<int> aoeAttackIndex = new List<int>();
        int i = 0;
        foreach (var item in hand)
        {
            if (item.GetCardType() == CardType.Offensive)
            {
                cardsAvaible.Add(item);
                indexes.Add(i);
                if(!item.GetIsSingleTarget())
                {
                    hasAoeAttack = true;
                    aoeAttackIndex.Add(i);
                }
            }
            i++;
                
        }
        if (indexes.Count<=0)
        {
            triedToAttack = true;            
        
            if (triedToAttack && triedToDefend)
                StopAi();
            else
                goToDefendState = true;         
            
        }
        else
        {
            var rand = UnityEngine.Random.Range(0, cardsAvaible.Count); // first we try with a random chance for any card, maybe later on, we can do a more complex check
            Debug.Log("Card count: " + cardsAvaible.Count);
            Debug.Log("Index count: " + indexes.Count);
            Debug.Log("Rand: " + rand);
            if (rand >= indexes.Count)
                rand = indexes.Count - 1;
            yield return new WaitForFixedUpdate();
            /// need to add a check for the effects for when there is more than one target.
            if (aoeAttackIndex.Contains(indexes[rand]))
            {
                List<int> targetsUIDS = new List<int>();
                for(int targetNum=0; targetNum < cardsAvaible[rand].GetNumTargets()+ 1; targetNum++)
                {
                    int uidTarget = -1;
                    float aggro = -1.0f;
                    // missing being able to select more than one target for cards with the singleTarget bool false;
                    foreach (var item in aggroList)
                    {
                        float randAggro = UnityEngine.Random.Range(0, 3);
                        if (aggro < (item.Value + randAggro) && !targetsUIDS.Contains(item.Key))
                        {
                            uidTarget = item.Key;
                            aggro = (item.Value + randAggro);                                                        
                        }
                    }
                    targetsUIDS.Add(uidTarget);
                    Debug.Log("Target " + uidTarget);
                }

                UICombatManager.PlayCard(indexes[rand], cardsAvaible[rand], targetsUIDS);
                currentEntityTurn.UseCardNormalAction();
                PlayCard(indexes[rand], targetsUIDS.ToArray());
            }
            else
            {

                int uidTarget = -1;
                float aggro = -1.0f;
                // missing being able to select more than one target for cards with the singleTarget bool false;
                foreach (var item in aggroList)
                {
                    float randAggro = UnityEngine.Random.Range(0, 3);
                    if (aggro < (item.Value + randAggro))
                    {
                        uidTarget = item.Key;
                        aggro = (item.Value + randAggro);
                    }
                }
                Debug.Log("Target " + uidTarget);

                UICombatManager.PlayCard(indexes[rand], cardsAvaible[rand], uidTarget);
                currentEntityTurn.UseCardNormalAction();
                PlayCard(indexes[rand], uidTarget);
            }
            goToCheckState = true;
            
        }
        
    }
    public IEnumerator PlayDefensiveCard()
    {
        
        if (triedToAttack && triedToDefend)
            StopAi();
        List<Card> cardsAvaible = new List<Card>();
        List<int> indexes = new List<int>();
        int i = 0;
        foreach (var item in hand)
        {
            if (item.GetCardType()==CardType.Deffensive)
            {
                
                cardsAvaible.Add(item);
                indexes.Add(i);
            }
            i++;

        }
        if (indexes.Count <= 0)
        {
            triedToDefend = true;
        
            if (triedToAttack && triedToDefend)
                StopAi();
            else
                goToAttackState = true;              
            
        }
        else
        {
            bool costAccepted = false;
            int rand;
            do {
                rand = UnityEngine.Random.Range(0, cardsAvaible.Count); // first we try with a random chance for any card, maybe later on, we can do a more complex check
                if(cardsAvaible[rand].GetCostEnergy()> 0) costAccepted = currentEntityTurn.CheckCost((int)cardsAvaible[rand].GetCostEnergy(), false);
                if(cardsAvaible[rand].GetCostMana() > 0) costAccepted = currentEntityTurn.CheckCost((int)cardsAvaible[rand].GetCostMana(), true);
                if (!costAccepted)
                {
                    cardsAvaible.RemoveAt(rand);
                    indexes.RemoveAt(rand);
                }
                if (cardsAvaible.Count == 0) break;
            } while (!costAccepted);
            if (cardsAvaible.Count == 0)
            {
                triedToDefend = true;
        
                if (triedToAttack && triedToDefend)
                    StopAi();
                else
                    goToAttackState = true;
            }
            else
            {
                if (rand >= indexes.Count)
                    rand = indexes.Count - 1;
                yield return new WaitForFixedUpdate();
                currentEntityTurn.UseCardNormalAction();
                UICombatManager.PlayCard(indexes[rand], cardsAvaible[rand], currentEntityTurn.GetUID());
                PlayCard(indexes[rand], currentEntityTurn.GetUID()); //for now only targets himself with defensive actions
                goToCheckState = true;
            }
            
        }
        
    }

    private void StopAi()
    {
        StopAllCoroutines();
        Debug.Log("Cementery: " + currentEntityTurn.GetCementery().Count);
        Debug.Log("Hand: " + currentEntityTurn.GetHand().Count);
        Debug.Log("Deck: " + currentEntityTurn.GetRemainingCards().Count);
        //  EndTurn();
    }

    public IEnumerator PlaySupportCard()
    {
        
        List<Card> cardsAvaible = new List<Card>();
        List<int> indexes = new List<int>();
        int i = 0;
        foreach (var item in hand)
        {
            if (item.GetCardType() == CardType.Support)
            {
                cardsAvaible.Add(item);
                indexes.Add(i);
            }
            i++;

        }
        if (indexes.Count > 0)
        {
            var rand = UnityEngine.Random.Range(0, cardsAvaible.Count); // first we try with a random chance for any card, maybe later on, we can do a more complex check
            yield return new WaitForFixedUpdate();
            currentEntityTurn.UseSupportCard();
            UICombatManager.PlayCard(indexes[rand], cardsAvaible[rand], currentEntityTurn.GetUID());
            PlayCard(indexes[rand], currentEntityTurn.GetUID()); //for now only targets himself with support actions
        

        }
        else
            EndTurn();
    }
    public IEnumerator CanIStillDoSomething() // we check support card, and extra actions here! or end the turn;
    {
        float time = 0.0f;
        while (cardBeingUsed)
        {
            time += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
            if (time > 2.0f)// in case something happens once 2s of card animation have pass, we automatically unblock the ai.
                cardBeingUsed = false;
        }
        aiUpdate = null;
        if (currentEntityTurn.GetActionsRemaining() > 0)
            goToStartState = true;
        else if (!currentEntityTurn.GetSupportActionUsed())
            goToSupportState = true;
        else
            EndTurn();
    }
    public void AssignEntities(List<EntityCombat> entitiesControlled)
    {
        entitiesOwned = entitiesControlled;
        indexOfEntityTurn = 0;
    }


    public void DrawCard()
    {
        currentEntityTurn.DrawCard();
        hand = currentEntityTurn.GetHand();
        Debug.Log("Current Hand");
        foreach (var item in hand)
        {
            Debug.Log("Card on Hand: " + item.name + " IsOffensive: " + item.GetEffects.isOffensive + " IsSupport:" + item.GetEffects.isSupport);
        }
    }

    public void EndTurn()
    {
        indexOfEntityTurn++;
        if (indexOfEntityTurn >= entitiesOwned.Count)
            indexOfEntityTurn = 0;
        isMyTurn = false;
        goToAttackState = false;
        goToCheckState = false;
        goToDefendState = false;
        goToStartState = false;
        goToSupportState = false;
        UICombatManager.EndTurn();
        StopAllCoroutines();
        combatManager.EndCurrentEntityTurn();
    }

    public void IsDead(EntityCombat entity)
    {
        combatManager.EntityKilled(entity);           
        entitiesOwned.Remove(entity);
        if (entitiesOwned.Count == 0)
            combatManager.EndCombat(true);//all entities of the ai are dead.
        if (indexOfEntityTurn >= entitiesOwned.Count)
            indexOfEntityTurn = 0;
    }

    public void IsMyTurn()
    {
        isMyTurn = true;        
        DrawCard();       
        UICombatManager.StartTurn(hand, true, currentEntityTurn.GetCementery().Count);
        UICombatManager.GetEntityData(currentEntityTurn.GetAttr(), currentEntityTurn.GetActionsRemaining(), !currentEntityTurn.GetSupportActionUsed(), currentEntityTurn.GetUID());
        if (currentEntityTurn.GetActionsRemaining() == 0 && currentEntityTurn.GetSupportActionUsed())
            Invoke("EndTurn", 3.0f);
        else
            goToStartState = true;        
    }

    public void PlayCard(int index, int[] targetsUID)
    {
        cardBeingUsed = true;        
        CardResult result = currentEntityTurn.PlayCard(index);
        foreach (var targetUID in targetsUID)
        {
            if (result.CardType == CardType.Offensive)
            {
                float finalDamage = combatManager.AttackEntity(result, targetUID, currentEntityTurn);
                Debug.Log("Final Damage: " + finalDamage);
                UICombatManager.CardResultsAttack(finalDamage);
            }
            else
            {
                if (result.CardType == CardType.Deffensive)
                {
                    currentEntityTurn.ProcessDefenseEffect(result.TypeOfEffect, result.newValue);
                    if (result.newSpecialEffects != null)
                    {
                        foreach (var item in result.newSpecialEffects)
                        {
                            currentEntityTurn.ProcessDefenseEffect(item.Key, item.Value);
                        }
                    }

                }
                else
                {
                    /// TODO proccess support Card
                }
                UICombatManager.CardResult(result, currentEntityTurn.GetAttr(), false);
            }
        }
    }

    public void PlayCard(int index, int targetUID)
    {
        cardBeingUsed = true;
        
        CardResult result = currentEntityTurn.PlayCard(index);
        if(result.CardType == CardType.Offensive)
        {
            float finalDamage = combatManager.AttackEntity(result, targetUID,currentEntityTurn);
            Debug.Log("Final Damage: " + finalDamage);
            UICombatManager.CardResultsAttack(finalDamage);
        }
        else 
        {
            UICombatManager.CardResult(result, currentEntityTurn.GetAttr(), false);
            if(result.CardType == CardType.Deffensive)
            {
                currentEntityTurn.ProcessDefenseEffect(result.TypeOfEffect, result.newValue);
                if(result.newSpecialEffects != null)
                {
                    foreach (var item in result.newSpecialEffects)
                    {
                        currentEntityTurn.ProcessDefenseEffect(item.Key, item.Value);
                    }
                }
                
            }
            else
            {
                /// TODO proccess support Card
            }
        }
        Debug.Log(result.value);
    }

    public void GotAttacked(int uid, float damage)
    {
        float value;
        if (aggroList.TryGetValue(uid, out value))
        {
            value += damage * 0.2f;
            aggroList[uid] = value;
        }
        else
        {
            aggroList.Add(uid, damage * 0.2f);
        }
    }

    public void CardAnimationOver()
    {
        cardBeingUsed = false;
    }
    public void FixedUpdate()
    {

        if (goToStartState)
        {
            goToStartState = false;
            StopAllCoroutines();
            aiUpdate = DecideDefensiveOrAttack();
            StartCoroutine(aiUpdate);
        } else if (goToAttackState)
        {
            goToAttackState = false;
            StopAllCoroutines();
            aiUpdate = PlayOffensiveCard();
            StartCoroutine(aiUpdate);
        } else if (goToDefendState)
        {
            goToDefendState = false;
            StopAllCoroutines();
            aiUpdate = PlayDefensiveCard();
            StartCoroutine(aiUpdate);
        } else if (goToSupportState)
        {
            goToSupportState = false;
            StopAllCoroutines();
            aiUpdate = PlaySupportCard();
            StartCoroutine(aiUpdate);
        } else if(goToCheckState)
        {
            goToCheckState = false;
            StopAllCoroutines();
            aiUpdate = CanIStillDoSomething();
            StartCoroutine(aiUpdate);
        }
    }
}

