﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Card",menuName = "Scriptable Object/Create Card")]
public class Card : ScriptableObject, ISerializationCallbackReceiver
{
    [SerializeField] private int GUID;
    [SerializeField] private CardRarity rarity;
    [SerializeField] private Sprite cardImage;
    [SerializeField] private string Name;
    [SerializeField] private string description;
    [SerializeField] private CardType type;
    [SerializeField] private CardEffect effect;
    [SerializeField] private List<Effect> listOfEffects;
    [SerializeField] private bool isSingleTarget;
    [SerializeField] private int numTargets;
    [SerializeField] private float costMana;
    [SerializeField] private float costEnergy;
    [SerializeField] private float costHP;
    //[SerializeField] private VFX specialEffect;
    [SerializeField] private CardAttribute attribute;
    [SerializeField] private Stats requirements;
    [SerializeField] private bool Unlocked;

    public CardRarity GetRarity() => rarity;
    public CardAttribute GetAttribute() => attribute;
    public Sprite GetImage() => cardImage;
    public bool IsUnlocked => Unlocked;
    public CardType GetCardType() => type;
    public bool GetIsSingleTarget() => isSingleTarget;
    public int GetNumTargets() => numTargets;
    public CardEffect GetEffects => effect;
    public float GetCostMana() => costMana;
    public float GetCostEnergy() => costEnergy;
    public string GetName() => Name;
    public string GetDescription() => description;
    public bool UnlockCard(Stats playerStats)
    {
        Debug.Log(playerStats >= requirements);
        return playerStats >= requirements;
    }
    public CardResult PlayCard(EntityAttributes attr, bool hasExtraEffect = false, float value = 0)
    {
        CardResult result = new CardResult();
        Debug.Log("Se ha jugado la carta");
        if (effect.isOffensive) result = PlayAttackCard(attr, hasExtraEffect, value);
        else if(effect.isSupport) result = PlaySupportCard(attr, hasExtraEffect, value);
        else result = PlayDefenseCard(attr, hasExtraEffect, value); 
        return result;
    }

    private CardResult PlayAttackCard(EntityAttributes attr, bool hasExtraEffect= false, float value = 0)
    {
        CardResult result = new CardResult(); 
        Debug.Log("Se ha jugado la carta");
        float mult = 0.0f;
        result.TypeOfEffect = TypeOfEffect.DirectDamage;
        result.CardAttribute = attribute;
        result.CardType = type;
        result.specialEffects = new Dictionary<TypeOfEffect, float>();
        result.newValue = new effectResult();
        result.newSpecialEffects = new Dictionary<TypeOfEffect, effectResult>();
        if (attribute == CardAttribute.Intelligence)
        {
            result.cost = effect.costMana;
            mult = attr.intMult;
        }
        else
        {
            result.cost = effect.costEnergy;
            mult = attr.strMult;
        }
            
        Debug.Log(mult);
        // missing the occasion with both
        
        if (effect.isUsingDice)
        {
            int rand = Random.Range(0, (int)effect.damageRoll) + 1;
            Debug.Log(rand);
            rand *= effect.numDices;
            float resultDamage = rand* effect.mult * mult + effect.fixedValue;
            result.value = resultDamage;
        }
        else
        {
            result.value = effect.fixedValue * effect.mult * mult;
            
        }
        if (effect.isAddingSpecialEffects)
        {
            //DO something in this cases
        }
        result.newValue = listOfEffects[0].Apply();
        result.newValue.value *= mult;
        Debug.Log("New Damage Value: " + result.newValue.value);
        result.newCost = new cardCost();
        result.newCost.costEnergy = costEnergy;
        result.newCost.costMana = costMana;
        result.newCost.costHP = costHP;
        if (listOfEffects.Count > 1)
        {
            for (int i =1; i < listOfEffects.Count; i++)
            {
                result.newSpecialEffects.Add(listOfEffects[i].GetTypeOfEffect(), listOfEffects[i].Apply());
            }
        }
        return result;
    }
    private CardResult PlayDefenseCard(EntityAttributes attr, bool hasExtraEffect = false, float value = 0)
    {
        CardResult result = new CardResult(); 
        Debug.Log("Se ha jugado la carta");
        result.CardAttribute = attribute;
        result.CardType = type;
        result.value = -1.0f;
        result.specialEffects = new Dictionary<TypeOfEffect, float>();
        result.newSpecialEffects = new Dictionary<TypeOfEffect, effectResult>();
        if (effect.hpHeal > 0.0f)
        {
            /// Healing card
            result.TypeOfEffect = TypeOfEffect.Healing;
            if (effect.isUsingDice)
            {
                int rand =(int) Random.Range(0, effect.hpHeal) + 1;
                result.value = rand * attr.intMult * effect.mult+effect.fixedValue;
            }
            else
            {
                result.value = effect.hpHeal * attr.intMult*effect.mult;
            }
            
        }
        if (effect.armorGiven >0.0f)
        {
            float valueArmor = CalculateEffectValue(effect.isUsingDice, effect.armorGiven, effect.fixedValue);
            valueArmor *= attr.strMult;
            if (result.value != -1.0f)
            {
                // there is already something on value, so this is an extra defensive effect beyond the main
                if (result.specialEffects == null) result.specialEffects = new Dictionary<TypeOfEffect, float>();                                
                result.specialEffects.Add(TypeOfEffect.ExtraArmor, valueArmor);
            }
            else
            {
                result.value = valueArmor;
                result.TypeOfEffect = TypeOfEffect.ExtraArmor;
            }
        }
        if(effect.magicArmorGiven > 0.0f)
        {
            float valueArmor = CalculateEffectValue(effect.isUsingDice, effect.magicArmorGiven, effect.fixedValue);
            valueArmor *= attr.intMult;
            if (result.value != -1.0f)
            {
                // there is already something on value, so this is an extra defensive effect beyond the main
                if (result.specialEffects == null) result.specialEffects = new Dictionary<TypeOfEffect, float>();
                result.specialEffects.Add(TypeOfEffect.ExtraMagicArmor, valueArmor);
            }
            else
            {
                result.value = valueArmor;
                result.TypeOfEffect = TypeOfEffect.ExtraMagicArmor;
            }
        }
        if (effect.damageReduction.Count > 0)
        {
            if (result.value == -1.0f)
                result.TypeOfEffect = TypeOfEffect.DamageReduction;
               // there is already something on value, so this is an extra defensive effect beyond the main
            if (result.specialEffects == null) result.specialEffects = new Dictionary<TypeOfEffect, float>();
            foreach (var item in effect.damageReduction)
            {
                if(item.Key == CardAttribute.Intelligence)
                    result.specialEffects.Add(TypeOfEffect.MagicDamageReduction, item.Value);
                else if (item.Key == CardAttribute.Strength)
                    result.specialEffects.Add(TypeOfEffect.PhysicalDamageReduction, item.Value);
                else if (item.Key == CardAttribute.Neutro)
                    result.specialEffects.Add(TypeOfEffect.DamageReduction, item.Value);
            }            
        }
        float mult = 0.0f;
        if (attribute == CardAttribute.Intelligence)
        {
            result.cost = effect.costMana;
            mult = attr.intMult;
        }
        else
        {
            result.cost = effect.costEnergy;
            mult = attr.strMult;
        }
        result.newValue = listOfEffects[0].Apply();
        result.newValue.value *= mult;
        result.newCost = new cardCost();
        result.newCost.costEnergy = costEnergy;
        result.newCost.costMana = costMana;
        result.newCost.costHP = costHP;
        if (listOfEffects.Count > 1)
        {
            for (int i = 1; i < listOfEffects.Count; i++)
            {
                result.newSpecialEffects.Add(listOfEffects[i].GetTypeOfEffect(), listOfEffects[i].Apply());
            }
        }
        return result;
    }
    public float CalculateEffectValue( bool isUsingDice, float value, float fixeValue = 0.0f)
    {
        float result = 0.0f;
        if (isUsingDice)
        {
            int rand = (int)Random.Range(0, value) + 1;
            result = rand * this.effect.mult+fixeValue;
        }
        else
        {
            result = value * this.effect.mult + fixeValue;
        }
        return result;
    }
    private CardResult PlaySupportCard(EntityAttributes attr, bool hasExtraEffect = false, float value = 0)
    {
        CardResult result = new CardResult();
        result.specialEffects = new Dictionary<TypeOfEffect, float>();
        Debug.Log("Se ha jugado la carta");
        // only extra action is accepted for now
        result.CardAttribute = attribute;
        result.CardType = type;
        if (effect.increaseNumberActions)
        {
            result.TypeOfEffect = TypeOfEffect.ExtraAction;
            result.value = 1 ;
        } else
        {
            if(effect.hpHeal > 0)
            {
                result.TypeOfEffect = TypeOfEffect.Healing;
                if (effect.isUsingDice)
                {
                    float heal = Random.Range(1, effect.hpHeal + 1) * effect.numDices;
                    heal += effect.fixedValue;
                    result.value = heal;
                }
                else
                {
                    result.value = effect.fixedValue + effect.hpHeal; // one of those two should be 0 in this case
                }
            }
            else if (hasExtraEffect)
            {
                result.TypeOfEffect = effect.keyEffects[0];
                if (effect.isUsingDice)
                {

                    float valueOfEffect = Random.Range(1, effect.valueExtraEffect[0] + 1) * effect.numDices;
                    valueOfEffect += effect.fixedValue;
                    result.value = valueOfEffect;
                } else
                {
                    result.value = effect.valueExtraEffect[0];
                }
            }
            Debug.Log(result.TypeOfEffect); 
            result.newValue = listOfEffects[0].Apply();
            result.newCost = new cardCost();
            result.newCost.costEnergy = costEnergy;
            result.newCost.costMana = costMana;
            result.newCost.costHP = costHP;
            if (listOfEffects.Count > 1)
            {
                for (int i = 1; i < listOfEffects.Count; i++)
                {
                    result.newSpecialEffects.Add(listOfEffects[i].GetTypeOfEffect(), listOfEffects[i].Apply());
                }
            }
        }
        return result;
    }

    public void OnBeforeSerialize()
    {        
    }

    public void OnAfterDeserialize()
    {
        effect.effects = new Dictionary<TypeOfEffect, float>();
        effect.damageReduction = new Dictionary<CardAttribute, float>();
        effect.extraEffect = new Dictionary<CardAttribute, float>();
        int i = 0;
       
        if(effect.keyEffects.Count >0)
        {
            foreach (var item in effect.keyEffects)
            {
                if (effect.valueEffects.Count > i)
                    effect.effects.Add(item, effect.valueEffects[i]);
                else
                    break;
                i++;
            }
        }
        if (effect.keyDamageReduction.Count > 0)
        {
            i = 0;
            foreach (var item in effect.keyDamageReduction)
            {
                if (effect.valueDamageReduction.Count > i)
                    effect.damageReduction.Add(item, effect.valueEffects[i]);
                else
                    break;
                i++;
            }
        }
        if (effect.keyExtraEffect.Count > 0)
        {
            i = 0;
            foreach (var item in effect.keyExtraEffect)
            {
                if (effect.valueExtraEffect.Count > i)
                    effect.extraEffect.Add(item, effect.valueEffects[i]);
                else
                    break;
                i++;
            }
        }
    }
}
