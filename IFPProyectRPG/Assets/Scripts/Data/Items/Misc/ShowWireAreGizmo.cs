﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowWireAreGizmo : MonoBehaviour
{
    private void OnDrawGizmos()
    {
        RectTransform rt = (RectTransform)gameObject.transform;

        float width = rt.rect.width;
        float height = rt.rect.height;

        Gizmos.color = new Color(170f/255f, 255f/255f, 120f/255f, 1.0f);
        Gizmos.DrawWireCube(gameObject.transform.position, new Vector2(width, height));
    }
}
