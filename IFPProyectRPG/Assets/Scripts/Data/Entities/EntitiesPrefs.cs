﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EntitiesPrefs
{
    public static int baseHP = 10;
    public static int[] baseConHPMult = { 1, 2, 2, 3, 4, 5 }; // max HP = 5*20 + 10 = 110;
    public static int[] baseConHPMultEnemy = { 1, 2, 3, 4, 5, 6 }; // max HP = 6*20 + 10 = 130;
    public static int baseMana = 50;
    public static int baseEnergy = 100;
    public static int[] baseManEnergyMult = {5, 7, 10, 15, 20, 25}; // max Mana/Energy 25*20 + 50 = 550
    public static int[] baseManEnergyMultEnemy = { 5, 10, 15, 20, 25, 30 }; // max Mana/Energy 30*20 + 50 = 650
    public static int baseManaEnergyRegen = 5;
    public static int[] baseManaEnergyRegenMult = { 1, 1, 2, 2, 3, 4 }; // max Mana/Energy regen = 10 + 20*4 = 90;
    public static int[] baseManaEnergyRegenMultEnemy = { 1, 2, 3, 3, 4, 5 }; // max Mana/Energy regen = 10+20*5 = 110;
    public static int baseNumActions = 1;
    public static int baseSupportActions = 1;
    public static int baseArmor = 0;
    public static int baseMArmor = 0;
    public static float baseArmorMultEnemy = 0.25f; // maxArmor 0 + 20*0.5 = 10;
    public static float baseMArmorMultEnemy = 0.25f; // maxMargmor 0+20*0.5 = 10;
}

