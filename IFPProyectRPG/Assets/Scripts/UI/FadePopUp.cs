﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FadePopUp : MonoBehaviour
{
    private Color color;
    public TextMeshProUGUI text;
    public Image image;
    private Color imageColor;

    private void Start()
    {
        imageColor = image.color;
    }

    void Update()
    {
        if (text.color.a <= 0.0f)
        {
            Destroy(gameObject);
        }
        color = text.color;
        color.a -= 0.35f * Time.deltaTime;
        text.color = color;
        imageColor = image.color;
        imageColor.a -= 0.35f * Time.deltaTime;
        image.color = imageColor;
    }

    public void SetText(string str)
    {
        text.text = str;
    }
}
