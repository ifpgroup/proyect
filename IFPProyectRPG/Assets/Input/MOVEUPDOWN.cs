﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class MOVEUPDOWN : MonoBehaviour
{
    public static float movement;
    bool[] keysPressed;
    public GameObject target;
    public Animator animator;
    public event UnityAction<float> onMove;

    // Start is called before the first frame update
    void Start()
    {
        movement = 0;
        keysPressed = new bool[4];
        for (int i = 0; i < keysPressed.Length; i++)
        {
            keysPressed[i] = false;
        }
    }
    public void SetTarget(GameObject character) => target = character;
    public void SetAnimator(Animator animController) => animator = animController;
    public void Move(float dist)
    {
        onMove?.Invoke(dist);
    }
    // Update is called once per frame
    void Update()
    {
        if (target == null) return;
        float dist = 0.0f;
        if (MOVELEFTRIGHT.movement.Equals(0))
        {
            if (MOVEUPDOWN.movement.Equals(1))
            {
             
                animator.SetFloat("USpeed", Mathf.Abs(movement));
                dist = movement * 3 * Time.deltaTime;
                target.transform.Translate(0.0f, dist, 0.0f);
                
            } 
            else
            {
            
                animator.SetFloat("DSpeed", Mathf.Abs(movement));
                dist = movement * 3 * Time.deltaTime;
                target.transform.Translate(0.0f, dist, 0.0f);
            }
        }
        Move(dist);
    }


    public void MoveUPDown(InputAction.CallbackContext ctx)
    {
        if (ctx.performed)
        {
            movement = ctx.ReadValue<float>();

        }
        else if (ctx.canceled)
        {
            movement = 0;
        }
    }
}
