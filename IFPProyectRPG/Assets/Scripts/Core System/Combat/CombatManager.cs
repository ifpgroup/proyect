﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatManager : MonoBehaviour, ICombatManager
{
    private List<EntityCombat> entities;
    private int currentTurnEntityIndex;
    private Player playerManager;
    [SerializeField] private List<Transform> spawnLocationsEnemy;
    [SerializeField] private List<Transform> spawnLocationsPlayer;
    [SerializeField] private AIEnemy ai;
    [SerializeField] private PlayerCombatController player;
    [SerializeField] private EntityFactory factory;
    [SerializeField] private AudioController sound;
    private Dictionary<int,GameObject> prefabList;

    private bool isCombatFinish;
    private int numEnemiesDead;
    private static int turnNumber = 0;
    private Reward loot;
    public static bool isFixedEncounter = false;
    public static bool isFixedEncounterDone = false;
    private EntityCombat currentEntityTurn
    {
        get
        {   
            if (currentTurnEntityIndex>=entities.Count)            
                currentTurnEntityIndex = 0;
                return entities[currentTurnEntityIndex];                            
        }
    }

    public bool IsCombatFinish 
    { 
        get => isCombatFinish; 

        set {             
            isCombatFinish = value;
        } 
    }
    private void ProcessEnemyLoot(Reward lootOfTheEnemy)
    {
        loot.MaxGold += UnityEngine.Random.Range(lootOfTheEnemy.MaxGold / 2, lootOfTheEnemy.MaxGold + 1);
        loot.maxCrystals += UnityEngine.Random.Range(lootOfTheEnemy.maxCrystals / 2, lootOfTheEnemy.maxCrystals + 1);
        if (lootOfTheEnemy.possibleCards.Count > 0)
        {
            foreach (var item in lootOfTheEnemy.possibleCards)
            {
                var chance = UnityEngine.Random.Range(1, 100 + 1);
                if (chance < lootOfTheEnemy.cardDropProbability)
                    loot.possibleCards.Add(item);
            }
        }
        else if (lootOfTheEnemy.possibleValuableItemDrop.Count > 0)
        {
            foreach (var item in lootOfTheEnemy.possibleValuableItemDrop)
            {
                var chance = UnityEngine.Random.Range(1, 100+1);
                if (chance < lootOfTheEnemy.valuableItemDropProbability)
                    loot.possibleValuableItemDrop.Add(item);
            }
        }

    }
    public void Start()
    {
        entities = new List<EntityCombat>();
        playerManager = GameObject.FindGameObjectWithTag("PlayerManager")?.GetComponent<Player>();
        IsCombatFinish = false;
        loot = new Reward();
        loot.MaxGold = 0;
        loot.maxCrystals = 0;
        loot.possibleValuableItemDrop = new List<Item>();
        loot.possibleCards = new List<Card>();
        prefabList = new Dictionary<int, GameObject>();
        //Missing find and add the playeableCharacters. ids 0+i
        //Change this 5 lines to the one from the Player class!
        EntityCombat playerCha;         
        GameObject character;
        if(playerManager == null)
        {
            playerCha = new EntityCombat(factory.CreateEntity(true, 1, 0), player, 0, false,sound);
            character = factory.CreatePrefabEntity(true, 0, spawnLocationsPlayer[0]);
            character.GetComponent<UIDHolder>().SetUID(0);
            character.GetComponent<UIDHolder>().SetDataCombat(playerCha);
            entities.Add(playerCha);
            
            ai.AddPlayerToAggroList(playerCha);
        }
        else
        {
            for (int i = 0; i< playerManager.GetParty().Count; i++)
            {
                playerCha = new EntityCombat(playerManager.GetPartyMember(i),player, i, false, sound);
                character = Instantiate(playerManager.GetPartyMemberPrefab(i), spawnLocationsPlayer[i]);
                character.GetComponent<UIDHolder>().SetUID(0);
                character.GetComponent<UIDHolder>().SetDataCombat(playerCha);               
                Debug.Log("Initial hp: " + playerCha.GetAttr().hp);
                entities.Add(playerCha);
                prefabList.Add(i, character);
                ai.AddPlayerToAggroList(playerCha);
            }
        }
        
        
        /// SPawn enemies
        int randEnemNum = UnityEngine.Random.Range(1, entities.Count+1);
        if (isFixedEncounter) randEnemNum = 2;
        // Create enmies with UID 100+i
        // Missing randomize the type of enemy.
        int enemyLevel = UnityEngine.Random.Range(1, playerManager.GetPartyMember(0).GetLevel() + 1);
        for (int i = 0; i < randEnemNum; i++)
        {
            int enemyToSpawn = UnityEngine.Random.Range(0,factory.GetEnemies().Count);
            
            EntityCombat entity = new EntityCombat(factory.CreateEntity(false,enemyLevel , enemyToSpawn), ai, 100 + i, true, sound);
            var enemy = factory.CreatePrefabEntity(false, 0, spawnLocationsEnemy[i]);
            enemy.GetComponent<UIDHolder>().SetUID(100 + i);
            enemy.GetComponent<UIDHolder>().SetDataCombat(entity);
            ProcessEnemyLoot(factory.GetEnemyReward(enemyToSpawn));
            /// here will have to instance an enemy in the world, and give this data.
            entities.Add(entity);
            prefabList.Add(100 + i, enemy);
        }


        /// Enviar mensaje a UI con players UIDS y enemies UIDS
        Invoke("StartCombat", 2.5f);
        // init enemies and player for combat.
    }

    public void EntityKilled(EntityCombat entityKilled)
    {
        numEnemiesDead++;
        entities.Remove(entityKilled);
        prefabList[entityKilled.GetUID()]?.SetActive(false); // lacks animation notify the UI? or just activate an animation and at the end of it it disables itself?
        // notify UI
    }
    public void EndCurrentEntityTurn()
    {
        turnNumber++;
        Debug.Log("Turn over, now is turn: " + turnNumber);
     //   if (!currentEntityTurn.isEnemy)
     //        currentEntityTurn.GetController().EndTurn();
        currentTurnEntityIndex++;
        if (currentTurnEntityIndex >= entities.Count)
            currentTurnEntityIndex = 0;
        if(!isCombatFinish)
            currentEntityTurn.GetMyTurn();
    }

    public List<Card> GetCardsOnCementary()
    {
        List<Card> actionsUsed = currentEntityTurn.GetCementery();
        return actionsUsed;
    }

    public List<Card> GetCardsOnDeck()
    {
        List<Card> actionsRemaining = currentEntityTurn.GetRemainingCards();
        return actionsRemaining;
    }
    
    public void PlayCardOnTarget(int index, int UIDTarget)
    {
        currentEntityTurn.GetController().PlayCard(index,UIDTarget);
    }

    public void PlayCardOnTarget(int index, int[] UIDTargets)
    {
        currentEntityTurn.GetController().PlayCard(index, UIDTargets);
    }

    public void StartCombat()
    {
        int index = 0;
        int[] iniciative = new int[entities.Count];        
        for (int i = 0; i < entities.Count; i++)
        {
            int init = entities[i].GetInit();
            index = i;
            for (int j = i+1; j < entities.Count; j++)
            {
                if (init < entities[j].GetInit())
                {
                    init = entities[j].GetInit();
                    index = j;
                }
                    
            }
            iniciative[i] = index;
        }
        for (int i = 0; i < iniciative.Length; i++)
        {
            EntityCombat oldEntity = entities[i];
            entities[i] = entities[iniciative[i]];
            entities[iniciative[i]] = oldEntity;
        }
        foreach (var item in entities)
        {
            if (item.isEnemy)
                ai.AddCreature(item);
            else
                player.AddCreature(item);
        }
        
        currentTurnEntityIndex = 0;
        currentEntityTurn.GetMyTurn();//we tell that entity it is his turn;
    }
    public void EndCombat(bool battleWon = false)
    {
        
        
        isCombatFinish = true;
        if (battleWon)
        {
            GameObject.FindGameObjectWithTag("UIManager").GetComponent<IUIManager>().EndCombat(GameObject.FindGameObjectWithTag("PlayerManager").GetComponent<Player>().GetState().lastLevelName, battleWon, loot);
            playerManager.GetInventory().AddCrystal(loot.maxCrystals);
            playerManager.GetInventory().AddGold(loot.MaxGold);
            foreach (var item in loot.possibleValuableItemDrop)
            {
                playerManager.GetInventory().AddItem(item);
            }
            /// TODO CARDS
            //UnityEngine.SceneManagement.SceneManager.LoadScene(GameObject.FindGameObjectWithTag("PlayerManager").GetComponent<Player>().GetState().lastLevelName); //Missing finding endCombat Message and rewards            
        }
        else
        {
            GameObject.FindGameObjectWithTag("UIManager").GetComponent<IUIManager>().EndCombat("MainMenu", battleWon, loot);
        }

    }

    internal float AttackEntity(CardResult result, int targetUID, EntityCombat attacker)
    {
        if(result.CardAttribute == CardAttribute.Intelligence)
        {
            sound.PlayMagicAttack();
        }
        else
        {
            sound.PlayAttack();
        }
        foreach (var entity in entities)
        {
            if (entity.CheckUID(targetUID))
            {
                Debug.Log(targetUID);
                return entity.GetDamage(result, attacker.GetUID());                
            }
        }
        return -1;
    }

    public void CardAnimationOver()
    {
        currentEntityTurn.GetController().CardAnimationOver();
    }

    public void EndPlayerTurn()
    {
        currentEntityTurn.GetController().EndTurn();
    }
}
