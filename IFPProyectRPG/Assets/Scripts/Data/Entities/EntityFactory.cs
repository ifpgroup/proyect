﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityFactory : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] List<EnemyType> enemiesToSpawn;
    [SerializeField] List<DefaultDeck> playerDecks;
    [SerializeField] Equipment[] intEquipmentDefault;
    [SerializeField] Equipment[] strEquipmentDefault;

    [SerializeField] List<GameObject> enemiesToSpawnPrefabs;
    [SerializeField] List<GameObject> playersToSpawnPrefabs;

    void Start()
    {
        
    }
    public List<EnemyType> GetEnemies() => enemiesToSpawn;
    public List<Equipment> GetEquipmentSelected(bool isMage)
    {
        if (isMage)
            return new List<Equipment>(intEquipmentDefault);
        else
            return new List<Equipment>(strEquipmentDefault);
    }
    public Reward GetEnemyReward(int index) => enemiesToSpawn[index].GetLoot();
    public Entity CreateEntity(bool isPlayer, int lvl, int typeToSpawn = 0)
    {
        if (isPlayer)
        {
            int rand = Random.Range(0, 2);
            PlayableCharacter player;
            if (rand==0) player = new PlayableCharacter(playerDecks[0].actions,intEquipmentDefault[0], intEquipmentDefault[1], intEquipmentDefault[2]);
            else player = new PlayableCharacter(playerDecks[0].actions, strEquipmentDefault[0], strEquipmentDefault[1], strEquipmentDefault[2]);
            return player;
        }
        else
        {
            Enemy enemy = new Enemy(enemiesToSpawn[typeToSpawn], lvl);
            return enemy;
        }
    }
    public GameObject CreatePrefabEntity(bool isPlayer, int typeToSpawn, Transform location)
    {
        if(!isPlayer)
            return Instantiate(enemiesToSpawnPrefabs[typeToSpawn], location);
        else
            return Instantiate(playersToSpawnPrefabs[typeToSpawn], location);
    }
    public PlayableCharacter CreatePlayer(Stats stats,bool isMage)
    {
        PlayableCharacter player;
        if (isMage)
            player = new PlayableCharacter(stats, playerDecks[1].actions, intEquipmentDefault[0], intEquipmentDefault[1], intEquipmentDefault[2]);
        else
            player = new PlayableCharacter(stats, playerDecks[0].actions, strEquipmentDefault[0], strEquipmentDefault[1], strEquipmentDefault[2]);
        return player;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
